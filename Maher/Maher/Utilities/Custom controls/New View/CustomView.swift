//
//  CustomView.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ViewButtonDelegate {
    func didButtonPressed(keyWord: String)
}

class CustomView: UIView {
    
    var ButtonDelegate: ViewButtonDelegate?

    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var tView: UIView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var ordersImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var providerImageView: UIImageView!
    @IBOutlet weak var notificationsImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        
        print("LFES")
        
        newView.addCornerRadius(raduis: newView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        let button = UIButton(type: .custom)
        var toMakeButtonUp = 40
        button.frame = CGRect(x: self.centerView.frame.width / 2 - 25, y: self.centerView.frame.height / 2 - 50, width: 50, height: 55)
        print(self.centerView.frame.width / 2 - 25)
//        button.setBackgroundImage(UIImage(named: "Add new question"), for: .normal)
//        button.setBackgroundImage(UIImage(named: "Add new question"), for: .highlighted)
        button.setBackgroundImage(UIImage(named: "17"), for: .normal)
        button.setBackgroundImage(UIImage(named: "17"), for: .highlighted)
        let heightDifference: CGFloat = CGFloat(toMakeButtonUp)
//        if heightDifference < 0 {
//            button.center = tabBar.center
//        } else {
//            var center: CGPoint = tabBar.center
//            center.y = center.y - heightDifference / 0.6
//            button.center = center
//        }
        button.addTarget(self, action: #selector(goToHome), for:.touchUpInside)
        centerView.addSubview(button)
        tView.roundCorners([.topLeft, .topRight], radius: 30)
        newView.backgroundColor = .yellow
        setLocalization()
    }
    
    func configureBottomView(key: String) {
        notificationsLabel.textColor = .white
        providerLabel.textColor = .white
        newView.backgroundColor = .white
        ordersLabel.textColor = .white
        profileLabel.textColor = .white
        ordersImageView.image = UIImage(named: "ic_directions_carWhite")
//        profileImageView.image = UIImage(named: "Profiless")
        profileImageView.image = UIImage(named: "Profile copy white")
        providerImageView.image = UIImage(named: "444")
//        notificationsImageView.image = UIImage(named: "notifications")
        notificationsImageView.image = UIImage(named: "bell copy2")

        if key == "orders" {
            ordersLabel.textColor = .yellow
            ordersImageView.image = UIImage(named: "ic_directions_carYellow")
        } else if key == "main" {
            newView.backgroundColor = .yellow
        } else if key == "profile" {
            profileLabel.textColor = .yellow
//            profileImageView.image = UIImage(named: "ProfileYellow")
            profileImageView.image = UIImage(named: "Profile _yelloww")
        } else if key == "notifications" {
            notificationsLabel.textColor = .yellow
//            notificationsImageView.image = UIImage(named: "notificationsYellow")
            notificationsImageView.image = UIImage(named: "bell copy")
        } else if key == "provider" {
            providerLabel.textColor = .yellow
            providerImageView.image = UIImage(named: "Service providerYellow")
        }
    }
    
    func setLocalization() {
        notificationsLabel.text = "notifications label".localized
        providerLabel.text = "provider label".localized
        mainLabel.text = "main label".localized
        ordersLabel.text = "orders label".localized
        profileLabel.text = "profile label".localized
    }
    
    @objc func goToHome() {
        print("Home")
        didButtonPressed(keyWord: "Home")
    }
    
    //MARK:- Delegate Helpers
    func didButtonPressed(keyWord: String) {
        if let delegateValue = ButtonDelegate {
            delegateValue.didButtonPressed(keyWord: keyWord)
        }
    }

    @IBAction func notificationsButtonIsPressed(_ sender: Any) {
        print("notifications")
        didButtonPressed(keyWord: "Notifications")
        newView.backgroundColor = .white
    }
    
    @IBAction func providerButtonIsPressed(_ sender: Any) {
        print("provider")
        didButtonPressed(keyWord: "Provider")
        newView.backgroundColor = .white
    }
    
    @IBAction func ordersButtonIsPressed(_ sender: Any) {
        print("Orders")
        didButtonPressed(keyWord: "Orders")
        newView.backgroundColor = .white
    }
    
    @IBAction func profileButtonIsPressed(_ sender: Any) {
        print("profile")
        didButtonPressed(keyWord: "Profile")
        newView.backgroundColor = .white
    }
    
    @IBAction func mainButtonIsPressed(_ sender: Any) {
        print("main")
        didButtonPressed(keyWord: "Main")
        newView.backgroundColor = .yellow
    }
}
