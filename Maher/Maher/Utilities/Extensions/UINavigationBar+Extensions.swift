//
//  UINavigationBar+Extensions.swift
//  GameOn
//
//  Created by Hassan on 2/3/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func adjustDefaultNavigationBar() {
//        self.barTintColor = mainBlueColor
        self.barTintColor = #colorLiteral(red: 0.3098039216, green: 0.2509803922, blue: 0.5843137255, alpha: 1)
        
        self.setBackgroundImage(UIImage(), for: .default)
//        self.backgroundColor = mainBlueColor
        self.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.2509803922, blue: 0.5843137255, alpha: 1)
        
        self.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        
        self.isTranslucent = false
        self.shadowImage = UIImage()
    }
}
