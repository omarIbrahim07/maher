//
//  LoginViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var phoneBottomView: UIView!
    @IBOutlet weak var passwordBottomView: UIView!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        animateView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        hideViews()
        configureView()
        // Do any additional setup after loading the view.
        closeKeypad()
    }
    
    func hideViews() {
        let scaleDownTransform = CGAffineTransform(scaleX: 0, y: 0)
        logoImageView.transform = scaleDownTransform
        let scaleLeftTransform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
        phoneTextField.transform = scaleLeftTransform
        phoneBottomView.transform = scaleLeftTransform
        let scaleRightTransform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        passwordTextField.transform = scaleRightTransform
        passwordBottomView.transform = scaleRightTransform
    }
    
    func animateView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1) {
                self.logoImageView.transform = .identity
                self.phoneTextField.transform = .identity
                self.passwordTextField.transform = .identity
                self.phoneBottomView.transform = .identity
                self.passwordBottomView.transform = .identity
            }
        }
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
//        phoneTextField.endEditing(true)
//        passwordTextField.endEditing(true)
    }
    
    func configureView() {
    
        mainView.roundCorners([.topLeft , .bottomRight], radius: 70)
        signUpButton.addCornerRadius(raduis: 25, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        logInButton.addCornerRadius(raduis: 25, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        logInButton.setTitle("log in".localized, for: .normal)
        forgetPasswordButton.setTitle("forget password".localized, for: .normal)
        phoneTextField.placeholder = "phone".localized
        passwordTextField.placeholder = "password".localized
        signUpButton.setTitle("sign up".localized, for: .normal)
        orLabel.text = "or button title".localized

        self.navigationItem.title = "log in".localized
        
        if "Lang".localized == "en" {
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
        }
        
    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 0.7411764706, green: 0.7960784314, blue: 0.8862745098, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
    
    func presentRegistration() {
        if let SignUpNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInNavigationVC") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignInViewController {
            //                rootViewContoller.test = "test String"
            self.present(SignUpNavigationController, animated: true, completion: nil)
        }
    }
    
//    func logIn() {
//
//        guard let phone = phoneTextField.text, phone.isEmpty == false else {
//            let apiError = APIError()
//            apiError.message = "Please enter your phone"
//            showError(error: apiError)
//            return
//        }
//
//        guard let password = passwordTextField.text, password.isEmpty == false else {
//            let apiError = APIError()
//            apiError.message = "Please enter the password"
//            showError(error: apiError)
//            return
//        }
//
//        let params: [String : AnyObject] = [
//            "phone" : phone as AnyObject,
//            "password" : password as AnyObject,
////            "app_id" : 4 as AnyObject,
////            "fcm_token" : FirebaseToken as AnyObject
//        ]
//
//        startLoading()
//
//        AuthenticationAPIManager().loginUser(basicDictionary: params, onSuccess: { (token) in
//
//            self.stopLoadingWithSuccess()
//            print(UserDefaultManager.shared.currentUser)
//            self.presentHomeScreen()
//
//        }) { (error) in
//            self.stopLoadingWithError(error: error)
//        }
//
//
//    }
    
    func logIn() {
        
        guard let phone = phoneTextField.text, phone.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "Please enter your phone"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "Please enter the password"
            showError(error: apiError)
            return
        }
        
        let params: [String : AnyObject] = [
            "phone" : phone as AnyObject,
            "password" : password as AnyObject,
//            "app_id" : 3 as AnyObject,
////            "fcm_token" : FirebaseToken as AnyObject
        ]
        
        startLoading()
        
        AuthenticationAPIManager().loginUser(basicDictionary: params, onSuccess: { (token, success) in
            
            if success == true {
                self.presentHomeScreen()
            } else if success == false {
                let apiError = APIError()
                if "Lang".localized == "en" {
                    apiError.message = "Invalid Phone or Password"
                } else if "Lang".localized == "ar" {
                    apiError.message = "هناك خطأ ما في بياناتك"
                }
                self.showError(error: apiError)
                return
            }

        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
//    func getUserProfile() {
//        weak var weakSelf = self
//
//        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
//
//            weakSelf?.stopLoadingWithSuccess()
//            weakSelf?.presentHomeScreen()
//
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
//    }


    
    //MARK:- Navigation
    func presentHomeScreen() {
        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
            appDelegate.window!.rootViewController = viewController
            appDelegate.window!.makeKeyAndVisible()
        }
    }
    
    func goToForgetPassword() {
        if let forgetPasswordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController {
            //                rootViewContoller.test = "test String"
            navigationController?.pushViewController(forgetPasswordVC, animated: true)
//            (forgetPasswordVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        presentRegistration()
    }
    
    @IBAction func forgetPasswordButtonPressed(_ sender: Any) {
        goToForgetPassword()
    }
    
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        logIn()
    }
    

}
