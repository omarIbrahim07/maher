//
//  AboutUsViewController.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {

    var error: APIError?

    lazy var viewModel: AboutUsViewModel = {
        return AboutUsViewModel()
    }()

    @IBOutlet weak var egyDesignerButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var egyDesignerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                                                
        viewModel.reloadStaticPage = { [weak self] () in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
//                    if let saftey = self?.viewModel.termsAndConditions {
//                        self!.setData(staticPage: saftey)
//                }
            }
        }
        
        self.viewModel.initSafteyProcedures(lang: "Lang".localized)
    }
    
    func setData(staticPage: StaticPage) {
//        termsAndConditionsLabel.text = staticPage.content
    }
    
    func configureView() {
        navigationItem.title = "about app".localized
        egyDesignerLabel.text = "Copyright©️RAWA \nrawa-sa.com All Rights Reserved"
        egyDesignerButton.setTitle("Designed and Developed by egydesigner", for: .normal)
    }
    
    func configureTableView() {
//        egyDesignerLabel.text = "Copyright©️RAWA \nrawa-sa.com All Rights Reserved \nDesigned and Developed by egydesigner"
        tableView.register(UINib(nibName: "TermsAndConditionTableViewCell", bundle: nil), forCellReuseIdentifier: "TermsAndConditionTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension AboutUsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TermsAndConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionTableViewCell") as? TermsAndConditionTableViewCell {
            
            if let title: String = self.viewModel.termsAndConditions?.content {
                cell.setTermsAndConditions(title: title)
                return cell
            }
        }
        return UITableViewCell()
    }
    
}
