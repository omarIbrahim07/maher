//
//  AboutUsViewModel.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class AboutUsViewModel {
    
    var error: APIError?
    var reloadStaticPage: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var termsAndConditions: StaticPage? {
        didSet {
            self.reloadStaticPage?()
        }
    }
    
    func initSafteyProcedures(lang: String) {
        state = .loading
        
        if lang == "ar" {
            getSafteyProcedures(pageID: 1)
        } else if lang == "en" {
            getSafteyProcedures(pageID: 12)
        }
    }
    
    private func getSafteyProcedures(pageID: Int) {
        
        let params: [String : AnyObject] = [
            "page_id" : pageID as AnyObject
        ]
        
        StaticAPIManager().getStaticPage(basicDictionary: params, onSuccess: { (staticPage) in
            self.termsAndConditions = staticPage
            self.state = .populated

        }) { (error) in
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
