//
//  DelegateNewOrdersViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation

extension DelegateNewOrdersViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
        print(location.coordinate)
//                saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
            providerCurrentLatitude = location.coordinate.latitude
            providerCurrentLongitude = location.coordinate.longitude
            
        }
    }
}

extension DelegateNewOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.ordersChoosed == 1 {
            if let cell: DelegateNewOrdersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateNewOrdersTableViewCell") as? DelegateNewOrdersTableViewCell {
                let cellVM = viewModel.getCellViewModel( at: indexPath )
                cell.photoListCellViewModel = cellVM

                return cell
            }
        } else if self.ordersChoosed == 2 {
            if let cell: DelegateAnotherOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateAnotherOrderTableViewCell") as? DelegateAnotherOrderTableViewCell {
                let cellVM = viewModel.getCellViewModel( at: indexPath )
                cell.photoListCellViewModel = cellVM

                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.userPressed(at: indexPath)
                
        if let selectedServie = self.viewModel.selectedDelegateNewOrder {
            if let orderID: Int = selectedServie.orderID, let orderStatus: Int = selectedServie.orderStatus, let clientName: String = selectedServie.clientName {
                if orderStatus == 0 {
                    self.goToDelegateOrderDescription(orderID: orderID, clientName: clientName)
                } else if orderStatus == 1 {
                    print("Go to chat")
                    goToChat(orderID: orderID)
                } else if orderStatus == 2 {
                    print("Order is done")
                }
            }
        }

    }
}

extension DelegateNewOrdersViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
