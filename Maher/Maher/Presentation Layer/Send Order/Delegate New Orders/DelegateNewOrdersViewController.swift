//
//  DelegateNewOrdersViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation

var providerCurrentLongitude: Double?
var providerCurrentLatitude: Double?

class DelegateNewOrdersViewController: BaseViewController {
    
    lazy var viewModel: DelegatesNewOrdersViewModel = {
        return DelegatesNewOrdersViewModel()
    }()
    
    var error: APIError?
    
    var ordersChoosed: Int? = 2
    let locationManager = CLLocationManager()
    
    let statusEn = ["New orders", "Active orders", "Completed orders"]
    let statusAr = ["الطلبات الجديدة", "الطلبات النشطة", "الطلبات المكتملة"]

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var offersButton: UIButton!
    @IBOutlet weak var onlineSwitch: UISwitch!
    @IBOutlet weak var arrangeByLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkLocationManager()
        setNavigationTitle()
        configureSideMenu()
        configureView()
//        makeCustomLogoutBarButtonItem(imageName: "menu", buttonType: "left")

        configureTableView()
        initVM()
        checkOnlineButton()
    }
    
    func checkOnlineButton() {
        if onlineSwitch.isOn == true {
            tableView.isHidden = false
        } else if onlineSwitch.isOn == false {
            tableView.isHidden = true
        }
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
//        viewModel.updateProviderState = { [weak self] () in
//            DispatchQueue.main.async {
//
//            }
//        }
        
        if let longitude = providerCurrentLongitude, let latitude = providerCurrentLatitude {
            self.viewModel.initFetch(longitude: String(longitude), latitude: String(latitude))
        }
    }

    func checkLocationManager() {
        locationManager.requestWhenInUseAuthorization()
//        checkLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func configureView() {
        arrangeByLabel.text = "arrange by label".localized
        offersButton.setTitle("offers label".localized, for: .normal)
        if "Lang".localized == "en" {
            offersButton.roundCorners([.topRight, .bottomRight], radius: offersButton.frame.height / 2)
        } else if "Lang".localized == "ar" {
            offersButton.roundCorners([.topLeft, .bottomLeft], radius: offersButton.frame.height / 2)
        }
        navigationItem.title = "orders label".localized
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.configureBottomView(key: "provider")
        reusableView.ButtonDelegate = self
        
        
        if "Lang".localized == "en" {
            self.orderStatusLabel.text = statusEn[0]
        } else if "Lang".localized == "ar" {
            self.orderStatusLabel.text = statusAr[0]
        }
        
        if UserDefaultManager.shared.currentUser?.ordersNotify == 1 {
            onlineSwitch.isOn = true
        } else {
            onlineSwitch.isOn = false
        }
    }
    
    func setNavigationTitle() {
        navigationItem.title = "orders".localized
    }
    
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 30))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(self.configureLeftSideMenu), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(self.configureRightSideMenu), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    @objc func configureSideMenu() {
        // Define the menus
        let leftMenuNavigationControllerr = storyboard?.instantiateViewController(withIdentifier: "LefttSideMenu") as? UISideMenuNavigationController

        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationControllerr
        SideMenuManager.defaultManager.leftMenuNavigationController?.menuWidth = (view.frame.width - 50)
        
        if let x = self.navigationController
        {
            
            SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.left)
        }
        
        leftMenuNavigationControllerr!.statusBarEndAlpha = 0
    }
    
    @objc func configureLeftSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.rightMenuNavigationController?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.leftMenuNavigationController?.menuWidth = (view.frame.width - 50)

        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    @objc func configureRightSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController?.leftSide = false
        SideMenuManager.default.rightMenuNavigationController = menu
        SideMenuManager.default.rightMenuNavigationController?.menuWidth = (view.frame.width - 50)
        
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "DelegateNewOrdersTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateNewOrdersTableViewCell")
        tableView.register(UINib(nibName: "DelegateAnotherOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateAnotherOrderTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK:- Show Time Options Picker
    func setupOrdersStatusSheet() {
        
        let sheet = UIAlertController(title: "orders alert title".localized, message: "orders alert message".localized, preferredStyle: .actionSheet)
        if "Lang".localized == "en" {
            for status in statusEn {
                sheet.addAction(UIAlertAction(title: status, style: .default, handler: {_ in
//                    self.viewModel.getGender(gender: gender)
                    self.orderStatusLabel.text = status
                }))
            }
        } else if "Lang".localized == "ar" {
            for status in statusAr {
                sheet.addAction(UIAlertAction(title: status, style: .default, handler: {_ in
//                    self.viewModel.getGender(gender: gender)
                    self.orderStatusLabel.text = status
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "orders alert cancel button".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
        
    }
    
// MARK:- Actions
    @IBAction func ordersStatusButtonIsPressed(_ sender: Any) {
        self.setupOrdersStatusSheet()
    }
    
    @IBAction func onlineIsPressed(_ sender: UISwitch) {
        self.viewModel.changeProviderState()
        if sender.isOn {
            print("Online")
            tableView.isHidden = false
        } else if sender.isOn == false {
            print("Offline")
            tableView.isHidden = true
        }
    }

    
    // MARK: - Navigation
    func goToDelegateOrderDescription(orderID: Int, clientName: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DelegateOrderDescriptionViewController") as! DelegateOrderDescriptionViewController
        viewController.orderID = orderID
        viewController.clientName = clientName

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToChat(orderID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        viewController.orderID = orderID

        navigationController?.pushViewController(viewController, animated: true)
    }

}
