//
//  DelegateNewOrdersTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateNewOrdersTableViewCell: UITableViewCell {

    var photoListCellViewModel : DelegatesNewOrdersCellViewModel? {
        didSet {
            if let clientName: String = photoListCellViewModel?.clientName {
                clientNameLabel.text = clientName
            }
        }
    }

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var clientNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        otherView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        innerView.layer.masksToBounds = false
        innerView.layer.shadowRadius = 4
        innerView.layer.shadowOpacity = 1
        innerView.layer.shadowColor = UIColor.gray.cgColor
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
