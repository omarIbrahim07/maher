//
//  DelegatesNewOrdersViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class DelegatesNewOrdersViewModel {
    
    private var delegateNewOrders: [DelegateNewOrder] = [DelegateNewOrder]()
    var selectedDelegateNewOrder: DelegateNewOrder?
    
    var error: APIError?
    
    var cellViewModels: [DelegatesNewOrdersCellViewModel] = [DelegatesNewOrdersCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateProviderState: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var providerState: Int? {
        didSet {
            self.updateProviderState?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initFetch(longitude: String, latitude: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "latitude"    : latitude as AnyObject,
            "longitude"    : longitude as AnyObject
        ]
        
        DelegatesOffersAPIManager().getDelegatesNewOrders(basicDictionary: params, onSuccess: { (delegateNewOrders) in
            
            self.processFetchedPhoto(delegateNewOrders: delegateNewOrders)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func changeProviderState() {
        
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        AuthenticationAPIManager().changeProviderState(basicDictionary: params, onSuccess: { (providerState) in
            
        self.state = .populated
        self.providerState = providerState
            self.getUserProfile()

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getUserProfile() {
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func processFetchedPhoto( delegateNewOrders: [DelegateNewOrder] ) {
          self.delegateNewOrders = delegateNewOrders // Cache
          var vms = [DelegatesNewOrdersCellViewModel]()
          for delegateNewOrder in delegateNewOrders {
              vms.append( createCellViewModel(delegateNewOrder: delegateNewOrder) )
          }
          self.cellViewModels = vms
      }
    
    func createCellViewModel( delegateNewOrder: DelegateNewOrder ) -> DelegatesNewOrdersCellViewModel {
                
        return DelegatesNewOrdersCellViewModel(orderID: delegateNewOrder.orderID, orderStatus: delegateNewOrder.orderStatus, clientName: delegateNewOrder.clientName, pickLatitude: delegateNewOrder.pickLatitude, pickLongitude: delegateNewOrder.pickLongitude, desLatitude: delegateNewOrder.desLatitude, desLongitude: delegateNewOrder.desLongitude)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> DelegatesNewOrdersCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func userPressed( at indexPath: IndexPath ){
        let delegateNewOrder = self.delegateNewOrders[indexPath.row]

        self.selectedDelegateNewOrder = delegateNewOrder
    }

    
}
