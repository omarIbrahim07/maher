//
//  DelegatesNewOrdersCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegatesNewOrdersCellViewModel {
    let orderID: Int?
    let orderStatus: Int?
    let clientName: String?
    let pickLatitude: String?
    let pickLongitude: String?
    let desLatitude: String?
    let desLongitude: String?
//    let id: Int?
//    let orderDetails: String?
//    let userID: Int?
//    let itemId: Int?
//    let subserviceID: Int?
//    let promocode: String?
//    let paymentMethod: Int?
//    let pickLatitude: String?
//    let pickLongitude: String?
//    let desLatitude: String?
//    let orderTime: String?
//    let attachmentFile: String?
//    let providerID: Int?
//    let country: String?
}
