//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation

class HomeViewController: BaseViewController {
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var error: APIError?
    let locationManager = CLLocationManager()
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureSideMenu()
        configureView()
        makeCustomLogoutBarButtonItem(imageName: "menu", buttonType: "left")
        makeNotificationIcon(imageName: "bell", buttonType: "right")
//        configureSideMenu()
        configureCollectionView()
        checkLocationManager()
        
        initVM()
    }
    
    func checkLocationManager() {
        locationManager.requestWhenInUseAuthorization()
//        checkLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
        
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
        
        viewModel.initFetch(forCountry: "SA")
    }
    
    func setNavigationTitle() {
        navigationItem.title = "main label".localized
    }
    
    func configureView() {
        backgroundImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        reusableView.frame = self.viewContainToolBar.bounds
        reusableView.configureBottomView(key: "main")
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
        
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 30))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(self.configureLeftSideMenu), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(self.configureRightSideMenu), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func makeNotificationIcon(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
//        containerButton.addTarget(self, action: #selector(self.goToNotifications), for: UIControl.Event.touchUpInside)
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func saveLocation(longitude: Double, latitude: Double) {
        UserDefaults.standard.set(longitude, forKey: "longitude") //Bool
        UserDefaults.standard.set(latitude, forKey: "latitude") //Bool
    }
    
    @objc func configureSideMenu() {
        // Define the menus
        let leftMenuNavigationControllerr = storyboard?.instantiateViewController(withIdentifier: "LefttSideMenu") as? UISideMenuNavigationController

        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationControllerr
        SideMenuManager.defaultManager.leftMenuNavigationController?.menuWidth = (view.frame.width - 50)
        
        if let x = self.navigationController
        {
            
            SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.left)
        }
        
        leftMenuNavigationControllerr!.statusBarEndAlpha = 0
    }
    
    @objc func configureLeftSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.rightMenuNavigationController?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.leftMenuNavigationController?.menuWidth = (view.frame.width - 50)

        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    @objc func configureRightSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController?.leftSide = false
        SideMenuManager.default.rightMenuNavigationController = menu
        SideMenuManager.default.rightMenuNavigationController?.menuWidth = (view.frame.width - 50)
        
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Navigation
//    @objc func goToNotifications() {
//        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//            self.showCantLoginPopUp()
//            return
//        }
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
//        //        viewController.typeIdSelected = self.typeIdSelected
//        navigationController?.pushViewController(viewController, animated: true)
//    }

    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }
    
    func goToSubcategories(service: Service) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SubcategoriesViewController") as! SubcategoriesViewController
        viewController.selectedService = service

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToDDDDD(orderID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DelegateRateViewController") as! DelegateRateViewController
        viewController.viewModel.orderID = orderID

        navigationController?.pushViewController(viewController, animated: true)
    }
        
}
