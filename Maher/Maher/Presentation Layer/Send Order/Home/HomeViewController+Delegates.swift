//
//  HomeViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
        print(location.coordinate)
//                saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
            providerCurrentLatitude = location.coordinate.latitude
            providerCurrentLongitude = location.coordinate.longitude
            
        }
    }
}

// MARK:- CollectionView Delegate
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell {
            
            let cellVM = viewModel.getCellViewModel( at: indexPath )
            cell.photoListCellViewModel = cellVM
            
//            cell.cellView.layer.masksToBounds = false
//            cell.cellView.layer.shadowRadius = 4
//            cell.cellView.layer.shadowOpacity = 1
//            cell.cellView.layer.shadowColor = UIColor.gray.cgColor
//            cell.cellView.layer.shadowOffset = CGSize(width: 0 , height:20)

            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.showCantLoginPopUp()
            return
        }
        self.viewModel.userPressed(at: indexPath)
//        self.goToDDDDD(orderID: 5)
//        return
                
        if let selectedServie = self.viewModel.selectedService {
            self.goToSubcategories(service: selectedServie)
        }
        
    }
}

// MARK:- CollectionView Layout delegate
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 40 ) / 2.0
//        return CGSize (width: width, height: width * 1.1)
        return CGSize (width: width, height: 151)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 15 , left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension HomeViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
