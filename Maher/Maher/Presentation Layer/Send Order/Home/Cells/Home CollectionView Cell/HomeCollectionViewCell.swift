//
//  HomeCollectionViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/13/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    
    var photoListCellViewModel : HomeCellViewModel? {
        didSet {
            serviceTitle.text = photoListCellViewModel?.titleText
            if let imageURL: String = ImageURLServices + (photoListCellViewModel?.imageUrl)! {
                serviceImage.loadImageFromUrl(imageUrl: imageURL)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.cellView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
//        self.cellView.layer.shadowOpacity = 0.66
//        self.cellView.layer.shadowRadius = 30
    }

}
