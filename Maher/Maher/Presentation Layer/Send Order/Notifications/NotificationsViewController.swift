//
//  NotificationsViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/18/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {
    
    lazy var viewModel: NotificationsViewModel = {
        return NotificationsViewModel()
    }()
    
    var error: APIError?
    
    var orderStatusEn: [String] = ["Waitng", "On my way", "Finished"]
    var orderStatusAr: [String] = ["قيد الانتظار", "في الطريق إليك", "منتهية"]

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        initVM()
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.initFetch()
    }
    
    func configureView() {
        self.navigationItem.title = "notifications".localized
        reusableView.frame = self.viewContainToolBar.bounds
        reusableView.configureBottomView(key: "notifications")
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
        
    func configureTableView() {
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }


    
    // MARK: - Navigation
//    func goToOrderDetails(orderID: Int) {
//        if let orderDetailsController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
//            orderDetailsController.orderId = orderID
//           self.present(orderDetailsController, animated: true, completion: nil)
//           print("Mentainance Orders")
//       }
//    }

}
