//
//  NotificationCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct NotificationCellViewModel {
    let id: Int?
    let orderID: Int?
    let userID: Int?
    let message: String?
    let updatedAt: String?
    let delegateName: String?
    let orderState: Int?
//    let service: String?
}
