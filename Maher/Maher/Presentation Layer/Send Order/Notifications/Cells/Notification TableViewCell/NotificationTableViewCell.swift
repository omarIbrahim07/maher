//
//  NotificationTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/18/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    var photoListCellViewModel : NotificationCellViewModel? {
        didSet {
            
            if let orderNumber: Int = photoListCellViewModel?.orderID {
//                orderNumberValueLabel.text = String(orderNumber)
            }
            
            if let delegate: String = self.photoListCellViewModel?.delegateName {
                delegateValueLabel.text = delegate
            }
            
//            if let orderState: Int = photoListCellViewModel?.orderState {
////                orderStateValueLabel.text = ""
//            }
            
//            if let service: String = photoListCellViewModel?.service {
//                serviceValueLabel.text = service
//            }
        }
    }
    
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var delegateValueLabel: UILabel!
    @IBOutlet weak var orderStateValueLabel: UILabel!
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var providerImageView: UIImageView!
    @IBOutlet weak var orderTimingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        configureView()
    }
    
    func configureView() {
        notificationView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        providerImageView.addCornerRadius(raduis: providerImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
