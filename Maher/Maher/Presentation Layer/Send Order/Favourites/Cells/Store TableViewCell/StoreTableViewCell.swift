//
//  StoreTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {
    
    var favouritedelegate: FavouriteButtonTableViewCellDelegate?

    var favouriteItemCellViewModel : FavouriteItemCellViewModel? {
        didSet {
            
            if let favouriteItemID: Int = self.favouriteItemCellViewModel?.itemID {
                clearButton.tag = favouriteItemID
            }
            if "Lang".localized == "en" {
                if let favouriteItemName = self.favouriteItemCellViewModel?.favouriteItemNameEn {
                    storeNameLabel.text = favouriteItemName
                }
            } else if "Lang".localized == "ar" {
                if let favouriteItemName = self.favouriteItemCellViewModel?.favouriteItemName {
                    storeNameLabel.text = favouriteItemName
                }
            }
            if let favouriteItemRate = self.favouriteItemCellViewModel?.favouriteItemRating {
//                delegateRateNumberLabel.text = favouriteItemRate
            }
            if let favouriteItemImage = self.favouriteItemCellViewModel?.favouriteItemIcon {
                storeImageView.loadImageFromUrl(imageUrl: ImageURLSecondSubervices + favouriteItemImage)
            }
            
        }
    }

    @IBOutlet weak var storeImageView: UIImageView!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func itemFavouriteButtonPressed(itemID: Int) {
        if let delegateValue = favouritedelegate {
            delegateValue.itemFavouriteButtonPressed(itemID: itemID)
        }
    }
    
    @IBAction func clearFavouriteItemButtonIsPressed(_ sender: Any) {
        itemFavouriteButtonPressed(itemID: self.clearButton.tag)
    }
    
}
