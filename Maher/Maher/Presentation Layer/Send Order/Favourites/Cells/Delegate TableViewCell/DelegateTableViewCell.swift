//
//  DelegateTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol FavouriteButtonTableViewCellDelegate {
    func delegateFavouriteButtonPressed(providerID: Int)
    func itemFavouriteButtonPressed(itemID: Int)
}

class DelegateTableViewCell: UITableViewCell {
    
    var favouritedelegate: FavouriteButtonTableViewCellDelegate?
    
    var favouriteProviderCellViewModel : FavouriteProviderCellViewModel? {
        didSet {
            
            if let providerID: Int = favouriteProviderCellViewModel?.providerID {
                clearButton.tag = providerID
            }
            if let providerName = self.favouriteProviderCellViewModel?.providerName {
                delegateNameLabel.text = providerName
            }
            if let delegateRate = self.favouriteProviderCellViewModel?.providerRating {
                delegateRateNumberLabel.text = delegateRate
            }
            if let delegateImage = self.favouriteProviderCellViewModel?.providerImage {
                delegateImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + delegateImage)
            }
            
        }
    }
    
    @IBOutlet weak var delegateImageView: UIImageView!
    @IBOutlet weak var delegateNameLabel: UILabel!
    @IBOutlet weak var delegateRateNumberLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureView() {
        delegateImageView.addCornerRadius(raduis: delegateImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func delegateFavouriteButtonPressed(providerID: Int) {
        if let delegateValue = favouritedelegate {
            delegateValue.delegateFavouriteButtonPressed(providerID: providerID)
        }
    }

    @IBAction func clearButtonIsPressed(_ sender: Any) {
        delegateFavouriteButtonPressed(providerID: clearButton.tag)
    }
}
