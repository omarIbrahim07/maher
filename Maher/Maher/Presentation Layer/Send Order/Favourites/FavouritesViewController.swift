//
//  FavouritesViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class FavouritesViewController: BaseViewController {

    lazy var viewModel: FavouritesViewModel = {
        return FavouritesViewModel()
    }()
    
    var error: APIError?

    var viewChoosed: Int? = 1
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var storesMarkView: UIView!
    @IBOutlet weak var delegatesMarkView: UIView!
    @IBOutlet weak var storesLabel: UILabel!
    @IBOutlet weak var delegatesLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.fetchFavouriteItems()
        viewModel.fetchFavouriteProviders()
    }

    
    func configureView() {
        storesMarkView.isHidden = false
        delegatesMarkView.isHidden = true
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "StoreTableViewCell", bundle: nil), forCellReuseIdentifier: "StoreTableViewCell")
        tableView.register(UINib(nibName: "DelegateTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - Navigation
    
    
    // MARK:- Actions
    @IBAction func storesButtonIsPressed(_ sender: Any) {
        storesMarkView.isHidden = false
        delegatesMarkView.isHidden = true
        viewChoosed = 1
//        self.viewModel.fetchFavouriteItems()
        tableView.reloadData()
    }
    
    @IBAction func delegatesButtonIsPressed(_ sender: Any) {
        delegatesMarkView.isHidden = false
        storesMarkView.isHidden = true
        viewChoosed = 2
//        self.viewModel.initFetch()
        tableView.reloadData()
    }

}

