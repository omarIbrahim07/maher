//
//  FavouriteProviderCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct FavouriteProviderCellViewModel {
    let id: Int?
    let userID: Int?
    let providerID: Int?
    let createdAt: String?
    let updatedAt: String?
    let providerName: String?
    let providerImage: String?
    let providerRating: String?
}
