//
//  FavouritesViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class FavouritesViewModel {
    
    private var favouriteProviders: [FavouriteProvider] = [FavouriteProvider]()
    var selectedFavouriteProvider: FavouriteProvider?
    private var favouriteItems: [FavouriteItem] = [FavouriteItem]()
    var selectedFavouriteItem: FavouriteItem?
    
    var error: APIError?
    
    var favouriteDelegatecellViewModels: [FavouriteProviderCellViewModel] = [FavouriteProviderCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var favouriteItemCellViewModels: [FavouriteItemCellViewModel] = [FavouriteItemCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfFavouroteDelegatesCells: Int {
        return favouriteDelegatecellViewModels.count
    }
    
    var numberOfFavouriteItemsCells: Int {
        return favouriteItemCellViewModels.count
    }
    
    func fetchFavouriteProviders() {
        state = .loading
        
        DelegatesOffersAPIManager().getFavouriteProviders(basicDictionary: [:], onSuccess: { (favouriteProviders) in
            
            self.processFetchedDelegates(favouriteProviders: favouriteProviders)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchFavouriteItems() {
        state = .loading
        
        DelegatesOffersAPIManager().getFavouriteItems(basicDictionary: [:], onSuccess: { (favouriteitems) in
            
            self.processFetchedItems(favouriteItems: favouriteitems)
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    
    func removeProviderToFavourites(providerID: Int, action: Int) {
        
        let parameters: [String : AnyObject] = [
            "provider_id" : providerID as AnyObject,
            "action" : action as AnyObject,
        ]
        
        state = .loading
        
        DelegatesOffersAPIManager().favouriteProvider(basicDictionary: parameters , onSuccess: { (saved) in
            
            self.state = .populated
            self.fetchFavouriteProviders()
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
        
    }
    
    func removeItemToFavourites(itemID: Int, action: Int) {
        
        let parameters: [String : AnyObject] = [
            "item_id" : itemID as AnyObject,
            "action" : action as AnyObject,
        ]
        
        state = .loading
        
        DelegatesOffersAPIManager().favouriteItem(basicDictionary: parameters , onSuccess: { (saved) in
            
            self.state = .populated
            self.fetchFavouriteItems()
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    
    
    func processFetchedDelegates( favouriteProviders: [FavouriteProvider] ) {
        self.favouriteProviders = favouriteProviders // Cache
        var vms = [FavouriteProviderCellViewModel]()
        for favouriteProvider in favouriteProviders {
            vms.append( createFavouriteDelegateCellViewModel(favouriteProvider: favouriteProvider) )
        }
        self.favouriteDelegatecellViewModels = vms
    }
    
    func processFetchedItems( favouriteItems: [FavouriteItem] ) {
        self.favouriteItems = favouriteItems // Cache
        var vms = [FavouriteItemCellViewModel]()
        for favouriteItem in favouriteItems {
            vms.append( createFavouriteItemCellViewModel(favouriteItem: favouriteItem) )
        }
        self.favouriteItemCellViewModels = vms
    }
    
    func createFavouriteDelegateCellViewModel( favouriteProvider: FavouriteProvider ) -> FavouriteProviderCellViewModel {
        
        return FavouriteProviderCellViewModel(id: favouriteProvider.id, userID: favouriteProvider.userID, providerID: favouriteProvider.providerID, createdAt: favouriteProvider.createdAt, updatedAt: favouriteProvider.updatedAt, providerName: favouriteProvider.providerName, providerImage: favouriteProvider.providerImage, providerRating: favouriteProvider.providerRating)
    }
    
    func createFavouriteItemCellViewModel( favouriteItem: FavouriteItem ) -> FavouriteItemCellViewModel {
        
        return FavouriteItemCellViewModel(id: favouriteItem.id, userID: favouriteItem.userID, itemID: favouriteItem.itemID, createdAt: favouriteItem.createdAt, updatedAt: favouriteItem.updatedAt, favouriteItemID: favouriteItem.favouriteItemDetails?.id, favouriteItemName: favouriteItem.favouriteItemDetails?.name, favouriteItemNameEn: favouriteItem.favouriteItemDetails?.nameEn, favouriteItemIcon: favouriteItem.favouriteItemDetails?.icon, favouriteItemCover: favouriteItem.favouriteItemDetails?.cover, favouriteItemRating: favouriteItem.favouriteItemDetails?.rating)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getFavouriteDelegateCellViewModel( at indexPath: IndexPath ) -> FavouriteProviderCellViewModel {
        return favouriteDelegatecellViewModels[indexPath.row]
    }
    
    func getFavouriteItemCellViewModel( at indexPath: IndexPath ) -> FavouriteItemCellViewModel {
        return favouriteItemCellViewModels[indexPath.row]
    }
    
    func favouriteDelegateuserPressed( at indexPath: IndexPath ){
        let favouriteProvider = self.favouriteProviders[indexPath.row]
        
        self.selectedFavouriteProvider = favouriteProvider
    }
    
    func favouriteItemUserPressed( at indexPath: IndexPath ){
        let favouriteItem = self.favouriteItems[indexPath.row]
        
        self.selectedFavouriteItem = favouriteItem
    }
    
    
}
