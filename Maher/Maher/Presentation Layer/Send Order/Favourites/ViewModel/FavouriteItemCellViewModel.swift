//
//  FavouriteItemCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct FavouriteItemCellViewModel {
    let id: Int?
    let userID: Int?
    let itemID: Int?
    let createdAt: String?
    let updatedAt: String?
    let favouriteItemID: Int?
    let favouriteItemName: String?
    let favouriteItemNameEn: String?
    let favouriteItemIcon: String?
    let favouriteItemCover: String?
    let favouriteItemRating: String?
}
