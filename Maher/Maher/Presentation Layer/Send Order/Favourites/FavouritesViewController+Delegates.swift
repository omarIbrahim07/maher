//
//  FavouritesViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension FavouritesViewController: FavouriteButtonTableViewCellDelegate {
    func itemFavouriteButtonPressed(itemID: Int) {
        self.viewModel.removeItemToFavourites(itemID: itemID, action: 1)
    }
    
    func delegateFavouriteButtonPressed(providerID: Int) {
        self.viewModel.removeProviderToFavourites(providerID: providerID, action: 1)
    }
}

extension FavouritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewChoosed == 1 {
            return viewModel.numberOfFavouriteItemsCells
        } else if viewChoosed == 2 {
            return viewModel.numberOfFavouroteDelegatesCells
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewChoosed == 1 {
            if let cell: StoreTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StoreTableViewCell") as? StoreTableViewCell {
                
                let cellVM = viewModel.getFavouriteItemCellViewModel(at: indexPath)
                cell.favouriteItemCellViewModel = cellVM
                cell.favouritedelegate = self

                return cell
            }

        } else if viewChoosed == 2 {
            if let cell: DelegateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateTableViewCell") as? DelegateTableViewCell {
                
                let cellVM = viewModel.getFavouriteDelegateCellViewModel( at: indexPath )
                cell.favouriteProviderCellViewModel = cellVM
                cell.favouritedelegate = self

                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.goToSecondSubcategories()
    }
    
}

extension FavouritesViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
