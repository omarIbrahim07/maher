//
//  DelegateOrderDescriptionViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateOrderDescriptionViewController: BaseViewController {

    lazy var viewModel: DelegateOrderDescriptionViewModel = {
        return DelegateOrderDescriptionViewModel()
    }()
    
    var error: APIError?

    var orderID: Int?
    var clientName: String?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
        
        if let clientName: String = self.clientName {
            clientNameLabel.text = clientName
        }
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("BBBBB")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        if let longitude = providerCurrentLongitude, let latitude = providerCurrentLatitude, let orderID: Int = self.orderID {
            self.viewModel.initFetch(longitude: String(longitude), latitude: String(latitude), orderID: String(orderID))
        }
    }

    func bindData(clientName: String) {
        
    }
    
    func configureView() {
        
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "DelegateOrderDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateOrderDescriptionTableViewCell")
        tableView.register(UINib(nibName: "DelegateDeliveryPlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateDeliveryPlaceTableViewCell")
        tableView.register(UINib(nibName: "DelegateDistanceTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateDistanceTableViewCell")
        tableView.register(UINib(nibName: "DelegateEnterPriceTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateEnterPriceTableViewCell")
        tableView.register(UINib(nibName: "DelegateDeliveryPackageTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateDeliveryPackageTableViewCell")
        tableView.register(UINib(nibName: "DelegateTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateTimeTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func sendProviderOffer() {
        guard let orderID: Int = self.orderID, orderID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الوصف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let price: String = self.viewModel.cost, price.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال سعر الخدمة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let longitude: Double = providerCurrentLongitude, longitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let latitude: Double = providerCurrentLatitude, latitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        self.viewModel.sendProviderOffer(orderID: orderID, orderPrice: price, longitude: String(longitude), latitude: String(latitude))
    }
    
    // MARK: - Navigation

}

