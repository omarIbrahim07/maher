//
//  DelegateTimeTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateTimeTableViewCell: UITableViewCell {

    var delegateTimeCellViewModel : DelegateTimeCellViewModel? {
        didSet {
//            if let clientName: String = photoListCellViewModel?.clientName {
//                clientNameLabel.text = clientName
//            }
        }
    }

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.backgroundColor = #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1)
        outerView.roundCorners(.topRight, radius: 60)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
