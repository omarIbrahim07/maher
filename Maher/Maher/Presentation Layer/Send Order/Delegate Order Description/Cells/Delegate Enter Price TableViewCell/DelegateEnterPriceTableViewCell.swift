//
//  DelegateEnterPriceTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol DelegateEnterPriceTextfieldPressed {
    func delegateEnterPriceTextfieldPressed(cost: String)
    func sendPriceButtonIsPressed()
}

class DelegateEnterPriceTableViewCell: UITableViewCell {

    var delegate: DelegateEnterPriceTextfieldPressed?
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var textFieldContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        outerView.roundCorners(.topRight, radius: 60)
        textFieldContainerView.addCornerRadius(raduis: textFieldContainerView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        textFieldContainerView.delegate = self
        priceTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        priceTextField.roundCorners(.allCorners , radius: priceTextField.frame.height / 2)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        delegateEnterPriceTextfieldPressed(cost: textField.text!)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        textFieldContainerView.endEditing(true)
    }
    
    func delegateEnterPriceTextfieldPressed(cost: String) {
        if let delegateValue = delegate {
            delegateValue.delegateEnterPriceTextfieldPressed(cost: cost)
        }
    }
    
    func sendPriceButtonIsPressed() {
        if let delegateValue = delegate {
            delegateValue.sendPriceButtonIsPressed()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func sendButtonIsPressed(_ sender: Any) {
        sendPriceButtonIsPressed()
    }
    
}
