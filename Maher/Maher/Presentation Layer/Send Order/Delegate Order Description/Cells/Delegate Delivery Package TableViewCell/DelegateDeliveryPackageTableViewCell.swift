//
//  DelegateDeliveryPackageTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateDeliveryPackageTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        outerView.roundCorners(.topRight, radius: 60)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
