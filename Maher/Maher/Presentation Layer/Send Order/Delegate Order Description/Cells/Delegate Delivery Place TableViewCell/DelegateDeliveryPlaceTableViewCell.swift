//
//  DelegateDeliveryPlaceTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateDeliveryPlaceTableViewCell: UITableViewCell {

    var delegateDescriptionCellViewModel : DelegateOrderDescriptionCellViewModel? {
        didSet {
//            if let orderDescription: String = delegateDescriptionCellViewModel?.orderDescription {
//                orderDescriptionValueLabel.text = orderDescription
//            }
        }
    }

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var storeLocationLabel: UILabel!
    @IBOutlet weak var deliveryPlaceLabel: UILabel!
    @IBOutlet weak var deliveryPlaceValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        outerView.roundCorners(.topRight, radius: 60)
    }

}
