//
//  DelegateDeliveryPlaceCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegateDeliveryPlaceCellViewModel {
    let pickLatitude: String?
    let pickLongitude: String?
    let destLatitude: String?
    let destLongitude: String?
    let placeName: String?
}
