//
//  DelegateOrderDescriptionViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class DelegateOrderDescriptionViewModel {
    
    var error: APIError?
    var cost: String?
                        
    var delegateOrderDescriptionCellViewModel: DelegateOrderDescriptionCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var delegateDeliveryPlaceCellViewModel: DelegateDeliveryPlaceCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var delegateDistanceCellViewModel: DelegateDistanceCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
                
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var check: Bool? = false
        
    var numberOfCells: Int {
        if self.check == true {
            return 4
        }
        return 0
     }

    func initFetch(longitude: String, latitude: String, orderID: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "order_id"     : orderID as AnyObject,
            "latitude"     : latitude as AnyObject,
            "longitude"    : longitude as AnyObject
        ]
        
        DelegatesOffersAPIManager().getDelegateNewOrderDetails(basicDictionary: params, onSuccess: { (delegateNewOrder) in
            
            self.check = true
            
            self.delegateOrderDescriptionCellViewModel = self.createDelegateOrderDescriptionCellViewModel(delegateNewOrder: delegateNewOrder)
            self.delegateDeliveryPlaceCellViewModel = self.createDelegateDeliveryPlaceCellViewModel(delegateNewOrder: delegateNewOrder)
            self.delegateDistanceCellViewModel = self.createDelegateDistanceCellViewModel(delegateNewOrder: delegateNewOrder)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func sendProviderOffer(orderID: Int, orderPrice: String, longitude: String, latitude: String) {
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
            "cost" : orderPrice as AnyObject,
            "orderoffer_long" : longitude as AnyObject,
            "orderoffer_lat" : latitude as AnyObject,
        ]
        
        self.state = .loading
        
        DelegatesOffersAPIManager().delegateSendOffer(basicDictionary: params, onSuccess: { (saved) in
            if saved != nil {
                print("Success")
                self.state = .populated
//                self.sendingOrder = .success
            } else {
                self.state = .empty
//                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
//            self.sendingOrder = .failed
        }

    }
    
    func createDelegateOrderDescriptionCellViewModel( delegateNewOrder: DelegateNewOrder ) -> DelegateOrderDescriptionCellViewModel {
        //Wrap a description
        return DelegateOrderDescriptionCellViewModel(orderDescription: delegateNewOrder.description)
    }
    
    func createDelegateDeliveryPlaceCellViewModel( delegateNewOrder: DelegateNewOrder ) -> DelegateDeliveryPlaceCellViewModel {
        //Wrap a description
        return DelegateDeliveryPlaceCellViewModel(pickLatitude: delegateNewOrder.pickLatitude, pickLongitude: delegateNewOrder.pickLongitude, destLatitude: delegateNewOrder.desLatitude, destLongitude: delegateNewOrder.desLongitude, placeName: "Store Location")
    }
    
    func createDelegateDistanceCellViewModel( delegateNewOrder: DelegateNewOrder ) -> DelegateDistanceCellViewModel {
        //Wrap a description
//        return DelegateDistanceCellViewModel(youStroeDistance: <#T##Int?#>, storeClientDistance: <#T##Int?#>, youStoreDuration: <#T##String?#>, storeClientDuration: <#T##String?#>)
        return DelegateDistanceCellViewModel (youStroeDistance: 10, storeClientDistance: 10, youStoreDuration: "10", storeClientDuration: "10")
    }

        
    func getError() -> APIError {
        return self.error!
    }

    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }
            
    func getDelegateOrderDescriptionCellViewModel() -> DelegateOrderDescriptionCellViewModel {
        return delegateOrderDescriptionCellViewModel ?? DelegateOrderDescriptionCellViewModel(orderDescription: "No Description")
    }
    
    func getDelegateDeliveryPlaceCellViewModel() -> DelegateDeliveryPlaceCellViewModel {
        return delegateDeliveryPlaceCellViewModel ?? DelegateDeliveryPlaceCellViewModel(pickLatitude: "", pickLongitude: "", destLatitude: "", destLongitude: "", placeName: "")
    }
    
    func getDelegateDistanceCellViewModel() -> DelegateDistanceCellViewModel {
        return delegateDistanceCellViewModel ?? DelegateDistanceCellViewModel(youStroeDistance: 0, storeClientDistance: 0, youStoreDuration: "", storeClientDuration: "")
    }
    
}
