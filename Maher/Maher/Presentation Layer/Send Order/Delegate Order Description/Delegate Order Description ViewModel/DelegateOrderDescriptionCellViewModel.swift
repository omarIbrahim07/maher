//
//  DelegateOrderDescriptionCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegateOrderDescriptionCellViewModel {
    let orderDescription: String?
}
