//
//  DelegateDistanceCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegateDistanceCellViewModel {
    let youStroeDistance: Int?
    let storeClientDistance: Int?
    let youStoreDuration: String?
    let storeClientDuration: String?
}
