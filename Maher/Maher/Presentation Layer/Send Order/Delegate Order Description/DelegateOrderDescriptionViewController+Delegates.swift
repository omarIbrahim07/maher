//
//  DelegateOrderDescriptionViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension DelegateOrderDescriptionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: DelegateOrderDescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateOrderDescriptionTableViewCell") as? DelegateOrderDescriptionTableViewCell {
                
                let cellVM = viewModel.getDelegateOrderDescriptionCellViewModel()
                cell.delegateDescriptionCellViewModel = cellVM
                return cell
            }
        } else if indexPath.row == 1 {
            if let cell: DelegateDeliveryPlaceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateDeliveryPlaceTableViewCell") as? DelegateDeliveryPlaceTableViewCell {
                return cell
            }
        } else if indexPath.row == 2 {
            if let cell: DelegateDistanceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateDistanceTableViewCell") as? DelegateDistanceTableViewCell {
                return cell
            }
        } else if indexPath.row == 3 {
            if let cell: DelegateEnterPriceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateEnterPriceTableViewCell") as? DelegateEnterPriceTableViewCell {
                cell.delegate = self
                return cell
            }
        } else if indexPath.row == 5 {
            if let cell: DelegateDeliveryPackageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateDeliveryPackageTableViewCell") as? DelegateDeliveryPackageTableViewCell {
                return cell
            }
        } else if indexPath.row == 4 {
            if let cell: DelegateTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateTimeTableViewCell") as? DelegateTimeTableViewCell {
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}

extension DelegateOrderDescriptionViewController: DelegateEnterPriceTextfieldPressed {
    func delegateEnterPriceTextfieldPressed(cost: String) {
        print(cost)
        self.viewModel.cost = cost
    }
    func sendPriceButtonIsPressed() {
        self.sendProviderOffer()
    }
}
