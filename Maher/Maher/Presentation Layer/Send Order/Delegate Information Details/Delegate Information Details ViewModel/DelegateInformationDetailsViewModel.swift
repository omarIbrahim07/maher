//
//  DelegateInformationDetailsViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class DelegateInformationDetailsViewModel {

    private var providerData: ProviderData?
    var error: APIError?

//    var selectedTechnician: Technician?
    var providerRatings: [ProviderRating] = [ProviderRating]()

    var delegateInfoViewModel: DelegateInfoViewModel? {
        didSet {
            self.updateProviderDataClosure?()
        }
    }

    var numberOfOrderscellViewModel: NumberOfOrdersCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var ratesCellViewModels: [RatesCellViewModel] = [RatesCellViewModel]() {
         didSet {
             self.reloadTableViewClosure?()
         }
     }

    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateProviderDataClosure: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var numberOfRatesCells: Int {
        return ratesCellViewModels.count
    }

    var numberOfNumberOfOrdersCell: Int {
        if self.providerData != nil {
            return 1
        }
        return 0
     }

    func initFetch(providerID: Int) {
        state = .loading

        let params: [String : AnyObject] = [
            "provider_id" : providerID as AnyObject,
        ]
        
        AuthenticationAPIManager().getProviderProfile(basicDictionary: params, onSuccess: { (providerProfile) in
            self.providerData = providerProfile

            self.delegateInfoViewModel = self.createProviderInfoViewModel(providerData: providerProfile)
            
            if let providerRatings = self.providerData?.providerRatings {
                self.providerRatings = providerRatings
            }

            print(providerProfile)
            if providerProfile.providerRatings?.count ?? 0 > 0 {
                self.processRatedOrders(providerRatings: providerProfile.providerRatings!)
            }
            
            if let numberOfOrders = providerProfile.id {
                self.numberOfOrderscellViewModel = self.createNumberOfOrdersCellViewModel(numberOfOrders: numberOfOrders)
            }

            self.state = .populated

        }) { (error) in
            print(error)
        }
    }

    func processRatedOrders( providerRatings: [ProviderRating] ) {
//        if let workerOrders = self.technicianDetails?.workerOrders {
//            self.workerOrders = workerOrders // Cache
//        }

        var vms = [RatesCellViewModel]()
        for providerRating in providerRatings {
            vms.append( createRatesCellViewModel(providerRating: providerRating) )
        }
        self.ratesCellViewModels = vms
    }
    
    func createRatesCellViewModel( providerRating: ProviderRating ) -> RatesCellViewModel {

        //Wrap a description
        return RatesCellViewModel(id: providerRating.id, userID: providerRating.userID, providerID: providerRating.providerID, userRating: providerRating.userRating, userComment: providerRating.userComment, userName: providerRating.userName, userImage: providerRating.userImage, providerRating: providerRating.providerRating, createdAt: providerRating.createdAt)
    }
    
    func createNumberOfOrdersCellViewModel( numberOfOrders: Int ) -> NumberOfOrdersCellViewModel {

        //Wrap a description
        return NumberOfOrdersCellViewModel(numberOfOrders: numberOfOrders)
    }
    
    func createProviderInfoViewModel( providerData: ProviderData ) -> DelegateInfoViewModel {

        //Wrap a description
        return DelegateInfoViewModel(providerName: providerData.firstName, providerImage: providerData.image, providerRating: providerData.rating)
    }

    func getRatesCellViewModel( at indexPath: IndexPath ) -> RatesCellViewModel {
        return ratesCellViewModels[indexPath.row]
    }
    
    func getNumberOfOrdersCellViewModel() -> NumberOfOrdersCellViewModel? {
        return numberOfOrderscellViewModel
    }
    
    func getDelegateInfoCellViewModel() -> DelegateInfoViewModel? {
        return delegateInfoViewModel
    }

    func getError() -> APIError {
        return self.error!
    }

}

