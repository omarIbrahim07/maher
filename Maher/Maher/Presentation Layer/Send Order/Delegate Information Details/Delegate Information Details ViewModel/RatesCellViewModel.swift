//
//  RatesCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct RatesCellViewModel {
    let id: Int?
    let userID: Int?
    let providerID: String?
    let userRating: Int?
    let userComment: String?
    let userName: String?
    let userImage: String?
    let providerRating: Int?
    let createdAt: String?
}

