//
//  DelegateInfoViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegateInfoViewModel {
    let providerName: String?
    let providerImage: String?
    let providerRating: String?
}

