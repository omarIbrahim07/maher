//
//  DelegateInformationDetailsViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class DelegateInformationDetailsViewController: BaseViewController {

    lazy var viewModel: DelegateInformationDetailsViewModel = {
        return DelegateInformationDetailsViewModel()
    }()
    
    var error: APIError?
    var delegateinfoViewModel: DelegateInfoViewModel?

    var providerID: Int?
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var delegateImageView: UIImageView!
    @IBOutlet weak var delegateNameLabel: UILabel!
    @IBOutlet weak var delegateRatingCosmosView: CosmosView!
    @IBOutlet weak var delegateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateProviderDataClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.delegateinfoViewModel = self?.viewModel.getDelegateInfoCellViewModel()
                self?.bindProviderProfile(delegateInfoViewModel: self!.delegateinfoViewModel!)
            }
        }
        
        if let providerID = self.providerID {
            self.viewModel.initFetch(providerID: providerID)
        }
    }
    
    func configureView() {
        delegateImageView.addCornerRadius(raduis: delegateImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "RatesTableViewCell", bundle: nil), forCellReuseIdentifier: "RatesTableViewCell")
        tableView.register(UINib(nibName: "NumberOfOrdersTableViewCell", bundle: nil), forCellReuseIdentifier: "NumberOfOrdersTableViewCell")
        tableView.register(UINib(nibName: "CellView", bundle: nil), forHeaderFooterViewReuseIdentifier: CellView.reuseIdentifier)
        tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(40))
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func bindProviderProfile(delegateInfoViewModel: DelegateInfoViewModel) {
        if let providerImage = delegateInfoViewModel.providerImage {
            delegateImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + providerImage)
        }
        if let providerName = delegateInfoViewModel.providerName {
            delegateNameLabel.text = providerName
        }
        if let providerRating = delegateInfoViewModel.providerRating {
            delegateRatingCosmosView.rating = Double(providerRating) as! Double
        }
    }

    // MARK: - Navigation

}
