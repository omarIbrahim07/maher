//
//  DelegateInformationDetails+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension DelegateInformationDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.numberOfNumberOfOrdersCell
        } else if section == 1 {
            return viewModel.numberOfRatesCells
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "number of orders section".localized
                return headerView
            }
        }
        if section == 1 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "rates section".localized
                return headerView
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40  // or whatever
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let cell: NumberOfOrdersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NumberOfOrdersTableViewCell", for: indexPath) as? NumberOfOrdersTableViewCell {
//                cell.delegate = self
                let cellVM = viewModel.getNumberOfOrdersCellViewModel()
                cell.numberOfOrdersCellViewModel = cellVM
                
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell: RatesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RatesTableViewCell", for: indexPath) as? RatesTableViewCell {
                
                let cellVm = viewModel.getRatesCellViewModel(at: indexPath)
                cell.rateListCellViewModel = cellVm
                
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension DelegateInformationDetailsViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
