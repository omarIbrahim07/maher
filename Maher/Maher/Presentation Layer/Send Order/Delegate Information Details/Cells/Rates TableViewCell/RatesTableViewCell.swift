//
//  RatesTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class RatesTableViewCell: UITableViewCell {

    var rateListCellViewModel : RatesCellViewModel? {
        didSet {

            if let clientName = self.rateListCellViewModel?.userName {
                clientNameLabel.text = clientName
            }
            
            if let rate: Int = self.rateListCellViewModel?.userRating {
                clientRateView.rating = Double(rate)
            }

            if let comment: String = self.rateListCellViewModel?.userComment {
                clientCommentLabel.text = comment
            }

            if let createdAt: String = rateListCellViewModel?.createdAt {
                createdAtLabel.text = createdAt
            }

            if let image: String = self.rateListCellViewModel?.userImage {
                clientImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
            }

        }
    }
    
    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var clientRateView: CosmosView!
    @IBOutlet weak var clientCommentLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure() {
        clientImage.layer.cornerRadius = clientImage.frame.height / 2
    }

}
