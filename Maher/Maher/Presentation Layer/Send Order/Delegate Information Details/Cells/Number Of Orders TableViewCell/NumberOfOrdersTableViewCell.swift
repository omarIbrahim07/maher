//
//  NumberOfOrdersTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class NumberOfOrdersTableViewCell: UITableViewCell {

    var numberOfOrdersCellViewModel : NumberOfOrdersCellViewModel? {
        didSet {
            if let numberOfOrders = self.numberOfOrdersCellViewModel?.numberOfOrders {
                numberOfOrdersLabel.text = String(numberOfOrders)
            }
        }
    }
    
    @IBOutlet weak var numberOfOrdersLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
