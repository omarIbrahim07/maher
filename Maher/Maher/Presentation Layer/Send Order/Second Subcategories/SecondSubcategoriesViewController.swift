//
//  SecondSubcategoriesViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SecondSubcategoriesViewController: BaseViewController {
    
    lazy var viewModel: SecondSubcategoriesViewModel = {
        return SecondSubcategoriesViewModel()
    }()
    
    var error: APIError?
    var selectedService: Service?
    var viewChoosed: Int? = 1
    var selectedSubservice: Subservice?
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var secSubcategoryMarkView: UIView!
    @IBOutlet weak var offersMarkView: UIView!
    @IBOutlet weak var secSubcategoryLabel: UILabel!
    @IBOutlet weak var offersLabel: UILabel!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
        initVM()
    }
    
    func configureView() {
        secSubcategoryMarkView.isHidden = false
        offersMarkView.isHidden = true
        searchBar.isHidden = false
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        searchBar.delegate = self
        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "SecondSubcategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "SecondSubcategoryTableViewCell")
        tableView.register(UINib(nibName: "OfferTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        if let subserviceID = selectedSubservice?.id {
            viewModel.initFetch(forSubCategoryId: subserviceID)
        }
    }
    
    func setNavigationTitle() {
        if let selectedSubserviceTitle: String = selectedSubservice?.nameEN {
            navigationItem.title = selectedSubserviceTitle
        }
        if "lang".localized == "en" {
            if let selectedSubserviceTitle: String = selectedSubservice?.nameEN {
                navigationItem.title = selectedSubserviceTitle
            }
        } else if "lang".localized == "ar" {
            if let selectedSubserviceTitle: String = selectedSubservice?.name {
                navigationItem.title = selectedSubserviceTitle
            }
        }
    }

    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Navigation
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }
    
    func goToSecondSubcategories(forSecondSelectedSubservice secondSelectedSubservice: SecondSubservice) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "StoreDetailsViewController") as! StoreDetailsViewController
        viewController.storeDetails = secondSelectedSubservice

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK:- Actions
    @IBAction func secondSubcategoryButtonIsPressed(_ sender: Any) {
        secSubcategoryMarkView.isHidden = false
        offersMarkView.isHidden = true
        searchBar.isHidden = false
        searchBarHeight.constant = 50
        viewChoosed = 1
        backgroundImageView.isHidden = false
        tableView.reloadData()
    }
    
    @IBAction func offersButtonIsPressed(_ sender: Any) {
        offersMarkView.isHidden = false
        secSubcategoryMarkView.isHidden = true
        searchBar.isHidden = true
        searchBarHeight.constant = 0
        viewChoosed = 2
        backgroundImageView.isHidden = true
        tableView.reloadData()
    }
    

}
