//
//  SecondSubcategoryTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SecondSubcategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subcategoryImageView: UIImageView!
    @IBOutlet weak var subcategoryTitleLabel: UILabel!
    @IBOutlet weak var subcategorydistanceLabel: UILabel!

    var photoListCellViewModel : SecondSubcategoryCellViewModel? {
        didSet {
            
            subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryNameEn
            
            if "lang".localized == "en" {
                subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryNameEn
            } else if "lang".localized == "ar" {
                subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryName
            }
            
            if let imagePathURL: String = photoListCellViewModel?.icon {
                subcategoryImageView.loadImageFromUrl(imageUrl: ImageURLSecondSubervices + imagePathURL)
            }

//            subcategoryNumberLabel.text = photoListCellViewModel?.
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
