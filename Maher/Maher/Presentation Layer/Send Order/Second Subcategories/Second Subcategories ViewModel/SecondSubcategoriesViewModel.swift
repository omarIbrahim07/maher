//
//  SecondSubcategoriesViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

class SecondSubcategoriesViewModel {
    
    var selectedSecondSubervice: SecondSubservice?
    
    var secondSubservices: [SecondSubservice] = [SecondSubservice]()
    var error: APIError?
    
    var searchSecondSubcategories = [SecondSubservice]()
    var searching = false
    
    var cellViewModels: [SecondSubcategoryCellViewModel] = [SecondSubcategoryCellViewModel]() {
       didSet {
           self.reloadTableViewClosure?()
       }
   }
    
     var searchedCellViewModels: [SecondSubcategoryCellViewModel] = [SecondSubcategoryCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var reloadOffersPagerView: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
            
    func initFetch(forSubCategoryId subcategoryId: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        ServicesAPIManager().getSecondSubservices(forSubservice: subcategoryId, basicDictionary: params, onSuccess: { (secondSubservices) in
            
            self.processFetchedPhoto(secondSubservices: secondSubservices)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func createCellViewModel( secondSubservice: SecondSubservice ) -> SecondSubcategoryCellViewModel {

        //Wrap a description
        var descTextContainer: [String] = [String]()
//        if let camera = secondSubservices. {
//            descTextContainer.append(camera)
//        }
//
//        if let description = secondSubservices.image {
//            descTextContainer.append( description )
//        }
        
        if "Lang".localized == "ar" {
            return SecondSubcategoryCellViewModel(subcategoryId: secondSubservice.id, subcategoryName: secondSubservice.name, subcategoryNameEn: secondSubservice.nameEN, icon: secondSubservice.icon, cover: secondSubservice.cover, rating: secondSubservice.rating, longitude: secondSubservice.longitude, latitude: secondSubservice.latitude, description: secondSubservice.description, descriptionEn: secondSubservice.descriptionEn, openFrom: secondSubservice.openFrom, openTo: secondSubservice.openTo, location: secondSubservice.location)
        } else if "Lang".localized == "en" {
            return SecondSubcategoryCellViewModel(subcategoryId: secondSubservice.id, subcategoryName: secondSubservice.name, subcategoryNameEn: secondSubservice.nameEN, icon: secondSubservice.icon, cover: secondSubservice.cover, rating: secondSubservice.rating, longitude: secondSubservice.longitude, latitude: secondSubservice.latitude, description: secondSubservice.description, descriptionEn: secondSubservice.descriptionEn, openFrom: secondSubservice.openFrom, openTo: secondSubservice.openTo, location: secondSubservice.location)
        }
        
        return SecondSubcategoryCellViewModel(subcategoryId: secondSubservice.id, subcategoryName: secondSubservice.name, subcategoryNameEn: secondSubservice.nameEN, icon: secondSubservice.icon, cover: secondSubservice.cover, rating: secondSubservice.rating, longitude: secondSubservice.longitude, latitude: secondSubservice.latitude, description: secondSubservice.description, descriptionEn: secondSubservice.descriptionEn, openFrom: secondSubservice.openFrom, openTo: secondSubservice.openTo, location: secondSubservice.location)
    }
    
    func processFetchedPhoto( secondSubservices: [SecondSubservice] ) {
        self.secondSubservices = secondSubservices // Cache
        var vms = [SecondSubcategoryCellViewModel]()
        for secondSubservice in secondSubservices {
            vms.append( createCellViewModel(secondSubservice: secondSubservice) )
        }
        self.cellViewModels = vms
    }
        
    func processSearchingSecondSubcategories( secondSubservices: [SecondSubservice] ) {
        self.searchSecondSubcategories = secondSubservices // Cache
        var vms = [SecondSubcategoryCellViewModel]()
        for secondSubservice in secondSubservices {
            vms.append( createCellViewModel(secondSubservice: secondSubservice) )
        }
        self.searchedCellViewModels = vms
    }
        
    func getCellViewModel( at indexPath: IndexPath ) -> SecondSubcategoryCellViewModel {
        if searching {
            return searchedCellViewModels[indexPath.row]
        } else {
            return cellViewModels[indexPath.row]
        }
    }
    
    func userPressed( at indexPath: IndexPath ){
        if searching {
            let secondSubservice = self.searchSecondSubcategories[indexPath.row]
            self.selectedSecondSubervice = secondSubservice
        } else {
            let secondSubservice = self.secondSubservices[indexPath.row]
            self.selectedSecondSubervice = secondSubservice
        }
    }

    func getError() -> APIError {
        return self.error!
    }
    
}
