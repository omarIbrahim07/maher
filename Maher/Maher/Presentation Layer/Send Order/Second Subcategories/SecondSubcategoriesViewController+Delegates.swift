//
//  SecondSubcategoriesViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SecondSubcategoriesViewController: UISearchBarDelegate {
            
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.searching = true
        self.viewModel.searchSecondSubcategories = viewModel.secondSubservices.filter({ (subcatChar) -> Bool in
            guard let text = searchBar.text else { return false }
            return (subcatChar.nameEN?.contains(text))!
        })
        self.viewModel.processSearchingSecondSubcategories(secondSubservices: self.viewModel.searchSecondSubcategories)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.searching = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
}

extension SecondSubcategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.searching {
            return self.viewModel.searchedCellViewModels.count
        } else {
            return viewModel.cellViewModels.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewChoosed == 1 {
            if let cell: SecondSubcategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SecondSubcategoryTableViewCell") as? SecondSubcategoryTableViewCell {
                
                    let cellVM = viewModel.getCellViewModel( at: indexPath )
                    cell.photoListCellViewModel = cellVM

                return cell
            }
        } else if viewChoosed == 2 {
            if let cell: OfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell") as? OfferTableViewCell {
                
                if indexPath.row % 2 == 0 {
                    cell.outerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.innerView.roundCorners(.bottomRight, radius: 60)
                    cell.innerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.orderButton.addCornerRadius(raduis: cell.orderButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                    cell.orderButton.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.orderButton.setTitleColor(#colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), for: .normal)
                    cell.offerOwnerNameLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.rateValueLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.descriptionLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.delegateLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.youLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.distanceLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.durationLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.firstView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.secondView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                } else if indexPath.row % 2 == 1 {
                    cell.outerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.innerView.roundCorners(.bottomRight, radius: 60)
                    cell.innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                    cell.orderButton.addCornerRadius(raduis: cell.orderButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                    cell.orderButton.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.orderButton.setTitleColor(#colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1), for: .normal)
                    cell.offerOwnerNameLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.rateValueLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.descriptionLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.delegateLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.youLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.distanceLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.durationLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.firstView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                    cell.secondView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.showCantLoginPopUp()
            return
        }
        self.viewModel.userPressed(at: indexPath)
                        
        if let selectedSecondSubservie = self.viewModel.selectedSecondSubervice {
            self.goToSecondSubcategories(forSecondSelectedSubservice: selectedSecondSubservie)
        }
    }
    
}

extension SecondSubcategoriesViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
