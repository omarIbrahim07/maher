//
//  SettingsViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    
//    @IBOutlet weak var logoImageView: UIImageView!
//    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var saveButton: UIButton!
//    @IBOutlet weak var nameTextField: UITextField!
//    @IBOutlet weak var emailTextField: UITextField!
//    @IBOutlet weak var phoneTextField: UITextField!
//    @IBOutlet weak var countryTextField: UIButton!
//    @IBOutlet weak var languageTextField: UIButton!
//    @IBOutlet weak var delegateButton: UIButton!
//    @IBOutlet weak var clientButton: UIButton!
//    @IBOutlet weak var beDelegateLabel: UILabel!
//    @IBOutlet weak var beDelegateButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//    func bindAccountData(user: User) {
//        if let firstName: String = user.firstName {
//            nameTextField.text = firstName
//        }
//        
//        if let phone = user.phone {
//            phoneTextField.text = phone
//        }
//        if let email = user.email {
//            emailTextField.text = email
//        }
//        if let country = user.country {
//            countryTextField.t = country
//        }
////        if let userImagee = user.image {
////            userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + userImagee)
////        }
//    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func viewTapped() {
//        nameTextField.endEditing(true)
//        phoneTextField.endEditing(true)
//        passwordTextField.endEditing(true)
//        confirmPasswordTextField.endEditing(true)
    }

    
    @IBAction func delegateButtonIsPressed(_ sender: Any) {
        print("Delegate")
    }
    
    @IBAction func clientButtonIsPressed(_ sender: Any) {
        print("Client")
    }
    
    @IBAction func saveButtonIsPressed(_ sender: Any) {
        print("Save")
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
