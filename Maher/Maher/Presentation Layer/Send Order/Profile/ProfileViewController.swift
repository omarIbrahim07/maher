//
//  ProfileViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class ProfileViewController: BaseViewController {
    
    lazy var viewModel: ProfileViewModel = {
       return ProfileViewModel()
    }()
    
    var user: User?
    var myImage: UIImage? = UIImage()
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userRateCosmosView: CosmosView!
    @IBOutlet weak var myWalletLabel: UILabel!
    @IBOutlet weak var walletValueLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var feesValueLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var ordersValueLabel: UILabel!
//    @IBOutlet weak var userCommentsLabel: UILabel!
//    @IBOutlet weak var userCommentsValueLabel: UILabel!
    @IBOutlet weak var serviceRateLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var contactUsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        
        user = viewModel.initAccountData()
        self.getUserProfile()
        
        if let user = UserDefaultManager.shared.currentUser {
            bindAccountData(user: user)
        }
    }
    
    func bindAccountData(user: User) {
        if let firstName: String = user.firstName {
            userNameLabel.text = firstName
        }
        userRateCosmosView.rating = 0
        if let userRating = user.rating {
            userRateCosmosView.rating = Double(userRating)!
        }
        if let userImagee = UserDefaultManager.shared.currentUser?.image {
            profileImageView.addCornerRadius(raduis: profileImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            profileImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + userImagee)
        }
        if let balance = user.balance {
            if balance.count == 1 {
                walletValueLabel.text = "0" + balance + " EGP"
            } else {
                walletValueLabel.text = balance + " EGP"
            }
        }
        if let deliveryCosts = user.deliveryCosts {
            if deliveryCosts <= 9 {
                feesValueLabel.text = "0" + String(deliveryCosts) + " EGP"
            } else {
                feesValueLabel.text = String(deliveryCosts) + " " + "EGP"
            }
        }
        if let ordersCount = user.ordersCount {
            ordersValueLabel.text = String(ordersCount) + " " + "Orders"
        }
        if let commentsCount = user.commentsCounts {
//            userCommentsValueLabel.text = String(commentsCount) + " " + "Comments"
        }
    }
    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    func edit() {
        if myImage?.jpegData(compressionQuality: 0.3) != nil {
            
        let imgData = myImage!.jpegData(compressionQuality: 0.3)
            
            let parameters: [String : AnyObject] = [:]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfileWithImage(imageData: imgData!, basicDictionary: parameters, onSuccess: { (user) in
                
//                UserDefaultManager.shared.currentUser = user
                self.getUserProfile()
//                self.stopLoadingWithSuccess()
//                self.user = user
//                self.bindAccountData(user: user)
                                
                
            }) { (error) in
                self.stopLoadingWithError(error: error)
            }
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
            
            weakSelf?.stopLoadingWithSuccess()
            self.user = user
            
            self.bindAccountData(user: user)
//            if let image = UserDefaultManager.shared.currentUser?.image {
//                self.profileImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
//            }
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func configureView() {
        navigationItem.title = "profile label".localized
        reusableView.frame = self.viewContainToolBar.bounds
        reusableView.configureBottomView(key: "profile")
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func showCancelPopUp() {
        let alertController = UIAlertController(title: "رفع صورة حساب جديدة", message: "من فضلك اختر صورة جديدة للحساب أولًا!", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "إغلاق", style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Navigation

    // MARK:- Actions
    @IBAction func userCommentsButtonIsPressed(_ sender: Any) {
        print("User Comments")
    }
    
    @IBAction func servicesRateButtonIsPressed(_ sender: Any) {
        print("Services Rate")
    }
    
    @IBAction func settingsButtonIsPressed(_ sender: Any) {
        print("Settings")
    }
    
    @IBAction func contactUsButtonIsPressed(_ sender: Any) {
        print("Contact Us")
    }
    
    @IBAction func profileImageButtonIsPressed(_ sender: Any) {
        print("Profile image")
        imagePressed()
    }
    
    @IBAction func saveImageIsPressed(_ sender: Any) {
        if myImage?.jpegData(compressionQuality: 0.3) == nil {
            self.showCancelPopUp()
        }
        edit()
    }

}
