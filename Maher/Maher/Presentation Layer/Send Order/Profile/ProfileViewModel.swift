//
//  ProfileViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class ProfileViewModel {
    
    var reloadUserData: (()->())?
    
    func initAccountData() -> User {
        if let user = UserDefaultManager.shared.currentUser {
            print(user)
            return user
        }
        return User()
    }
    
}
