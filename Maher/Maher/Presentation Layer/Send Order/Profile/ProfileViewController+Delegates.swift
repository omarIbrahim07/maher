//
//  ProfileViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
            profileImageView.addCornerRadius(raduis: profileImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myImage = image
            self.dismiss(animated: true, completion: nil)
            if myImage?.jpegData(compressionQuality: 0.3) == nil {
                self.showCancelPopUp()
            } else {
                print("ارفع الصورة يا جمال يا عيد")
                edit()
            }
        }
    }
}

extension ProfileViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
