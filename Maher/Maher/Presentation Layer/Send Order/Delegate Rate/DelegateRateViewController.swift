//
//  DelegateRateViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class DelegateRateViewController: BaseViewController {
    
    lazy var viewModel: DelegateRateViewModel = {
        return DelegateRateViewModel()
    }()
    
    var error: APIError?

//    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
//        
//    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var rateOuterView: UIView!
    @IBOutlet weak var rateInnerView: UIView!
    @IBOutlet weak var recommendationOuterView: UIView!
    @IBOutlet weak var recommendationInnerView: UIView!
    @IBOutlet weak var rateCommentView: UIView!
    @IBOutlet weak var recommendationCommentView: UIView!
    @IBOutlet weak var categoryOuterView: UIView!
    @IBOutlet weak var categoryInnerView: UIView!
    @IBOutlet weak var confirmRateButton: UIButton!
    @IBOutlet weak var storesRateCommentView: UIView!
    @IBOutlet weak var delegateRateCosmosView: CosmosView!
    @IBOutlet weak var categortRateCosmosView: CosmosView!
    @IBOutlet weak var delegateRateCommentTextField: UITextField!
    @IBOutlet weak var recommendationsTextField: UITextField!
    @IBOutlet weak var categoryRateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        closeKeypad()
        initVM()
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {

                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                                                    
    }

    
    func configureView() {
        rateOuterView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        rateInnerView.roundCorners(.bottomRight, radius: 60)
        rateInnerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        recommendationOuterView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        recommendationInnerView.roundCorners(.bottomRight, radius: 60)
        recommendationInnerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        categoryOuterView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        categoryInnerView.roundCorners(.bottomRight, radius: 60)
        categoryInnerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        rateCommentView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        recommendationCommentView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        storesRateCommentView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        confirmRateButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        if "Lang".localized == "en" {
            delegateRateCommentTextField.textAlignment = .left
            categoryRateTextField.textAlignment = .left
            recommendationsTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            delegateRateCommentTextField.textAlignment = .right
            categoryRateTextField.textAlignment = .right
            recommendationsTextField.textAlignment = .right
        }

    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        delegateRateCommentTextField.endEditing(true)
        categoryRateTextField.endEditing(true)
        recommendationsTextField.endEditing(true)
    }
    
    func sendRate() {
        
        guard let orderID: Int = self.viewModel.orderID, orderID > 0 else {
            let apiError = APIError()
            apiError.message = "Please enter order id"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let delegateRating: Double = self.delegateRateCosmosView.rating, delegateRating > 0 else {
            let apiError = APIError()
            apiError.message = "Please enter delegate rating"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let delegateRatingComment: String = self.delegateRateCommentTextField.text, delegateRatingComment.count > 0 else {
            let apiError = APIError()
            apiError.message = "Please enter delegate rating comment"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let categoryRating: Double = self.categortRateCosmosView.rating, categoryRating > 0 else {
            let apiError = APIError()
            apiError.message = "Please enter category rating"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let categoryRatingComment: String = self.categoryRateTextField.text, categoryRatingComment.count > 0 else {
            let apiError = APIError()
            apiError.message = "Please enter category rating comment"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        self.viewModel.clientRatesProvider(orderID: orderID, userRating: delegateRating, userComment: delegateRatingComment, categoryRating: categoryRating, categoryComment: categoryRatingComment, extraComment: self.recommendationsTextField.text)

    }
    // MARK: - Navigation
    
    // MARK:- Actions
    @IBAction func confirmRateIsPressed(_ sender: Any) {
        self.sendRate()
    }
    

}
