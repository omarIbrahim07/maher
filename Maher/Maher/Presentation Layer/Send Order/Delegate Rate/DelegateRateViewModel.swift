//
//  DelegateRateViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class DelegateRateViewModel {
    
    var error: APIError?
    
    var orderID: Int?
    
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func clientRatesProvider(orderID: Int, userRating: Double, userComment: String, categoryRating: Double, categoryComment: String, extraComment: String?) {
                
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
            "provider_rating" : userRating as AnyObject,
            "provider_comment" : userComment as AnyObject,
            "category_rating" : categoryRating as AnyObject,
            "category_comment" : categoryComment as AnyObject,
            "extra_comment" : extraComment as AnyObject,
        ]
        
        self.state = .loading
        
        OrderAPIManager().rateOrder(basicDictionary: params, onSuccess: { (rateSaved) in
            if rateSaved == true {
                print("Success")
                self.state = .populated
            } else {
                self.state = .empty
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func providerRatesClient(orderID: Int, providerRating: Double, providerComment: String, categoryRating: Double, categoryComment: String, extraComment: String?) {
                
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
            "user_rating" : providerRating as AnyObject,
            "user_comment" : providerComment as AnyObject,
            "category_rating" : categoryRating as AnyObject,
            "category_comment" : categoryComment as AnyObject,
            "extra_comment" : extraComment as AnyObject,
        ]
        
        self.state = .loading
        
        OrderAPIManager().rateOrder(basicDictionary: params, onSuccess: { (rateSaved) in
            if rateSaved == true {
                print("Success")
                self.state = .populated
            } else {
                self.state = .empty
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }

}
