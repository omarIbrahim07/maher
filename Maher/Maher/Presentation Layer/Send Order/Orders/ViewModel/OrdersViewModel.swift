//
//  OrdersViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class OrdersViewModel {
    
    private var orders: [Order] = [Order]()
    var selectedOrder: Order?
    
    var error: APIError?
    
    var cellViewModels: [OrderCellViewModel] = [OrderCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initFetch() {
        state = .loading
        
        OrderAPIManager().getOrders(basicDictionary: [:], onSuccess: { (orders) in
            
            self.processFetchedPhoto(orders: orders)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processFetchedPhoto( orders: [Order] ) {
          self.orders = orders // Cache
          var vms = [OrderCellViewModel]()
          for order in orders {
              vms.append( createCellViewModel(order: order) )
          }
          self.cellViewModels = vms
      }
    
    func createCellViewModel( order: Order ) -> OrderCellViewModel {
                
        return OrderCellViewModel(id: order.id, orderStatus: order.orderStatus)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> OrderCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func orderPressed( at indexPath: IndexPath ){
        let order = self.orders[indexPath.row]

        self.selectedOrder = order
    }

    
}
