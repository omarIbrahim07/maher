//
//  OrderCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct OrderCellViewModel {
    let id: Int?
    let orderStatus: Int?
//    let orderDetails: String?
//    let userID: Int?
//    let itemId: Int?
//    let subserviceID: Int?
//    let promocode: String?
//    let paymentMethod: Int?
//    let pickLatitude: String?
//    let pickLongitude: String?
//    let desLatitude: String?
//    let orderTime: String?
//    let attachmentFile: String?
//    let providerID: Int?
//    let country: String?
}
