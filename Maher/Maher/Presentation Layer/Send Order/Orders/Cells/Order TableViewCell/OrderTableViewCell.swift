//
//  OrderTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol OrderButtonTableViewCellDelegate {
    func orderButtonPressed(orderID: Int)
}

class OrderTableViewCell: UITableViewCell {
    
    var delegate: OrderButtonTableViewCellDelegate?
    
    var photoListCellViewModel : OrderCellViewModel? {
        didSet {
            if let orderId: Int = photoListCellViewModel?.id {
                print("IDihaya: \(orderId)")
//                stateLabel.text = message
                ordersButton.tag = orderId
            }
            
            if let orderNumber: Int = photoListCellViewModel?.id {
                orderNumberValueLabel.text = String(orderNumber)
            }
            
//            if let delegate: String = photoListCellViewModel?.delegate {
//                delegateValueLabel.text = delegate
//            }
            
            if let orderState: Int = photoListCellViewModel?.orderStatus {
//                orderStateValueLabel.text = ""
            }
            
//            if let service: String = photoListCellViewModel?.service {
//                serviceValueLabel.text = service
//            }
        }
    }


    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var orderNumberTitleLabel: UILabel!
    @IBOutlet weak var delegateTitleLabel: UILabel!
    @IBOutlet weak var orderStateTitleLabel: UILabel!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var orderTitleLabel: UILabel!
    @IBOutlet weak var orderNumberValueLabel: UILabel!
    @IBOutlet weak var delegateValueLabel: UILabel!
    @IBOutlet weak var orderStateValueLabel: UILabel!
    @IBOutlet weak var serviceValueLabel: UILabel!
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var numberOfOrdersView: UIView!
    @IBOutlet weak var numberOfOrdersValueLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var ordersButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        configureView()
        setLocalization()
    }
    
    func configureView() {
        orderView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        numberOfOrdersView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        cancelView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func setLocalization() {
        orderNumberTitleLabel.text = "order number".localized
        delegateTitleLabel.text = "delegate".localized
        orderStateTitleLabel.text = "order status".localized
        serviceTitleLabel.text = "service".localized
        orderTitleLabel.text = "service".localized
        ordersLabel.text = "orders".localized
    }
    
    func orderButtonPressed(orderID: Int) {
        if let delegateValue = delegate {
            delegateValue.orderButtonPressed(orderID: orderID)
        }
    }
    
    @IBAction func ordersButtonIsPressed(_ sender: Any) {
        print("Orders button is pressed")
        print(ordersButton.tag)
        orderButtonPressed(orderID: ordersButton.tag)
    }
    
    @IBAction func cancelButtonIsPressed(_ sender: Any) {
        print("Cancel")
    }
    
}
