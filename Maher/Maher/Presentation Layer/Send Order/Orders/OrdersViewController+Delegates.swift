//
//  OrdersViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/29/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension OrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: OrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell") as? OrderTableViewCell {
            
            let cellVM = viewModel.getCellViewModel( at: indexPath )
            cell.photoListCellViewModel = cellVM
            cell.delegate = self
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.goToSecondSubcategories()
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
//            self.showCantLoginPopUp()
            return
        }
        self.viewModel.orderPressed(at: indexPath)
                
        if let selectedOrder = self.viewModel.selectedOrder {
            if let orderID = selectedOrder.id, let orderStatusID = selectedOrder.orderStatus {
                if orderStatusID == 0 {
                    self.goToOrderDelegateOffers(orderID: orderID)
                } else if orderStatusID == 1 {
                    self.goToChat(orderID: orderID)
                }
            }
        }

    }
    
}

extension OrdersViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}

extension OrdersViewController: OrderButtonTableViewCellDelegate {
    func orderButtonPressed(orderID: Int) {
        self.goToOrderDelegateOffers(orderID: orderID)
    }
}

