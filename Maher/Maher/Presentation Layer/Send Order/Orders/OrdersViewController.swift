//
//  OrdersViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class OrdersViewController: BaseViewController {
    
    lazy var viewModel: OrdersViewModel = {
        return OrdersViewModel()
    }()
    
    let statusEn = ["New orders", "Active orders", "Completed orders"]
    let statusAr = ["الطلبات الجديدة", "الطلبات النشطة", "الطلبات المكتملة"]
    var error: APIError?
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
    
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var arrangeByLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                    
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.initFetch()
    }
    
    func configureView() {
        navigationItem.title = "orders label".localized
        arrangeByLabel.text = "arrange by label".localized
        reusableView.frame = self.viewContainToolBar.bounds
        reusableView.configureBottomView(key: "orders")
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
        
        if "Lang".localized == "en" {
            self.orderStatusLabel.text = statusEn[0]
        } else if "Lang".localized == "ar" {
            self.orderStatusLabel.text = statusAr[0]
        }

    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK:- Show Time Options Picker
    func setupOrdersStatusSheet() {
        
        let sheet = UIAlertController(title: "orders alert title".localized, message: "orders alert message".localized, preferredStyle: .actionSheet)
        if "Lang".localized == "en" {
            for status in statusEn {
                sheet.addAction(UIAlertAction(title: status, style: .default, handler: {_ in
                    //                    self.viewModel.getGender(gender: gender)
                    self.orderStatusLabel.text = status
                }))
            }
        } else if "Lang".localized == "ar" {
            for status in statusAr {
                sheet.addAction(UIAlertAction(title: status, style: .default, handler: {_ in
                    //                    self.viewModel.getGender(gender: gender)
                    self.orderStatusLabel.text = status
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "orders alert cancel button".localized, style: .cancel, handler: nil))
        
        //        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
        
    }
    
    // MARK:- Actions
    @IBAction func ordersStatusButtonIsPressed(_ sender: Any) {
        self.setupOrdersStatusSheet()
    }
    
    // MARK: - Navigation
    func goToOrderDelegateOffers(orderID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DelegatesOffersViewController") as! DelegatesOffersViewController
        viewController.orderID = orderID
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToChat(orderID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        viewController.orderID = orderID
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
