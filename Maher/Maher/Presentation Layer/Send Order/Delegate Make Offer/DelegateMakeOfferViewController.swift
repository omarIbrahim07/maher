//
//  DelegateMakeOfferViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateMakeOfferViewController: BaseViewController {

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var textViewContainerView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    func configureView() {
    innerView.roundCorners(.bottomLeft, radius: 60)
    textViewContainerView.roundCorners([.bottomRight, .topRight, .topLeft], radius: 30)
    textViewContainerView.addCornerRadius(raduis: 30, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 1)
        addButton.addCornerRadius(raduis: addButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
