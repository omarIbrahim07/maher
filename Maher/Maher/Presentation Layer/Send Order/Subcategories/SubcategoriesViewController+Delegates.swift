//
//  SubcategoriesViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SubcategoriesViewController: UISearchBarDelegate {
            
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.searching = true
        self.viewModel.searchSubcategories = viewModel.subservices.filter({ (subcatChar) -> Bool in
            guard let text = searchBar.text else { return false }
            return (subcatChar.nameEN?.contains(text))!
        })
        self.viewModel.processSearchingSubcategories(subservices: self.viewModel.searchSubcategories)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.searching = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
}

extension SubcategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.searching {
            return self.viewModel.searchedCellViewModels.count
        } else {
            return viewModel.cellViewModels.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SubcategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubcategoriesTableViewCell") as? SubcategoriesTableViewCell {
            
                let cellVM = viewModel.getCellViewModel( at: indexPath )
                cell.photoListCellViewModel = cellVM            
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.showCantLoginPopUp()
            return
        }
        self.viewModel.userPressed(at: indexPath)
                        
        if let selectedSubservie = self.viewModel.selectedSubervice {
            if let type = selectedService?.type {
                if type == 0 {
                    self.goToSecondSubcategories(forSelectedSubservice: selectedSubservie)
                } else if type == 1 {
                    self.goToSendOrder(forSelectedSubservice: selectedSubservie)
                }
            }
        }
    }
    
}

extension SubcategoriesViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
