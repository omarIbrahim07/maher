//
//  SubcategoriesCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct SubcategoriesCellViewModel {
    let subcategoryId: Int?
    let subcategoryName: String?
    let subcategoryNameEn: String?
    let subcategoryImage: String?
    let itemsCount: Int?
}
