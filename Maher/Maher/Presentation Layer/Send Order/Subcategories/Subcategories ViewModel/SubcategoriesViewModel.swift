//
//  SubcategoriesViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

class SubcategoriesViewModel {
    
    var selectedSubervice: Subservice?
    
    var subservices: [Subservice] = [Subservice]()
    var error: APIError?
    var searchSubcategories = [Subservice]()
    
    var searching = false
    
    var cellViewModels: [SubcategoriesCellViewModel] = [SubcategoriesCellViewModel]() {
       didSet {
           self.reloadTableViewClosure?()
       }
   }
    
     var searchedCellViewModels: [SubcategoriesCellViewModel] = [SubcategoriesCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var reloadOffersPagerView: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
            
    func initFetch(forCategoryId categoryId: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        ServicesAPIManager().getSubervices(forService: categoryId, basicDictionary: params, onSuccess: { (subservices) in
            
            self.processFetchedPhoto(subservices: subservices)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
        
    func createCellViewModel( subservice: Subservice ) -> SubcategoriesCellViewModel {

        //Wrap a description
        var descTextContainer: [String] = [String]()
        if let camera = subservice.name {
            descTextContainer.append(camera)
        }
                
        if let description = subservice.image {
            descTextContainer.append( description )
        }
        
        if "Lang".localized == "ar" {
            return SubcategoriesCellViewModel(subcategoryId: subservice.id, subcategoryName: subservice.name, subcategoryNameEn: subservice.nameEN, subcategoryImage: subservice.image, itemsCount: subservice.itemsCount)
        } else if "Lang".localized == "en" {
            return SubcategoriesCellViewModel(subcategoryId: subservice.id, subcategoryName: subservice.name, subcategoryNameEn: subservice.nameEN, subcategoryImage: subservice.image, itemsCount: subservice.itemsCount)
        }
        
        return SubcategoriesCellViewModel(subcategoryId: subservice.id, subcategoryName: subservice.name, subcategoryNameEn: subservice.nameEN, subcategoryImage: subservice.image, itemsCount: subservice.itemsCount)
    }
    
    func processFetchedPhoto( subservices: [Subservice] ) {
        self.subservices = subservices // Cache
        var vms = [SubcategoriesCellViewModel]()
        for subservice in subservices {
            vms.append( createCellViewModel(subservice: subservice) )
        }
        self.cellViewModels = vms
    }
    
    func processSearchingSubcategories( subservices: [Subservice] ) {
        self.searchSubcategories = subservices // Cache
        var vms = [SubcategoriesCellViewModel]()
        for subservice in subservices {
            vms.append( createCellViewModel(subservice: subservice) )
        }
        self.searchedCellViewModels = vms
    }
        
    func getCellViewModel( at indexPath: IndexPath ) -> SubcategoriesCellViewModel {
        if searching {
            return searchedCellViewModels[indexPath.row]
        } else {
            return cellViewModels[indexPath.row]
        }
    }
    
    func userPressed( at indexPath: IndexPath ){
        if searching {
            let subservice = self.searchSubcategories[indexPath.row]
            self.selectedSubervice = subservice
        } else {
            let subservice = self.subservices[indexPath.row]
            self.selectedSubervice = subservice
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
