//
//  SubcategoriesTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SubcategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var subcategoryImageView: UIImageView!
    @IBOutlet weak var subcategoryTitleLabel: UILabel!
    @IBOutlet weak var subcategoryNumberLabel: UILabel!
    
    var photoListCellViewModel : SubcategoriesCellViewModel? {
        didSet {
            
            if let itemsCount = photoListCellViewModel?.itemsCount {
                subcategoryNumberLabel.text = String(itemsCount)
            }
            
            if let subcategoryName = photoListCellViewModel?.subcategoryNameEn {
                subcategoryTitleLabel.text = subcategoryName
            }
                        
            if "lang".localized == "en" {
                if let subcategoryName = photoListCellViewModel?.subcategoryNameEn {
                    subcategoryTitleLabel.text = subcategoryName
                }
            } else if "lang".localized == "ar" {
                if let subcategoryName = photoListCellViewModel?.subcategoryName {
                    subcategoryTitleLabel.text = subcategoryName
                }
            }
            
            if let imageUrl = photoListCellViewModel?.subcategoryImage {
                subcategoryImageView.loadImageFromUrl(imageUrl: ImageURLSubervices + imageUrl)
            }
                        
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
