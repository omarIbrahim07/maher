//
//  SubcategoriesViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/4/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SubcategoriesViewController: BaseViewController {
    
    lazy var viewModel: SubcategoriesViewModel = {
        return SubcategoriesViewModel()
    }()
    
    var error: APIError?
    var selectedService: Service?
    var type: Int?
        
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
        initVM()
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        if let serviceID = selectedService?.id {
            viewModel.initFetch(forCategoryId: serviceID)
        }
    }
    
    func setNavigationTitle() {
        if let selectedServiceTitle: String = selectedService?.nameEN {
            navigationItem.title = selectedServiceTitle
        }
        if "lang".localized == "en" {
            if let selectedServiceTitle: String = selectedService?.nameEN {
                navigationItem.title = selectedServiceTitle
            }
        } else if "lang".localized == "ar" {
            if let selectedServiceTitle: String = selectedService?.name {
                navigationItem.title = selectedServiceTitle
            }
        }
    }

    
    func configureView() {
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        searchBar.delegate = self
        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "SubcategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "SubcategoriesTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Navigation
    func goToSecondSubcategories(forSelectedSubservice selectedSubservice: Subservice) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SecondSubcategoriesViewController") as! SecondSubcategoriesViewController
        viewController.selectedSubservice = selectedSubservice

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToSendOrder(forSelectedSubservice selectedSubservice: Subservice) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MakeOrderViewController") as! MakeOrderViewController
        viewController.subserviceID = selectedSubservice.serviceId

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }

}
