//
//  ProviderRegistrationViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/5/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class ProviderRegistrationViewModel {
    
    var error: APIError?

    var selectedService: Service? {
        didSet {
            reloadUpdatedServiceClosure?()
            if let serviceID = self.selectedService?.id {
                fetchProviderQuestion(serviceID: String(serviceID))
            }
        }
    }
        
    var services: [Service] = [Service]() {
        didSet {
            reloadServicesClosure?()
        }
    }

    var question: ProviderQuestion? {
        didSet {
            updateQuestion?()
        }
    }
    
    var registered: Bool? {
        didSet {
            updateProviderRegistrationClosure?()
        }
    }
    
    var reloadServicesClosure: (()->())?
    var reloadUpdatedServiceClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateQuestion: (()->())?
    var updateProviderRegistrationClosure: (()->())?
    var reloadOffersPagerView: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
            
    func initFetch(forCountry country: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "country" : country as AnyObject,
        ]
        
        ServicesAPIManager().getServices(basicDictionary: params, onSuccess: { (services) in
            
            self.services = services
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }
    
    func fetchProviderQuestion(serviceID: String) {
        state = .loading

        let params: [String : AnyObject] = [
            "service_id"    : serviceID as AnyObject
        ]

        AuthenticationAPIManager().getProviderQuestion(basicDictionary: params, onSuccess: { (providerQuestion) in

        self.question = providerQuestion

        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func registerAsProvider(firstName: String, secondName: String, serviceID: Int, providerAnswer: String) {
        state = .loading

        let params: [String : AnyObject] = [
            "provider_first_name" : firstName as AnyObject,
            "provider_last_name" : secondName as AnyObject,
            "service_id": serviceID as AnyObject,
            "answer"    : providerAnswer as AnyObject,
        ]

        AuthenticationAPIManager().registerAsProviderWithoutImages(basicDictionary: params, onSuccess: { (saved) in

        self.registered = saved

        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }


    func createCellViewModel( service: Service ) -> ItemViewModel {

        //Wrap a description
        var descTextContainer: [String] = [String]()
        if let camera = service.name {
            descTextContainer.append(camera)
        }
                
        if let description = service.image {
            descTextContainer.append( description )
        }
        
        if "Lang".localized == "ar" {
            return ItemViewModel(id: service.id, type: service.name)
        } else if "Lang".localized == "en" {
            return ItemViewModel(id: service.id, type: service.nameEN)
        }
        
        return ItemViewModel(id: service.id, type: service.nameEN)
    }
            
    func userPressed(service: Service) {
        self.selectedService = service
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
