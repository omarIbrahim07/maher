//
//  ProviderRegisterationViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation

extension ProviderRegistrationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
        print(location.coordinate)
//                saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
            providerCurrentLatitude = location.coordinate.latitude
            providerCurrentLongitude = location.coordinate.longitude
            
        }
    }
}

extension ProviderRegistrationViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}

