//
//  OfferImagesViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 10/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class OfferImagesViewController: CommonExampleController {
    
    var serviceID: Int?
    var firstName: String?
    var secondName: String?
    var answer: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "add more images button".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    //MARK:- Configurations
    func configureNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add offer second stage bar button title".localized, style: .plain, target: self, action: #selector(saveAction))
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "offer is done alert title".localized, message: "offer is done alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "offer is done action message".localized, style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- API Calls
    func saveAdPictures() {
        
        let parameters = [
            "provider_first_name" : firstName as AnyObject,
            "provider_last_name" : secondName as AnyObject,
            "service_id": serviceID as AnyObject,
            "answer"    : answer as AnyObject,
        ]
        
        var imageDataArr = [Data]()
        for asset in assets {
            let assetImage = getAssetThumbnail(asset: asset)
            let imgData = assetImage.jpegData(compressionQuality: 1.0)
            imageDataArr.append(imgData!)
        }
        
        startLoading()
        weak var weakSelf = self
        
        AuthenticationAPIManager().registerAsProvider(imageDataArray: imageDataArr, basicDictionary: parameters, onSuccess: {_ in 
            
            weakSelf?.stopLoadingWithSuccess()
            self.showDonePopUp()
            //            weakSelf?.dismiss(animated: true, completion: nil)
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    //MARK:- Actions
    @objc fileprivate func saveAction() {
        saveAdPictures()
    }
    
    override func pressedPick(_ sender: Any) {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    //MARK:- Helpers
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
}
