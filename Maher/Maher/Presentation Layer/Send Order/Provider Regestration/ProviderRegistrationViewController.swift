//
//  ProviderRegistrationViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/5/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class ProviderRegistrationViewController: BaseViewController {

    lazy var viewModel: ProviderRegistrationViewModel = {
        return ProviderRegistrationViewModel()
    }()
    
    var error: APIError?
    var services: [Service] = [Service]()
    let locationManager = CLLocationManager()

//    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    //    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var chooseServiceView: UIView!
    @IBOutlet weak var chooseServiceLabel: UILabel!
    @IBOutlet weak var choosedServiceLabel: UILabel!
    @IBOutlet weak var answerQuestionLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var questionAnswerTextView: UITextView!
//    @IBOutlet weak var questionAnswerTextfield: UITextField!
//    @IBOutlet weak var attachView: UIView!
    @IBOutlet weak var attachThisFilesLabel: UILabel!
//    @IBOutlet weak var attachLabel: UILabel!
//    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var attachButton: UIButton!
    @IBOutlet weak var regestrationButton: UIButton!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        initVM()
        checkLocationManager()
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
        viewModel.reloadServicesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.services = self!.viewModel.services
            }
        }
        
        viewModel.reloadUpdatedServiceClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                if let choosedService = self!.viewModel.selectedService {
                    self?.bindService(service: choosedService)
                }
            }
        }
                            
        viewModel.updateQuestion = { [weak self] () in
            DispatchQueue.main.async {
                if let providerQuestion = self!.viewModel.question {
                    self?.bindQuestion(providerQuestion: providerQuestion)
                }
            }
        }
        
        viewModel.updateProviderRegistrationClosure = { [weak self] () in
            DispatchQueue.main.async {
                if self!.viewModel.registered == true {
                    print("7abeeby")
                } else {
                    print("ll2sf me4 7abeeby")
                }
            }
        }
        
        if UserDefaultManager.shared.currentUser?.isProvider == 1 {
            self.goToMyOrders()
        } else {
            self.viewModel.initFetch(forCountry: "SA")
            showEnterRealNameAlert()
        }
        
    }
    
    func checkLocationManager() {
        locationManager.requestWhenInUseAuthorization()
//        checkLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }

    
    func configureView() {
        navigationItem.title = "provider label".localized
        chooseServiceView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        regestrationButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        attachView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeypad()
        chooseServiceLabel.text = "choose service placeholder".localized
//        answerQuestionLabel.text = "please answer question".localized
//        questionAnswerTextfield.placeholder = "answer placeholder".localized
        attachThisFilesLabel.text = "please attach these files label".localized
//        attachLabel.text = "attach button".localized
        regestrationButton.setTitle("registration as provider button".localized, for: .normal)
        
        if "Lang".localized == "en" {
//            questionAnswerTextfield.textAlignment = .left
        } else if "Lang".localized == "ar" {
//            questionAnswerTextfield.textAlignment = .right
        }

    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
//        questionAnswerTextfield.endEditing(true)
    }
    
    func bindQuestion(providerQuestion: ProviderQuestion) {
        if let question = providerQuestion.question {
            self.questionLabel.text = question
        }
        if "Lang".localized == "ar" {
//            self.cityLabel.text = city.name
        } else if "Lang".localized == "en" {
//            self.cityLabel.text = city.nameEn
        }
    }
    
    func bindService(service: Service) {
        if let name = service.nameEN {
            self.choosedServiceLabel.text = name
        }
        if "Lang".localized == "ar" {
            if let name = service.name {
                self.choosedServiceLabel.text = name
            }
        } else if "Lang".localized == "en" {
            if let name = service.nameEN {
                self.choosedServiceLabel.text = name
            }
        }
    }

    // MARK:- Show Time Options Picker
    func setupServicesSheet() {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for service in services {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: service.name, style: .default, handler: {_ in
                    self.viewModel.userPressed(service: service)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: service.nameEN, style: .default, handler: {_ in
                    self.viewModel.userPressed(service: service)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func showEnterRealNameAlert() {
        let alertController = UIAlertController(title: "real name title popup".localized, message: "real name message popup".localized, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "real name okay button".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func registerAsProvider() {
        
        guard let serviceID: Int = self.viewModel.selectedService?.id, serviceID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى اختيار الخدمة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let firstName: String = self.firstNameTextField.text, firstName.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إسمك الأول"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let secondName: String = self.secondNameTextField.text, secondName.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إسمك الثاني"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let answer: String = self.questionAnswerTextView.text, answer.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إجابتك على السؤال"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.registerAsProvider(firstName: firstName, secondName: secondName, serviceID: serviceID, providerAnswer: answer)
    }
        
    // MARK: - Navigation
    func goToMyOrders() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DelegateNewOrdersNavigationViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
        print("My Orders")
    }
    
    func goToUploadImages() {
        
        guard let serviceID: Int = self.viewModel.selectedService?.id, serviceID > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى اختيار الخدمة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let firstName: String = self.firstNameTextField.text, firstName.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إسمك الأول"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let secondName: String = self.secondNameTextField.text, secondName.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إسمك الثاني"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let answer: String = self.questionAnswerTextView.text, answer.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال إجابتك على السؤال"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OfferImagesViewController") as! OfferImagesViewController
        viewController.serviceID = serviceID
        viewController.firstName = firstName
        viewController.secondName = secondName
        viewController.answer = answer

        navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func chooseServiceButtonIsPressed(_ sender: Any) {
        print("Choose service button is pressed")
        self.setupServicesSheet()
    }
    
    @IBAction func attachFilesButtonIsPressed(_ sender: Any) {
        print("Attach files")
        goToUploadImages()
    }
    
    @IBAction func registrationAsProviderButtonIsPressed(_ sender: Any) {
        registerAsProvider()
    }
    
}
