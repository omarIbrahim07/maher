//
//  StoreDetailsViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension StoreDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell") as? ImageTableViewCell {
                
                let cellVM = viewModel.cellViewModels
                cell.photoListCellViewModel = cellVM

                return cell
            }
        } else if indexPath.row == 1 {
            if let cell: DetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DetailsTableViewCell") as? DetailsTableViewCell {
                
//                let cellVM = viewModel.getCellViewModel(at: indexPath)
                let cellVM = viewModel.cellViewModels
                cell.photoListCellViewModel = cellVM
                cell.delegate = self
                
                return cell
            }
        } else if indexPath.row == 2 {
            if let cell: ListOfImagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ListOfImagesTableViewCell") as? ListOfImagesTableViewCell {
                
                cell.viewModels = viewModel.menuImagecellViewModels
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
}

extension StoreDetailsViewController: orderButtonTableViewCellDelegate {
    func orderButtonPressed(itemID: Int, subserviceID: Int) {
        goToSendOrder(itemID: itemID, subserviceID: subserviceID)
    }
    
}

extension StoreDetailsViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
