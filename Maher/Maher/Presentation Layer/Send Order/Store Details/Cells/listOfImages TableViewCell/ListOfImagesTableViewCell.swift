//
//  ListOfImagesTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ListOfImagesTableViewCell: UITableViewCell {

    var error: APIError?

    var viewModel: StoreDetailsViewModel = {
        return StoreDetailsViewModel()
    }()
    
    var viewModels : [menuImageCollectionCellViewModel] = [menuImageCollectionCellViewModel]() {
        didSet {
            self.collectionView.reloadData()
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func registerCollectionViewCell() {
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
    }
    
}

extension ListOfImagesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 5, height: 100)
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
//        return viewModel.numbOfCells
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.viewModels.count)
        return self.viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell {
            
            let cellVm: menuImageCollectionCellViewModel = self.viewModels[indexPath.row]
            cell.menuImageCollectionCellViewModel = cellVm
//            cell.delegate = self
                        
            return cell
        }
        return UICollectionViewCell()
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}

