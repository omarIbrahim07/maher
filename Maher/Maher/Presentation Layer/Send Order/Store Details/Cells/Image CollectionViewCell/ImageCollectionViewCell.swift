//
//  ImageCollectionViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var menuImageView: UIImageView!
    
    var menuImageCollectionCellViewModel : menuImageCollectionCellViewModel? {
        didSet {
            if let image = menuImageCollectionCellViewModel?.image {
                menuImageView.loadImageFromUrl(imageUrl: ImageURLSecondSubervices + image)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
