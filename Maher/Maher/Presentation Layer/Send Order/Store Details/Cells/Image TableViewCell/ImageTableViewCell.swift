//
//  ImageTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var storeDetailsImageView: UIImageView!
    
    var photoListCellViewModel : StoreDetailsCellsViewModel? {
        didSet {
            
            if let imagePathURL: String = photoListCellViewModel?.cover {
                storeDetailsImageView.loadImageFromUrl(imageUrl: ImageURLSecondSubervices + imagePathURL)
            }
                                    
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
