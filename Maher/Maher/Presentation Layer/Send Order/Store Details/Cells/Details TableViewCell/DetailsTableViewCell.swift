//
//  DetailsTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

protocol orderButtonTableViewCellDelegate {
    func orderButtonPressed(itemID: Int, subserviceID: Int)
}

class DetailsTableViewCell: UITableViewCell {
    
    var delegate: orderButtonTableViewCellDelegate?
    
    @IBOutlet weak var storeDescriptionLabel: UILabel!
    @IBOutlet weak var userRateLabel: UILabel!
    @IBOutlet weak var userRateView: CosmosView!
    @IBOutlet weak var sharesCountLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var operatingHoursLabel: UILabel!
    @IBOutlet weak var rateValueLabel: UILabel!
    @IBOutlet weak var orderButton: UIButton!
    

    var photoListCellViewModel : StoreDetailsCellsViewModel? {
        didSet {
            
            if let itemId = photoListCellViewModel?.itemId {
                
            }
            
            if let subserviceId = photoListCellViewModel?.subserviceID {
                
            }
            
            if let stroreDescription = photoListCellViewModel?.descriptionEn {
                storeDescriptionLabel.text = stroreDescription
            }
            
            if "lang".localized == "ar" {
                if let stroreDescription = photoListCellViewModel?.description {
                    storeDescriptionLabel.text = stroreDescription
                }
            } else if "lang".localized == "en" {
                if let stroreDescription = photoListCellViewModel?.descriptionEn {
                    storeDescriptionLabel.text = stroreDescription
                }
            }
            
            if let rate = photoListCellViewModel?.rating {
                if let ratingValue = Double(rate) {
                    self.userRateView.rating = ratingValue
                    self.rateValueLabel.text = rate
                }
            }
            
            if let address = photoListCellViewModel?.location {
                addressLabel.text = address
            }
            
            if let openFrom = photoListCellViewModel?.openFrom, let openTo = photoListCellViewModel?.openTo {
                operatingHoursLabel.text = openFrom + " : " + openTo
            }
//            subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryNameEn
//
//            if "lang".localized == "en" {
//                subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryNameEn
//            } else if "lang".localized == "ar" {
//                subcategoryTitleLabel.text = photoListCellViewModel?.subcategoryName
//            }
//
//            if let imageURL: String = ImageURLSecondSubervices + (photoListCellViewModel?.icon)! {
//                subcategoryImageView.loadImageFromUrl(imageUrl: imageURL)
//            }
            
//            subcategoryNumberLabel.text = photoListCellViewModel?.
        }
    }

    func orderButtonPressed(itemID: Int, subserviceID: Int) {
        if let delegateValue = delegate {
            delegateValue.orderButtonPressed(itemID: itemID, subserviceID: subserviceID)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- Actions
    @IBAction func orderButtonIsPressed(_ sender: Any) {
        print("Order Button is pressed")
        if let itemId = self.photoListCellViewModel?.itemId, let subserviceId = photoListCellViewModel?.subserviceID {
            self.orderButtonPressed(itemID: itemId, subserviceID: subserviceId)
        }
    }
    
    
}
