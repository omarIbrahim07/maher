//
//  StoreDetailsViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/10/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class StoreDetailsViewController: BaseViewController {
    
    lazy var viewModel: StoreDetailsViewModel = {
        return StoreDetailsViewModel()
    }()
    
    var storeDetails: SecondSubservice?
    var error: APIError?

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var storeNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.parseViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.parseTopViewData()
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
            
        if let itemId = self.storeDetails?.id {
            viewModel.initFetch(forItemId: itemId)
        }
        
    }

    
    func configureView() {
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func parseTopViewData() {
        if let cellVM = viewModel.cellViewModels {
            if let storeName = cellVM.subcategoryNameEn {
                storeNameLabel.text = storeName
            }
            
            if "lang".localized == "en" {
                if let storeName = cellVM.subcategoryNameEn {
                    storeNameLabel.text = storeName
                }
            } else if "lang".localized == "ar" {
                if let storeName = cellVM.subcategoryName {
                    storeNameLabel.text = storeName
                }
            }
            
            if let iconImage = cellVM.icon {
                iconImageView.loadImageFromUrl(imageUrl: ImageURLSecondSubervices + iconImage)
            }
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageTableViewCell")
        tableView.register(UINib(nibName: "DetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailsTableViewCell")
        tableView.register(UINib(nibName: "ListOfImagesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListOfImagesTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Navigation
    func goToSendOrder(itemID: Int, subserviceID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MakeOrderViewController") as! MakeOrderViewController
        viewController.itemID = itemID
        viewController.subserviceID = subserviceID

        navigationController?.pushViewController(viewController, animated: true)
    }


}
