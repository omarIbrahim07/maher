//
//  StoreDetailsViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class StoreDetailsViewModel {
        
    private var menuImages: [menuImage] = [menuImage]()
    private var error: APIError?
    
    private var check: Bool?
    
    var saved: Bool? {
        didSet {
            self.updateUploadingImageCheck?()
        }
    }
                
    var menuImagecellViewModels: [menuImageCollectionCellViewModel] = [menuImageCollectionCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var cellViewModels: StoreDetailsCellsViewModel? {
        didSet {
            self.parseViewClosure?()
            self.reloadTableViewClosure?()
        }
    }
                
    var reloadTableViewClosure: (()->())?
    var parseViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateUploadingImageCheck: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    var numberOfCells: Int {
        if self.check == true {
            return 1
        }
        return 0
     }
    
    func initFetch(forItemId itemID: Int) {
        state = .loading

        let params: [String : AnyObject] = [:]

        ServicesAPIManager().getStoreDetails(forItemId: itemID, basicDictionary: params, onSuccess: { (storeDetails) in

            if let user = UserDefaultManager.shared.currentUser {
                self.check = true
                self.cellViewModels = self.processStoreDetails(storeDetails: storeDetails)
            }
            
            if let menuImages = storeDetails.menuImages {
                self.menuImages = menuImages
                self.processMenuImages(menuImages: menuImages)
            }

            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }

    }

//    func initFetch() {
//        state = .loading
//
//        let params: [String : AnyObject] = [:]
//
//        OrderAPIManager().getWorkerWorks(basicDictionary: params, onSuccess: { (works) in
//
//            if let user = UserDefaultManager.shared.currentUser {
//                self.check = true
//                self.cellViewModels = self.processTechnicianDetails(user: user)
//            }
//
//            self.workerWorks = works
//
////            self.processFetchedPhoto(technicians: technicians)
//            print(works)
//            self.processPreviousWorks(works: self.workerWorks)
//
//            self.state = .populated
//
//        }) { (error) in
//            self.error = error
//            self.state = .error
//        }
//    }
    
    func uploadImage(previousWorkImage: Data?) {
//        state = .loading
//
//        let params: [String : AnyObject] = [:]
//
//        AuthenticationAPIManager().uploadPreviousWork(profileImage: previousWorkImage, basicDictionary: params, onSuccess: { (saved) in
//
//            if saved == true {
//                self.saved = saved
//                self.state = .populated
//            } else {
//                self.state = .error
//                self.makeError(message: "عفوا لفد تم الوصول للحد الاقصى لرفع الصور")
//            }
//
//        }) { (error) in
//            self.error = error
//            self.state = .error
//        }
    }
    
    func getError() -> APIError {
        return self.error!
    }

    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

        
    func processMenuImages( menuImages: [menuImage] ) {
    //        if let workerOrders = self.technicianDetails?.workerOrders {
    //            self.workerOrders = workerOrders // Cache
    //        }
        var vms = [menuImageCollectionCellViewModel]()
        for menuImage in menuImages {
            vms.append(createPreviousCellViewModel(menuImage: menuImage) )
        }
        self.menuImagecellViewModels = vms
    }
    
    func createPreviousCellViewModel( menuImage: menuImage ) -> menuImageCollectionCellViewModel {
        //Wrap a description
        return menuImageCollectionCellViewModel(image: menuImage.image)
    }
    
    var numbOfCells: Int {
        return menuImagecellViewModels.count
     }
        
    func processStoreDetails(storeDetails: StoreDetails) -> StoreDetailsCellsViewModel {
        return StoreDetailsCellsViewModel(itemId: storeDetails.id, subcategoryName: storeDetails.name, subcategoryNameEn: storeDetails.nameEN, icon: storeDetails.icon, cover: storeDetails.cover, rating: storeDetails.rating, longitude: storeDetails.longitude, latitude: storeDetails.latitude, description: storeDetails.description, descriptionEn: storeDetails.descriptionEn, openFrom: storeDetails.openFrom, openTo: storeDetails.openTo, location: storeDetails.location, subserviceID: storeDetails.subserviceID)
    }
    
    func getImageUploadingCheck() -> Bool {
        return saved!
    }
                                
}
