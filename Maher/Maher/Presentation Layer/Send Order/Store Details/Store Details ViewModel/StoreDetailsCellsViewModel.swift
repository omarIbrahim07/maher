//
//  StoreDetailsCellsViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct StoreDetailsCellsViewModel {
    let itemId: Int?
    let subcategoryName: String?
    let subcategoryNameEn: String?
    let icon: String?
    let cover: String?
    let rating: String?
    let longitude: String?
    let latitude: String?
    let description: String?
    let descriptionEn: String?
    let openFrom: String?
    let openTo: String?
    let location: String?
    let subserviceID: Int?
}

struct menuImageCollectionCellViewModel {
    let image: String?
//    let id: Int
}
