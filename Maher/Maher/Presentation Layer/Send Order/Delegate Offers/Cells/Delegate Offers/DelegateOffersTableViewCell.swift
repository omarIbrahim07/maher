//
//  DelegateOffersTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateOffersTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var packageSizeLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var deleteLabel: UILabel!
    @IBOutlet weak var packageView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        packageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
