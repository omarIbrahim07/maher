//
//  DelegateOffersViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DelegateOffersViewController: BaseViewController {
    
    var arrayCount: [Int] = [1,2,3,4]

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addOfferButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
    }
    
    func configureView() {
        addOfferButton.roundCorners([.topRight, .bottomRight], radius: addOfferButton.frame.height / 2)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "DelegateOffersTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateOffersTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DelegateOffersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: DelegateOffersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateOffersTableViewCell") as? DelegateOffersTableViewCell {
            
            if indexPath.row % 2 == 0 {
                cell.outerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.innerView.roundCorners(.bottomRight, radius: 60)
                cell.innerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.fromLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.textView.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.packageSizeLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.editLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.deleteLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                
                if indexPath.row == arrayCount.endIndex - 1 {
                    cell.outerView.backgroundColor = .clear
                }
                
            } else if indexPath.row % 2 == 1 {
                cell.outerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.innerView.roundCorners(.bottomRight, radius: 60)
                cell.innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.fromLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.textView.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.packageSizeLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.editLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.deleteLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                
                if indexPath.row == arrayCount.endIndex - 1 {
                    cell.outerView.backgroundColor = .clear
                }

            }
            return cell
        }
        return UITableViewCell()
    }   

    
}
