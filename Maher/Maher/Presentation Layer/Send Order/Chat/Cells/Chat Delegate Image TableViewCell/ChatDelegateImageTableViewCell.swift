//
//  ChatDelegateImageTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol WorkerChatImageTableViewCellDelegate {
    func WorkerChatImageButtonPressed(image: String)
}

class ChatDelegateImageTableViewCell: UITableViewCell {
    
    var selectedImagee: String?
    var delegate: WorkerChatImageTableViewCellDelegate?
    
        //MARK:- Delegate Helpers
    func WorkerChatImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.WorkerChatImageButtonPressed(image: image)
        }
    }
    
    @IBOutlet weak var virtualView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var chatDelegateImageView: UIImageView!
    @IBOutlet weak var seenTimeLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var delegateImageButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        containerView.layer.shadowOpacity = 0.44
        containerView.layer.shadowRadius = 5
        virtualView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        virtualView.layer.shadowOpacity = 0.44
        virtualView.layer.shadowRadius = 20
        
//        virtualView.roundCorners([.topLeft, .topRight, .bottomLeft ], radius: 20)
        containerView.layer.cornerRadius = 20.0
        userImageView.addCornerRadius(raduis: userImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func chatImageButtonIsPressed(_ sender: Any) {
        if let image = self.selectedImagee {
            WorkerChatImageButtonPressed(image: image)
        }
    }

}
