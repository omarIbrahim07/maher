//
//  ChatMaherTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ChatMaherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clientTextMessageLAbel: UILabel!
    @IBOutlet weak var maherImageView: UIImageView!
    @IBOutlet weak var seenTimeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.roundCorners([.topLeft, .topRight, .bottomLeft ], radius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func maherButtonIsPressed(_ sender: Any) {
        print("Maher is pressed")
    }
    
}
