//
//  ChatClientImageTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ClientChatImageTableViewCellDelegate {
    func ClientChatImageButtonPressed(image: String)
}

class ChatClientImageTableViewCell: UITableViewCell {
    
    var delegate: ClientChatImageTableViewCellDelegate?
    
    var selectedImagee: String?
    
        //MARK:- Delegate Helpers
    func ClientChatImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.ClientChatImageButtonPressed(image: image)
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var chatClientImageView: UIImageView!
    @IBOutlet weak var seenTimeLabel: UILabel!
    @IBOutlet weak var seenImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.roundCorners([.topLeft, .topRight, .bottomRight ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clientImageButtonIsPressed(_ sender: Any) {
        if let image = self.selectedImagee {
            ClientChatImageButtonPressed(image: image)
        }
    }
    
}
