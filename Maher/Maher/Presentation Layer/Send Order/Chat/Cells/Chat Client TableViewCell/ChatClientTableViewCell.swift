//
//  ChatClientTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ChatClientTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clientTextMessageLAbel: UILabel!
    @IBOutlet weak var seenImageView: UIImageView!
    @IBOutlet weak var seenTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.roundCorners([.topLeft, .topRight, .bottomRight ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
