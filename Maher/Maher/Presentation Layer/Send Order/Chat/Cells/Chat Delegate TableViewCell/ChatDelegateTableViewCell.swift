//
//  ChatDelegateTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ChatDelegateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var delegateTextMessageLAbel: UILabel!
    @IBOutlet weak var delegateImageView: UIImageView!
    @IBOutlet weak var seenTimeLabel: UILabel!
    @IBOutlet weak var virtualView: UIView!
    @IBOutlet weak var delegateImageButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        containerView.layer.shadowOpacity = 0.44
        containerView.layer.shadowRadius = 5
        virtualView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        virtualView.layer.shadowOpacity = 0.44
        virtualView.layer.shadowRadius = 20
        
//        virtualView.roundCorners([.topLeft, .topRight, .bottomLeft ], radius: 20)
        containerView.layer.cornerRadius = 20.0
        delegateImageView.addCornerRadius(raduis: delegateImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
