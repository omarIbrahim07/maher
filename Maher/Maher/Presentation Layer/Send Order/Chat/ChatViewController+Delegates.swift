//
//  ChatViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AVFoundation

extension ChatViewController: ClientChatImageTableViewCellDelegate {
    func ClientChatImageButtonPressed(image: String) {
        self.goToImageDetails(imageURL: image)
    }
}

extension ChatViewController: WorkerPersonalImageViewButtonTableViewCellDelegate {
    func workerPersonalImageViewButtonPressed() {
        if UserDefaultManager.shared.currentUser?.id == Int(self.toID ?? "0") {
            print("Mesa2 el fol ll nas el kol")
        } else {
            print("2l4t menak ya 7doota")
        }
    }
}

extension ChatViewController: WorkerChatImageTableViewCellDelegate {
    func WorkerChatImageButtonPressed(image: String) {
        self.goToImageDetails(imageURL: image)
    }
}

extension ChatViewController: playButtonTableViewCellDelegate {
    func didPlayButtonPressed(tag: Int) {
        playedCellIndex = tag
        tableView.reloadData()
    }
}

extension ChatViewController: workerplayButtonTableViewCellDelegate {
    func workerdidPlayButtonPressed(tag: Int) {
        playedCellIndex = tag
        tableView.reloadData()
    }
}

extension ChatViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

extension ChatViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//                firstImage.image = image
//                firstImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myFirstImage = image
            self.dismiss(animated: true, completion: nil)
            self.showDonePopUp()
        }
    }
}

extension ChatViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 240
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.1) {
            self.topConstraint.constant = 10
            self.view.layoutIfNeeded()
        }
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message: ChatMessage = self.chatMessages[indexPath.row]

        if message.messageType == "0" {
            if let cell: ChatClientImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatClientImageTableViewCell") as? ChatClientImageTableViewCell {
                if let imageText = message.message {
                    cell.chatClientImageView.loadImageFromUrl(imageUrl: imageText)
                    cell.selectedImagee = imageText
                }
                if let messageTimestamp = message.timeStamp {
                    cell.seenTimeLabel.text = messageTimestamp
                }
                cell.delegate = self
                return cell
            }
        } else if message.messageType == "1" {
            if let cell: ChatClientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatClientTableViewCell") as? ChatClientTableViewCell {
                if let messageText = message.message {
                    cell.clientTextMessageLAbel.text = messageText
                }
                if let messageTimestamp = message.timeStamp {
                    cell.seenTimeLabel.text = messageTimestamp
                }
                return cell
            }
        } else if message.messageType == "2" {
            if let cell: ChatClientRecordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatClientRecordTableViewCell") as? ChatClientRecordTableViewCell {
                if let messageTimestamp = message.timeStamp {
                    cell.seenLabel.text = messageTimestamp
                }
                if let clientRecord: String = message.message {
                    cell.playButton.tag = indexPath.row
                    cell.soundSlider.tag = indexPath.row
                    cell.delegate = self
                    cell.sound = clientRecord
                    if let index = playedCellIndex, indexPath.row == index {
                        cell.playedCellIndex = index
                    }
                    else {
                        cell.playedCellIndex = nil
                    }
                }
                return cell
            }
        } else if message.messageType == "3" {
            if let cell: ChatDelegateImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatDelegateImageTableViewCell") as? ChatDelegateImageTableViewCell {
                if let imageText = message.message {
                    cell.chatDelegateImageView.loadImageFromUrl(imageUrl: imageText)
                    cell.selectedImagee = imageText
                }
                if let messageTimestamp = message.timeStamp {
                    cell.seenTimeLabel.text = messageTimestamp
                }
                cell.delegate = self
                return cell
            }
        } else if message.messageType == "4" {
            if let cell: ChatDelegateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatDelegateTableViewCell") as? ChatDelegateTableViewCell {
                if let messageText = message.message {
                    cell.delegateTextMessageLAbel.text = messageText
                }
                if let messageTimestamp = message.timeStamp {
                    cell.seenTimeLabel.text = messageTimestamp
                }
                return cell
            }
        } else if message.messageType == "5" {
            if let cell: ChatDelegateRecordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatDelegateRecordTableViewCell") as? ChatDelegateRecordTableViewCell {
                cell.personalImageViewDelegate = self
                if let messageTimestamp = message.timeStamp {
                    cell.seenLabel.text = messageTimestamp
                }
                if let clientRecord: String = message.message {
                    cell.playButton.tag = indexPath.row
                    cell.soundSlider.tag = indexPath.row
                    cell.delegate = self
                    cell.sound = clientRecord
                    if let index = playedCellIndex, indexPath.row == index {
                        cell.playedCellIndex = index
                    }
                    else {
                        cell.playedCellIndex = nil
                    }
                }
                return cell
            }
        } else if message.messageType == "6" {
            print("receipt")
        } else if message.messageType == "7" {
            print("Maher message")
            if let cell: ChatMaherTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatMaherTableViewCell") as? ChatMaherTableViewCell {
                if let messageText = message.message {
                    cell.clientTextMessageLAbel.text = messageText
                }
                if let messageTimestamp = message.timeStamp {
                    cell.seenTimeLabel.text = messageTimestamp
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
}
