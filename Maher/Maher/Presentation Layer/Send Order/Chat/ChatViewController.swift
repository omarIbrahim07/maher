//
//  ChatViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class ChatViewController: BaseViewController {

    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    var url: URL?
    var myFirstImage: UIImage? = UIImage()

    var firstImageData: Data?

    var orderID: Int?
    var toID: String?
    var fromID: String?
    var chatMessages: [ChatMessage] = [ChatMessage]()
    var check: Bool? = false
    
    var playedCellIndex: Int?

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var moreButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendMessageView: UIView!
    @IBOutlet weak var innerSendMessageView: UIView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var delegateImageView: UIImageView!
    @IBOutlet weak var magicView: UIView!
    @IBOutlet weak var editTimeView: UIView!
    @IBOutlet weak var editPlaceView: UIView!
    @IBOutlet weak var editCostView: UIView!
    @IBOutlet weak var chatTextfield: UITextField!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordImage: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        configureChatTextfield()

        if let orderID: Int = self.orderID {
            self.observeMessages(roomID: orderID)
        }
        
        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.recordButton.isEnabled = true
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

    }
    
    // MARK:- Observe room id
    func observeMessages(roomID: Int) {
        let roomID: String = String(roomID)
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child(roomID).observe(.childAdded) { (snapShot) in
            print(snapShot)
            if let dataArray = snapShot.value as? [String : Any] {
                guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String, let timeStamp = dataArray["time_stamp"] as? String else { return }
                
                let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType, timeStamp: timeStamp)
                
                // MARK:- Know fromID & toID
                if self.check == false && self.chatMessages.count > 1 {
                    self.check = true
                    self.fromID = self.chatMessages[0].fromUSerID
                    self.toID = self.chatMessages[0].toUSerID
                    if let myID = UserDefaultManager.shared.currentUser?.id {
                        if self.fromID == String(myID) {
                            self.configureTopView(exist: true)
                        } else {
                            self.configureTopView(exist: false)
                        }
                    }
                }
                
                self.chatMessages.append(message)
                self.tableView.reloadData()
                self.scrollToTableViewBottom()
            }
        }
    }
    
    func configureTopView(exist: Bool) {
        if exist == true {
            self.topView.isHidden = false
            self.topViewHeightConstraint.constant = 50
            self.moreButtonTopConstraint.constant = 71
            self.scrollToTableViewBottom()
        } else {
            self.topView.isHidden = true
            self.topViewHeightConstraint.constant = 0
            self.moreButtonTopConstraint.constant = 20
            self.scrollToTableViewBottom()
        }
    }

// MARK:- Send message to firebase
    func sendMessage(message: String, fromID: String, toID: String, messageType: String) {
        let fromIDString: String = String(fromID)
        let toIDString: String = String(toID)
        let timeStamp: String = getCurrentTime()
        
        let dataArray: [String : Any] = ["from_user_id" : fromIDString,
                                         "message" : message,
                                         "message_type" : messageType,
                                         "to_user_id" : toIDString,
                                         "time_stamp" : timeStamp]
        
        let refrence = Database.database().reference()
        if let orderID: Int = self.orderID {
            let room = refrence.child(String(orderID))
            room.child(timeStamp).setValue(dataArray) { (err, ref) in
                if err == nil {
                    print("Zay elfol")
                    self.chatTextfield.text = ""
                } else {
                    print("Zay elzeft")
                }
            }
        }
    }

    // MARK:- Get Current Time
    func getCurrentTime() -> String {
        let timestamp = Date().timeIntervalSince1970

        // gives date with time portion in UTC 0
        let date = Date(timeIntervalSince1970: timestamp)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"  // change to your required format
        dateFormatter.timeZone = TimeZone.current

        // date with time portion in your specified timezone
        let dateString: String = dateFormatter.string(from: date)
        let englishDateString: String = dateString.withWesternNumbers
        print(englishDateString)
        
        return englishDateString
    }

    // Func to force tableview when u have many messages to down (( to last message ))
    private func scrollToTableViewBottom() {
        
        guard self.chatMessages.count > 0 else { return }
        
        self.tableView.scrollToRow(at: IndexPath(row: self.chatMessages.count - 1, section: 0),
                                   at: UITableView.ScrollPosition.bottom,
                                   animated: false)
    }
        
//MARK:- Configuration
    func configureView() {
        moreButton.setTitle("more button title".localized, for: .normal)
        self.topView.isHidden = true
        self.topViewHeightConstraint.constant = 0
        okButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.topConstraint.constant = 10
        moreButton.roundCorners([.topRight, .bottomRight], radius: moreButton.frame.height / 2)
        callView.roundCorners([.topLeft, .bottomLeft], radius: callView.frame.height / 2)
        delegateImageView.addCornerRadius(raduis: delegateImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        innerSendMessageView.addCornerRadius(raduis: innerSendMessageView.frame.height / 2, borderColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), borderWidth: 2)
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
        self.magicView.isHidden = true
        self.editTimeView.isHidden = true
        self.editPlaceView.isHidden = true
        self.editCostView.isHidden = true
        self.magicView.roundCorners([.topLeft, .bottomRight], radius: 60)
        closeKeypad()
    }
    
    func configureChatTextfield() {
        chatTextfield.delegate = self
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    
    @objc func viewTapped() {
        chatTextfield.endEditing(true)
        self.magicView.isHidden = true
//        moreButton.setTitle("more button title".localized, for: .normal)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ChatClientTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatClientTableViewCell")
        tableView.register(UINib(nibName: "ChatDelegateTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatDelegateTableViewCell")
        tableView.register(UINib(nibName: "ChatMaherTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatMaherTableViewCell")
        tableView.register(UINib(nibName: "ChatClientImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatClientImageTableViewCell")
        tableView.register(UINib(nibName: "ChatDelegateImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatDelegateImageTableViewCell")
        tableView.register(UINib(nibName: "ChatDelegateRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatDelegateRecordTableViewCell")
        tableView.register(UINib(nibName: "ChatClientRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatClientRecordTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // Option sheet of magic view on appears on chat when u pressed more button
    func setupOptionSheet() {
        let sheet = UIAlertController()
//        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        if UserDefaultManager.shared.currentUser?.id == Int(self.fromID ?? "0") {
            sheet.addAction(UIAlertAction(title: "Edit time".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editTimeView.isHidden = false
                self.editPlaceView.isHidden = true
                self.editCostView.isHidden = true
            }))
            sheet.addAction(UIAlertAction(title: "Edit place".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editPlaceView.isHidden = false
                self.editTimeView.isHidden = true
                self.editCostView.isHidden = true
            }))
            sheet.addAction(UIAlertAction(title: "Edit cost".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editCostView.isHidden = false
                self.editPlaceView.isHidden = true
                self.editTimeView.isHidden = true
            }))
            sheet.addAction(UIAlertAction(title: "Change provider".localized, style: .default, handler: {_ in
            }))
            sheet.addAction(UIAlertAction(title: "Add to favourite".localized, style: .default, handler: {_ in
                if let providerID = self.toID {
                    self.addProviderToFavourites(providerID: Int(providerID) ?? 0, action: 0)
                }
            }))
            sheet.addAction(UIAlertAction(title: "Make complaint".localized, style: .default, handler: {_ in
            }))
        } else if UserDefaultManager.shared.currentUser?.id == Int(self.toID ?? "0") {
            sheet.addAction(UIAlertAction(title: "complains".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editTimeView.isHidden = false
                self.editPlaceView.isHidden = true
                self.editCostView.isHidden = true
            }))
            sheet.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editPlaceView.isHidden = false
                self.editTimeView.isHidden = true
                self.editCostView.isHidden = true
            }))
            sheet.addAction(UIAlertAction(title: "receipt".localized, style: .default, handler: {_ in
                self.magicView.isHidden = false
                self.editCostView.isHidden = false
                self.editPlaceView.isHidden = true
                self.editTimeView.isHidden = true
            }))
        }

//        sheet.addAction(UIAlertAction(title: "block option".localized, style: .default, handler: {_ in
//            self.showBlockPopUp()
//        }))
        sheet.addAction(UIAlertAction(title: "Cancel order".localized, style: .cancel, handler: nil))
//        sheet.popoverPresentationController?.sourceView = s1WeigthLabel
        self.present(sheet, animated: true, completion: nil)
    }
    
    func addProviderToFavourites(providerID: Int, action: Int) {
        
        let parameters: [String : AnyObject] = [
            "provider_id" : providerID as AnyObject,
            "action" : action as AnyObject
        ]
                
        self.startLoading()
        
        DelegatesOffersAPIManager().favouriteProvider(basicDictionary: parameters , onSuccess: { (saved) in
            
            self.stopLoadingWithSuccess()
                        
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }

    }

// MARK:- Recording helpers
    //MARK:- Send Record Only
    func sendRecordOnly() {
        
        guard let fromID = self.fromID, fromID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.toID, toID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }

        //MARK:- Convert file url into Data
        let fileURl = self.url
        let audioData = try? Data(contentsOf: fileURl!)

        let parameters: [String : AnyObject] = [:]

        if let myID = UserDefaultManager.shared.currentUser?.id {
            if fromID == String(myID) {
                
            startLoading()
                    
            ChatAPIManager().sendRecord(recordData: audioData, recordName: "file", basicDictionary: parameters, onSuccess: { (message) in

            self.stopLoadingWithSuccess()
            print(message)
                    
            self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "2")
                                        
            }) { (error) in
                print(error)
            }
                
            } else if toID == String(myID) {
                startLoading()
                    
            ChatAPIManager().sendRecord(recordData: audioData, recordName: "file", basicDictionary: parameters, onSuccess: { (message) in

            self.stopLoadingWithSuccess()
            print(message)
                    
            self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "5")
                                        
                    
            }) { (error) in
                print(error)
            }
        }
    }
}

    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()

            print("Tap to Stop")
            recordImage.image = UIImage(named: "stop")
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    func finishRecording(success: Bool) {
        let fileUrl = audioRecorder.url
        url = fileUrl
        print(fileUrl)
        
        audioRecorder.stop()
        audioRecorder = nil

        if success {
            print("Tap to Re-record")
            recordImage.image = UIImage(named: "microphone")
        } else {
            print("Tap to Record")
            // recording failed :(
        }
    }

// MARK:- Image Upload functions helpers
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    func edit() {
                
        guard let fromID = self.fromID, fromID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.toID, toID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }
        
        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        
        let parameters: [String : AnyObject] = [:]
        
        if let myID = UserDefaultManager.shared.currentUser?.id {
            if fromID == String(myID) {
                
                startLoading()
                    
                ChatAPIManager().sendImage(imageData: firstImageData, basicDictionary: parameters, onSuccess: { (message) in
                    
                    self.stopLoadingWithSuccess()
                    print(message)
                    
                self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "0")
                                        
                }) { (error) in
                    print(error)
                }
                
            } else if toID == String(myID) {
                startLoading()
                    
                ChatAPIManager().sendImage(imageData: firstImageData, basicDictionary: parameters, onSuccess: { (message) in
                    
                    self.stopLoadingWithSuccess()
                    print(message)
                    
                self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "3")
                                        
                    
                }) { (error) in
                    print(error)
                }
            }
        }

    }
    
    func orderIsDone() {
        
        guard let orderID = self.orderID, orderID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        let strOrderID = String(orderID)
        
        let parameters: [String : AnyObject] = [
            "order_id" : strOrderID as AnyObject
        ]
        
        startLoading()
        
        OrderAPIManager().orderIsDone(basicDictionary: parameters, onSuccess: { (saved) in
            
            self.stopLoadingWithSuccess()
            
            if saved == true {
                self.configureTopView(exist: false)
                self.showOrderIsDonePopUp()
            } else {
                self.configureTopView(exist: true)
            }
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    func showDonePopUp() {
        let alertController = UIAlertController(title: "رفع الصورة", message: "جاري رفع الصورة !", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.edit()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showOrderIsDonePopUp() {
        let alertController = UIAlertController(title: "تم إنهاء الخدمة", message: "برجاء الضغط على تم", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToDelegateRate()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
        
// MARK:- Navigation
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
    func goToDelegateRate() {
        if let delegateRateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DelegateRateViewController") as? DelegateRateViewController {
//            delegateRateVC.imageURL = imageURL
            self.present(delegateRateVC, animated: true, completion: nil)
        }
    }
    
// MARK:- Actions
    @IBAction func okButtonIsPressed(_ sender: Any) {
        self.orderIsDone()
    }
    
    @IBAction func moreButtonIsPressed(_ sender: Any) {
        self.setupOptionSheet()
//        moreButton.setTitle("more button title opened".localized, for: .normal)
    }

    @IBAction func callViewButtonIsPressed(_ sender: Any) {
        print("Call is pressed")
    }
    
    @IBAction func sendMessageButtonIsPressed(_ sender: Any) {
        print("sendMessageButtonIsPressed")
        
        guard let textMessage = self.chatTextfield.text, textMessage.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء كتابة الرسالة"
            showError(error: apiError)
            return
        }
        
        guard let fromID = self.fromID, fromID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.toID, toID.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }
        
        if let myID = UserDefaultManager.shared.currentUser?.id {
            if fromID == String(myID) {
                self.sendMessage(message: textMessage, fromID: fromID, toID: toID, messageType: "1")
            } else if toID == String(myID) {
                self.sendMessage(message: textMessage, fromID: fromID, toID: toID, messageType: "4")
            }
        }
        
    }
    
    @IBAction func cameraButtonIsPressed(_ sender: Any) {
        print("cameraButtonIsPressed")

        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        imagePressed()
    }
    
    @IBAction func recordButtonIsPressed(_ sender: Any) {
        print("recordButtonIsPressed")

        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
            sendRecordOnly()
        }
    }


}
