//
//  ManagerReplyTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ManagerReplyTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.roundCorners([.topLeft, .bottomRight, .topRight], radius: 30)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
