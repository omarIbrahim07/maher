//
//  MakeComplainViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class MakeComplainViewController: BaseViewController {
    
    var viewChoosed: Int? = 1

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var rateCommentView: UIView!
    @IBOutlet weak var complainMarkView: UIView!
    @IBOutlet weak var replyMarkView: UIView!
    @IBOutlet weak var sendReplyView: UIView!
    @IBOutlet weak var sendReplyViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerSendReplyView: UIView!
    @IBOutlet weak var upsetImageView: UIImageView!
    @IBOutlet weak var neutralImageView: UIImageView!
    @IBOutlet weak var satsifiedImageView: UIImageView!
    @IBOutlet weak var emojisView: UIView!
    @IBOutlet weak var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
    }
    
    func configureView() {
        complainMarkView.isHidden = false
        replyMarkView.isHidden = true
        outerView.isHidden = false
        sendReplyView.isHidden = true
        rateCommentView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        outerView.roundCorners(.bottomRight, radius: 60)
        innerSendReplyView.addCornerRadius(raduis: innerSendReplyView.frame.height / 2, borderColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), borderWidth: 2)
        emojisView.isHidden = true
        tableView.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ManagerReplyTableViewCell", bundle: nil), forCellReuseIdentifier: "ManagerReplyTableViewCell")
        tableView.register(UINib(nibName: "ClientComplainTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientComplainTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK:- Actions
    @IBAction func complainButtonIsPressed(_ sender: Any) {
        tableView.isHidden = true
        outerView.isHidden = false
        complainMarkView.isHidden = false
        replyMarkView.isHidden = true
        sendReplyView.isHidden = true
        sendReplyViewHeightConstraint.constant = 0
        emojisView.isHidden = true
        viewChoosed = 1
//        backgroundImageView.isHidden = false
//        tableView.reloadData()
    }
    
    @IBAction func replyButtonIsPressed(_ sender: Any) {
        outerView.isHidden = true
        replyMarkView.isHidden = false
        complainMarkView.isHidden = true
        tableView.isHidden = false
        sendReplyView.isHidden = false
        sendReplyViewHeightConstraint.constant = 45
        emojisView.isHidden = false
        viewChoosed = 2
//        backgroundImageView.isHidden = true
//        tableView.reloadData()
    }
    
    @IBAction func upsetButtonIsPressed(_ sender: Any) {
        upsetImageView.image = UIImage(named: "Upset")
        neutralImageView.image = UIImage(named: "Unchecked Neutral")
        satsifiedImageView.image = UIImage(named: "Unchecked Satisfied")
    }
    
    @IBAction func neutralButtonIsPressed(_ sender: Any) {
        upsetImageView.image = UIImage(named: "Unchecked Upset")
        neutralImageView.image = UIImage(named: "Neutral")
        satsifiedImageView.image = UIImage(named: "Unchecked Satisfied")
    }

    @IBAction func satsifiedButtonIsPressed(_ sender: Any) {
        upsetImageView.image = UIImage(named: "Unchecked Upset")
        neutralImageView.image = UIImage(named: "Unchecked Neutral")
        satsifiedImageView.image = UIImage(named: "Satsified")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MakeComplainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            if let cell: ManagerReplyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ManagerReplyTableViewCell") as? ManagerReplyTableViewCell {
                return cell
            }
        } else if indexPath.row % 2 == 1 {
            if let cell: ClientComplainTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClientComplainTableViewCell") as? ClientComplainTableViewCell {
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    
}
