//
//  DelegatesOffersViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Firebase

class DelegatesOffersViewController: BaseViewController {

    lazy var viewModel: DelegatesOffersViewModel = {
        return DelegatesOffersViewModel()
    }()
    
    var error: APIError?
    var orderID: Int?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderNumberLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
    
    // MARK:- Get Current Time
    func getCurrentTime() -> String {
        let timestamp = Date().timeIntervalSince1970

        // gives date with time portion in UTC 0
        let date = Date(timeIntervalSince1970: timestamp)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"  // change to your required format
        dateFormatter.timeZone = TimeZone.current

        // date with time portion in your specified timezone
        let dateString: String = dateFormatter.string(from: date)
        let englishDateString: String = dateString.withWesternNumbers
        print(englishDateString)
        
        return englishDateString
    }
    
    func sendMessage(child: String, message: String, fromID: Int, toID: Int, messageType: String) {
        let fromIDString: String = String(fromID)
        let toIDString: String = String(toID)
        let timeStamp: String = getCurrentTime()
        
        let dataArray: [String : Any] = ["from_user_id" : fromIDString,
                                         "message" : message,
                                         "message_type" : messageType,
                                         "to_user_id" : toIDString,
                                         "time_stamp" : timeStamp]
        
        let refrence = Database.database().reference()
        if let orderID: Int = self.orderID {
//            let str = "c-"
//            let room = refrence.child(str + String(roomID))
            let room = refrence.child(String(orderID))
            room.child(child).setValue(dataArray) { (err, ref) in
                if err == nil {
                    print("Zay elfol")
                } else {
                    print("Zay elzeft")
                }
            }
        }
    }

    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateAcceptedOfferStatus = { [weak self] () in
            DispatchQueue.main.async {
                if let saved = self?.viewModel.saved, let selectedDelegateOffer = self?.viewModel.selectedDelegateOffer {
                    if saved == true {
                        if let fromUserID = UserDefaultManager.shared.currentUser?.id , let toUserID = selectedDelegateOffer.providerID, let description: String = selectedDelegateOffer.order?.orderDetails, let cost = selectedDelegateOffer.cost {
                            self?.sendMessage(child: "0", message: description, fromID: fromUserID, toID: toUserID, messageType: "7")
                            self?.sendMessage(child: "1", message: "Destination", fromID: fromUserID, toID: toUserID, messageType: "7")
                            self?.sendMessage(child: "2", message: "Pick up", fromID: fromUserID, toID: toUserID, messageType: "7")
                            self?.sendMessage(child: "3", message: cost, fromID: fromUserID, toID: toUserID, messageType: "7")
                            self!.showDonePopUp()
                        }
                    }
                }
            }
        }
        
        if let orderID: Int = self.orderID {
            viewModel.initFetch(orderID: orderID)
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم اختيار الأوردر", message: "دوس أوكي", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            if let orderID: Int = self.orderID {
                self.goToChat(orderID: orderID)
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func configureView() {
        if let orderID: Int = self.orderID {
            orderNumberLabel.text = "Order Number: " + String(orderID)
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "DelegateOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "DelegateOfferTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func showClientCantCancelOrderPopUp() {
        let alertController = UIAlertController(title: "إنهاء الطلب", message: "يرجى العلم أنه لا يمكن إلغاء الطلب بعد بدايته إلا من قبل مقدم الخدمة \n رصيد الحساب يستخدم فقط في دفع قيمة التوصيل \n يتم اعتماد الفاتورة بعد دقيقتين من إرسالها من قبل مقدم الخدمة", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "موافق", style: .default) { (action) in
//            self.goToDelegateRate()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    func goToChat(orderID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        viewController.orderID = orderID

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToDelegateInformationDetails(providerID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DelegateInformationDetailsViewController") as! DelegateInformationDetailsViewController
        viewController.providerID = providerID

        navigationController?.pushViewController(viewController, animated: true)
    }

}
