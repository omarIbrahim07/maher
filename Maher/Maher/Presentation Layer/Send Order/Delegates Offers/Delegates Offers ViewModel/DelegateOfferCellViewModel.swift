//
//  DelegateOfferCellViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct DelegateOfferCellViewModel {
    let id: Int?
    let orderID: Int?
    let cost: String?
    let providerID: Int?
    let orderOfferStatus: Int?
    let providerName: String?
    let time: String?
//    let orderOfferLong: String?
//    let orderOfferLat: String?
}
