//
//  DelegatesOffersViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class DelegatesOffersViewModel {
    
    private var delegatesOffers: [DelegateOffer] = [DelegateOffer]()
    var selectedDelegateOffer: DelegateOffer?
    
    var error: APIError?
    
    var cellViewModels: [DelegateOfferCellViewModel] = [DelegateOfferCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var saved: Bool? {
        didSet {
            updateAcceptedOfferStatus?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateAcceptedOfferStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initFetch(orderID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject
        ]
        
        DelegatesOffersAPIManager().getDelegatesOffers(basicDictionary: params, onSuccess: { (delegatesOffers) in
            
            self.processFetchedPhoto(delegatesOffers: delegatesOffers)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func userAssignsProvider(orderOfferID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "orderOffer_id" : orderOfferID as AnyObject
        ]
        
        DelegatesOffersAPIManager().assignProviderToOrder(basicDictionary: params, onSuccess: { (saved) in
            
            print(saved)
            self.saved = saved
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func processFetchedPhoto( delegatesOffers: [DelegateOffer] ) {
          self.delegatesOffers = delegatesOffers // Cache
          var vms = [DelegateOfferCellViewModel]()
          for delegateOffer in delegatesOffers {
              vms.append( createCellViewModel(delegateOffer: delegateOffer))
          }
          self.cellViewModels = vms
      }
    
    func createCellViewModel( delegateOffer: DelegateOffer ) -> DelegateOfferCellViewModel {
                
        return DelegateOfferCellViewModel(id: delegateOffer.id, orderID: delegateOffer.orderID, cost: delegateOffer.cost, providerID: delegateOffer.providerID, orderOfferStatus: delegateOffer.orderOfferStatus, providerName: delegateOffer.providerName, time: delegateOffer.order?.orderTime)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> DelegateOfferCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func userPressed( at indexPath: Int ){
        let delegateOffer = self.delegatesOffers[indexPath]

        self.selectedDelegateOffer = delegateOffer
    }

    
}


