//
//  DelegateOfferTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol AcceptButtonTableViewCellDelegate {
    func acceptOfferButtonPressed(offerID: Int, index: Int)
}

protocol ProviderInforamtionDetailsButtonTableViewCellDelegate {
    func providerInformationDetailsButtonPressed(providerID: Int)
}

class DelegateOfferTableViewCell: UITableViewCell {
    
    var delegate: AcceptButtonTableViewCellDelegate?
    var providerInformationDetailsDelegate: ProviderInforamtionDetailsButtonTableViewCellDelegate?
    var offerIDArray: [Int] = [Int]()
    var indexArray: [Int] = [Int]()

    var photoListCellViewModel : DelegateOfferCellViewModel? {
        didSet {
            if let offerId: Int = photoListCellViewModel?.id {
                offerIDArray.append(offerId)
//                acceptButton.tag = offerId
            }
            
            indexArray.append(acceptButton.tag)
            
            if let cost: String = photoListCellViewModel?.cost {
                deliveryCostValueLabel.text = cost + " EGP"
            }
            
            if let providerName: String = photoListCellViewModel?.providerName {
                offerOwnerNameLabel.text = providerName
            }

            if let orderTime: String = photoListCellViewModel?.time {
                durationValueLabel.text = orderTime + " Hour"
            }

////            if let delegate: String = photoListCellViewModel?.delegate {
////                delegateValueLabel.text = delegate
////            }
//
//            if let orderState: Int = photoListCellViewModel?.orderStatus {
////                orderStateValueLabel.text = ""
//            }
//
////            if let service: String = photoListCellViewModel?.service {
////                serviceValueLabel.text = service
////            }
        }
    }

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var offerOwnerImageView: UIImageView!
    @IBOutlet weak var offerOwnerNameLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var rateValueLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var deliveryCostLabel: UILabel!
    @IBOutlet weak var deliveryCostValueLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationValueLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var delegateProfileButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func acceptOfferButtonPressed(offerID: Int, index: Int) {
        if let delegateValue = delegate {
            delegateValue.acceptOfferButtonPressed(offerID: offerID, index: index)
        }
    }
    
    func providerInformationDetailsButtonPressed(providerID: Int) {
        if let delegateValue = providerInformationDetailsDelegate {
            delegateValue.providerInformationDetailsButtonPressed(providerID: providerID)
        }
    }
    
    @IBAction func acceptButtonIsPressed(_ sender: Any) {
        acceptOfferButtonPressed(offerID: offerIDArray[tag], index: indexArray[tag])
    }
    
    @IBAction func delegateProfileButtonIsPressed(_ sender: Any) {
        print("Delegate Profile")
        providerInformationDetailsButtonPressed(providerID: self.delegateProfileButton.tag)
    }
    
    
}
