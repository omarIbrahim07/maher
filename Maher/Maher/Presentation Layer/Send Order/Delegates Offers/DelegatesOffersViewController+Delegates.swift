//
//  DelegatesOffersViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension DelegatesOffersViewController: ProviderInforamtionDetailsButtonTableViewCellDelegate {
    func providerInformationDetailsButtonPressed(providerID: Int) {
        print(providerID)
        self.goToDelegateInformationDetails(providerID: providerID)
    }
}

extension DelegatesOffersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: DelegateOfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DelegateOfferTableViewCell") as? DelegateOfferTableViewCell {

            if indexPath.row % 2 == 0 {
                
                cell.delegate = self
                cell.providerInformationDetailsDelegate = self
                cell.outerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.innerView.roundCorners(.bottomRight, radius: 60)
                cell.innerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                
                if indexPath.row == viewModel.numberOfCells - 1 {
                    cell.outerView.backgroundColor = UIColor.clear
                }

                cell.acceptButton.addCornerRadius(raduis: cell.acceptButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                cell.acceptButton.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.acceptButton.setTitleColor(#colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), for: .normal)
                cell.offerOwnerNameLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.rateValueLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.deliveryCostLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.deliveryCostValueLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.durationLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.durationValueLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.distanceLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.distanceValueLabel.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                cell.firstView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                
                let cellVM = viewModel.getCellViewModel( at: indexPath )
                cell.photoListCellViewModel = cellVM
                cell.acceptButton.tag = indexPath.row
                if let providerID = cellVM.providerID {
                    cell.delegateProfileButton.tag = providerID
                }
                
                return cell
            } else if indexPath.row % 2 == 1 {
                
                cell.delegate = self
                cell.providerInformationDetailsDelegate = self
                cell.outerView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.innerView.roundCorners(.bottomRight, radius: 60)
                cell.innerView.backgroundColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
                
                if indexPath.row == viewModel.numberOfCells - 1 {
                    cell.outerView.backgroundColor = UIColor.clear
                }
                
                cell.acceptButton.addCornerRadius(raduis: cell.acceptButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                cell.acceptButton.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.acceptButton.setTitleColor(#colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1), for: .normal)
                cell.offerOwnerNameLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.rateValueLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.deliveryCostLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.deliveryCostValueLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.durationLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.durationValueLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.distanceLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.distanceValueLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                cell.firstView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
                
                let cellVM = viewModel.getCellViewModel( at: indexPath )
                cell.photoListCellViewModel = cellVM
                cell.acceptButton.tag = indexPath.row
                if let providerID = cellVM.providerID {
                    cell.delegateProfileButton.tag = providerID
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.goToSecondSubcategories()
    }
    
}

extension DelegatesOffersViewController: AcceptButtonTableViewCellDelegate {
    func acceptOfferButtonPressed(offerID: Int, index: Int) {
        print(offerID)
        print(index)
        self.viewModel.userAssignsProvider(orderOfferID: offerID)
        self.viewModel.userPressed(at: index)
    }
}

