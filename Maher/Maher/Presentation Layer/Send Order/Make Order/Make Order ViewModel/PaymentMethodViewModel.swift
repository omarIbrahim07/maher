//
//  PaymentMethodViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/1/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct PaymentMethodViewModel {
    let id: Int
    let type: String
    let typeAr: String
}
