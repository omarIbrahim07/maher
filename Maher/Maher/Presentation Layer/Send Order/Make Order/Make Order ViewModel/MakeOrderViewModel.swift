//
//  MakeOrderViewModel.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/1/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class MakeOrderViewModel {
    
    var error: APIError?
    
    var description: String?
    var promocode: String?
    var paymentMethodd: Int? = 1
    var duringHours: Int? = 1
    var date: String?
    var time: String?
    
    var updateLoadingStatus: (()->())?
    var updatePaymentMethod: (()->())?
    var updateTiming: (()->())?
    
    // callback for interfaces
    var paymentMethod: PaymentMethodViewModel? {
        didSet {
            updatePaymentMethod?()
        }
    }
    
    var timing: TimingViewModel? {
        didSet {
            updateTiming?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func sendFirstScenarioOrder(orderDetails: String, itemID: Int, subserviceID: Int, promocode: String, paymentMethod: Int, pickLat: String, pickLong: String, desLat: String, desLong: String, orderTime: String) {
        
        guard let startDate: String = orderTime, startDate.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.error = apiError
            self.state = .error
            return
        }
        
        let params: [String : AnyObject] = [
            "order_details" : orderDetails as AnyObject,
            "item_id" : itemID as AnyObject,
            "subservice_id" : subserviceID as AnyObject,
            "promocode" : promocode as AnyObject,
            "payment_method" : paymentMethod as AnyObject,
            "pick_lat" : pickLat as AnyObject,
            "pick_long" : pickLong as AnyObject,
            "des_lat" : desLat as AnyObject,
            "des_long" : desLong as AnyObject,
            "order_time" : orderTime as AnyObject
        ]
        
        self.state = .loading
        
        OrderAPIManager().SendOrder(basicDictionary: params, onSuccess: { (orderID) in
            if orderID != nil {
                print("Success")
                self.state = .populated
                //                self.sendingOrder = .success
            } else {
                self.state = .empty
                //                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
            //            self.sendingOrder = .failed
        }
    }
    
    func sendSecondScenarioOrder(orderDetails: String, subserviceID: Int, promocode: String, paymentMethod: Int, desLat: String, desLong: String, orderTime: String) {
        
        guard let startDate: String = orderTime, startDate.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.error = apiError
            self.state = .error
            return
        }
        
        let params: [String : AnyObject] = [
            "order_details" : orderDetails as AnyObject,
            "subservice_id" : subserviceID as AnyObject,
            "promocode" : promocode as AnyObject,
            "payment_method" : paymentMethod as AnyObject,
            "des_lat" : desLat as AnyObject,
            "des_long" : desLong as AnyObject,
            "order_time" : orderTime as AnyObject
        ]
        
        self.state = .loading
        
        OrderAPIManager().SendOrder(basicDictionary: params, onSuccess: { (orderID) in
            if orderID != nil {
                print("Success")
                self.state = .populated
                //                self.sendingOrder = .success
            } else {
                self.state = .empty
                //                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
            //            self.sendingOrder = .failed
        }
    }
    
    
    func getPaymentMethods() -> [PaymentMethodViewModel] {
        return [PaymentMethodViewModel(id: 1, type: "Cash", typeAr: "كاش"), PaymentMethodViewModel(id: 2, type: "Mada", typeAr: "مدى"), PaymentMethodViewModel(id: 3, type: "Visa", typeAr: "فيزا"), PaymentMethodViewModel(id: 4, type: "Balance", typeAr: "بلانس")]
    }
    
    func getPaymentMethod(paymentMethod: PaymentMethodViewModel) {
        self.paymentMethod = paymentMethod
    }
    
    func getTimings() -> [TimingViewModel] {
        if "Lang".localized == "ar" {
            return [TimingViewModel(title: "الآن"), TimingViewModel(title: "لاحقًا")]
        }
        return [TimingViewModel(title: "Now"), TimingViewModel(title: "Later")]
    }
    
    func getTiming(timingViewModel: TimingViewModel) {
        self.timing = timingViewModel
    }
    
    func getError() -> APIError {
        return self.error!
    }
}
