//
//  MakeOrderViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/29/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation
import DatePickerDialog
import TimePicker
import DateTimePicker


// MARK:- Date Time Picker Delegate
extension MakeOrderViewController: DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        var f: String?
        f = picker.selectedDateString
        let subString = f?.prefix(5)
        if "Lang".localized == "ar" {
//            self.viewModel.time = String(subString!) + ":00"
            let time = String(subString!) + ":00"
            self.viewModel.time = time.withWesternNumbers
            if self.itemID != nil {
                self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
            } else if self.itemID == nil {
                self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
            }
        } else if "Lang".localized == "en" {
            self.viewModel.time = String(subString!) + ":00"
            if self.itemID != nil {
                self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
            } else if self.itemID == nil {
                self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
            }
        }
//        self.reservationTimeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}

extension MakeOrderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.itemID != nil {
            return 7
        } else if itemID == nil {
            return 8
        } else {
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: TopViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TopViewTableViewCell") as? TopViewTableViewCell {
                return cell
            }
        } else if indexPath.row == 1 {
            if let cell: OrderDescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderDescriptionTableViewCell") as? OrderDescriptionTableViewCell {
                cell.delegate = self
                return cell
            }
        } else if indexPath.row == 2 {
            if let cell: PromocodeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PromocodeTableViewCell") as? PromocodeTableViewCell {
                cell.delegate = self
                return cell
            }
        } else if indexPath.row == 3 {
            if let cell: PaymentMethodTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell") as? PaymentMethodTableViewCell {
                cell.delegate = self
                
                if "Lang".localized == "ar" {
                    if let paymentMethod = self.choosedPaymentMethod?.typeAr {
                        cell.paymentMethodLabel.text = paymentMethod
                    }
                } else if "Lang".localized == "en" {
                    if let paymentMethod = self.choosedPaymentMethod?.type {
                        cell.paymentMethodLabel.text = paymentMethod
                    }
                }
                
                return cell
            }
        } else if indexPath.row == 4 {
            if let cell: ArrivalDestinationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ArrivalDestinationTableViewCell") as? ArrivalDestinationTableViewCell {
                cell.delegate = self
                
                if let arrivalAddress: String = myArrivalDestinationChoiceAddress {
                    cell.locationAddressLabel.text = arrivalAddress
                }
                
                return cell
            }
        }
        
        if self.itemID == nil {
            if indexPath.row == 5 {
                if let cell: DestinationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DestinationTableViewCell") as? DestinationTableViewCell {
                    cell.delegate = self
                    
                    if let destinationAddress: String = myDestinationChoiceAddress {
                        cell.locationAddressLabel.text = destinationAddress
                    }

                    return cell
                }
            } else if indexPath.row == 6 {
                if self.choosedTiming?.title == "Now" || self.choosedTiming?.title == "الآن" {
                    if let cell: TimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TimeTableViewCell") as? TimeTableViewCell {
                        cell.delegate = self
                        if let duringHours = self.viewModel.duringHours {
                            cell.duringHourValueLabel.text = String(duringHours)
                        }
                        return cell
                    }
                } else if self.choosedTiming?.title == "Later" || self.choosedTiming?.title == "لاحقًا" {
                    if let cell: LaterTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LaterTimeTableViewCell") as? LaterTimeTableViewCell {
                        cell.delegate = self
                        if let date = self.viewModel.date {
                            cell.dateValueLabel.text = String(date)
                        }
                        if let time = self.viewModel.time {
                            cell.timeValueLabel.text = String(time)
                        }
                        return cell
                    }
                }
            } else if indexPath.row == 7 {
                if let cell: OrderButtonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderButtonTableViewCell") as? OrderButtonTableViewCell {
                    cell.delegate = self
                    return cell
                }
            }
        } else if self.itemID != nil {
            if indexPath.row == 5 {
                if self.choosedTiming?.title == "Now" || self.choosedTiming?.title == "الآن" {
                    if let cell: TimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TimeTableViewCell") as? TimeTableViewCell {
                        cell.delegate = self
                        if let duringHours = self.viewModel.duringHours {
                            cell.duringHourValueLabel.text = String(duringHours)
                        }
                        return cell
                    }
                } else if self.choosedTiming?.title == "Later" || self.choosedTiming?.title == "لاحقًا" {
                    if let cell: LaterTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LaterTimeTableViewCell") as? LaterTimeTableViewCell {
                        cell.delegate = self
                        if let date = self.viewModel.date {
                            cell.dateValueLabel.text = String(date)
                        }
                        if let time = self.viewModel.time {
                            cell.timeValueLabel.text = String(time)
                        }
                        return cell
                    }
                }
            } else if indexPath.row == 6 {
                if let cell: OrderButtonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OrderButtonTableViewCell") as? OrderButtonTableViewCell {
                    cell.delegate = self
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    
}

extension MakeOrderViewController: mapdestinationButtonTableViewCellDelegate {
    func mapDestinationButtonPressed() {
        self.goToMap(cellChoosed: 1)
    }
    func currentLocationButtonPressed() {
        self.tag = 1
        self.checkLocationManager()
    }
}

extension MakeOrderViewController: mapArrivalDestinationButtonTableViewCellDelegate {
    func mapArrivalDestinationButtonPressed() {
        self.goToMap(cellChoosed: 2)
    }
    func arrivalCurrentLocationButtonPressed() {
        self.tag = 2
        self.checkLocationManager()
    }
}

extension MakeOrderViewController: sendOrderButtonTableViewCellDelegate {
    func sendOrderButtonPressed() {
        if self.viewModel.timing?.title == "Now" || self.viewModel.timing?.title == "الآن" {
            if self.itemID != nil {
                setSecondSceinarioOrder()
            } else if self.itemID == nil {
                setFirstSceinarioOrder()
            }
//            self.setOrder()
        } else if self.viewModel.timing?.title == "Later" || self.viewModel.timing?.title == "لاحقًا" {
            if self.itemID != nil {
                self.setFirstScenarioStartDate()
            } else if self.itemID == nil {
                self.setSecondScenarioStartDate()
            }
//            self.setStartDate()
        }
    }
}

extension MakeOrderViewController: OrderDescriptionTableViewCellDelegate {
    func orderDescriptionTextViewPressed(description: String) {
        self.viewModel.description = description
        print(self.viewModel.description)
    }
}

extension MakeOrderViewController: PromocodeTableViewCellDelegate {
    func PromocodeViewPressed(promocde: String) {
        print(promocde)
        self.viewModel.promocode = promocde
    }
}

extension MakeOrderViewController: PaymentMethodTableViewCellDelegate {
    func paymentMethodButtonViewPressed() {
        self.setupPaymentMethodsSheet()
    }
}

extension MakeOrderViewController: TimeTableViewCellDelegate {
    func timeButtonViewPressed() {
        self.setupTimingsSheet()
    }
    func duringButtonViewPressed() {
        self.setupDuringHoursTimeSheet()
    }
}

extension MakeOrderViewController: LaterTimeTableViewCellDelegate {
    func laterTimeButtonViewPressed() {
        self.setupTimingsSheet()
    }
    func timeButtonPressed() {
        self.setTheTime()
    }
    func dateButtonPressed() {
        self.datePickerTapped()
    }
}

// MARK:- GET CURRENT LOCATION
// MARK:- Location Manager Delegate
extension MakeOrderViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
            print(location.coordinate)
            if tag == 1 {
//                saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
                destinationLatitude = location.coordinate.latitude
                destinationLongitude = location.coordinate.longitude
            } else if tag == 2 {
                arrivalDestinationLatitude = location.coordinate.latitude
                arrivalDestinationLongitude = location.coordinate.longitude
            }
            self.locationManager.stopUpdatingLocation()
        }
    }
}

extension MakeOrderViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Orders" {
            goToOrders()
        } else if keyWord == "Notifications" {
            goToNotifications()
        } else if keyWord == "Profile" {
            goToProfile()
        } else if keyWord == "Main" {
            goToMain()
        } else if keyWord == "Provider" {
            goToProvider()
        }
    }
}
