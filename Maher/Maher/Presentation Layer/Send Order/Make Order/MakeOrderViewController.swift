//
//  MakeOrderViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation
import DatePickerDialog
import TimePicker
import DateTimePicker

var destinationLongitude: Double?
var destinationLatitude: Double?
var arrivalDestinationLongitude: Double?
var arrivalDestinationLatitude: Double?
var myDestinationChoiceAddress: String?
var myArrivalDestinationChoiceAddress: String?

class MakeOrderViewController: BaseViewController {
    
    lazy var viewModel: MakeOrderViewModel = {
        return MakeOrderViewModel()
    }()
    
    var itemID: Int?
    var subserviceID: Int?
    var tag: Int?
    let locationManager = CLLocationManager()
    
    var error: APIError?
    
    var paymentMethods: [PaymentMethodViewModel] = [PaymentMethodViewModel]()
    var choosedPaymentMethod: PaymentMethodViewModel?
    var timings: [TimingViewModel] = [TimingViewModel]()
    var choosedTiming: TimingViewModel?

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
        
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
        print(arrivalDestinationLongitude)
        print(arrivalDestinationLatitude)
        print(destinationLongitude)
        print(destinationLatitude)
        print(myDestinationChoiceAddress)
        print(myArrivalDestinationChoiceAddress)
        
        if myArrivalDestinationChoiceAddress != nil {
            self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        }
        
        if myDestinationChoiceAddress != nil {
            if self.itemID == nil {
                self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initMap()
        configureView()
        initVM()
        configureTableView()
    }
    
    func initMap() {
        arrivalDestinationLongitude = nil
        arrivalDestinationLatitude = nil
        destinationLongitude = nil
        destinationLatitude = nil
        myDestinationChoiceAddress = nil
        myArrivalDestinationChoiceAddress = nil
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {

                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.stopLoadingWithError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                                    
        viewModel.updatePaymentMethod = { [weak self] () in
            DispatchQueue.main.async {
                self?.choosedPaymentMethod = self!.viewModel.paymentMethod
                if let paymentMethod = self?.choosedPaymentMethod {
                    self?.bindPaymentMethod(paymentMethod: paymentMethod)
                }
            }
        }
        
        viewModel.updateTiming = { [weak self] () in
            DispatchQueue.main.async {
                self?.choosedTiming = self!.viewModel.timing
                if let timing = self?.choosedTiming {
                    self?.bindTiming(timing: timing)
                }
            }
        }
        
        self.paymentMethods = viewModel.getPaymentMethods()
        self.timings = viewModel.getTimings()
        self.choosedTiming = timings[0]
    }
    
    func bindPaymentMethod(paymentMethod: PaymentMethodViewModel) {
        tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
    }
    
    func bindTiming(timing: TimingViewModel) {
        if self.itemID != nil {
            self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        } else if self.itemID == nil {
            self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
        }
    }

    func checkLocationManager() {
        locationManager.requestWhenInUseAuthorization()
//        checkLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
        
    func configureView() {
        print(itemID)
        print(subserviceID)
        self.navigationItem.title = "send button".localized
        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TopViewTableViewCell", bundle: nil), forCellReuseIdentifier: "TopViewTableViewCell")
        tableView.register(UINib(nibName: "OrderDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderDescriptionTableViewCell")
        tableView.register(UINib(nibName: "PromocodeTableViewCell", bundle: nil), forCellReuseIdentifier: "PromocodeTableViewCell")
        tableView.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodTableViewCell")
        tableView.register(UINib(nibName: "ArrivalDestinationTableViewCell", bundle: nil), forCellReuseIdentifier: "ArrivalDestinationTableViewCell")
        tableView.register(UINib(nibName: "DestinationTableViewCell", bundle: nil), forCellReuseIdentifier: "DestinationTableViewCell")
        tableView.register(UINib(nibName: "TimeTableViewCell", bundle: nil), forCellReuseIdentifier: "TimeTableViewCell")
        tableView.register(UINib(nibName: "OrderButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderButtonTableViewCell")
        tableView.register(UINib(nibName: "LaterTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "LaterTimeTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK:- Set the time
    func setTheTime() {
        let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        self.view.addSubview(picker)
        
        picker.isTimePickerOnly = true
        picker.is12HourFormat = false
        picker.isTimePickerOnly = true
        
//        picker.todayButtonTitle = "time now".localized
//        picker.todayButtonTitle = ""
        picker.cancelButtonTitle = ""

        picker.delegate = self
        
        picker.show()
    }
    
    func datePickerTapped() {
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
//                self.viewModel.date = formatter.string(from: dt)
                let date = formatter.string(from: dt)
                self.viewModel.date = date.withWesternNumbers
                if self.itemID != nil {
                    self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
                } else if self.itemID == nil {
                    self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
                }
//                self.reservationDateLabel.text = formatter.string(from: dt)
//                self.reservationDateLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }

    
    // MARK:- Show Payment Methods Options Picker
    func setupPaymentMethodsSheet() {
        
        let sheet = UIAlertController(title: "gender alert title".localized, message: "gender alert message".localized, preferredStyle: .actionSheet)
        for paymentMethod in paymentMethods {
//            sheet.addAction(UIAlertAction(title: paymentMethod.type, style: .default, handler: {_ in
//                self.viewModel.getPaymentMethod(paymentMethod: paymentMethod)
//            }))
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: paymentMethod.typeAr, style: .default, handler: {_ in
                    self.viewModel.getPaymentMethod(paymentMethod: paymentMethod)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: paymentMethod.type, style: .default, handler: {_ in
                    self.viewModel.getPaymentMethod(paymentMethod: paymentMethod)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel gender".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    func setupDuringHoursTimeSheet() {
        
        let sheet = UIAlertController(title: "gender alert title".localized, message: "gender alert message".localized, preferredStyle: .actionSheet)
        if choosedTiming?.title == "Now" || choosedTiming?.title == "الآن" {
            for i in 1...24 {
                sheet.addAction(UIAlertAction(title: String(i), style: .default, handler: {_ in
                    self.viewModel.duringHours = i
                    print(self.viewModel.duringHours)
                    if self.itemID != nil {
                        self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
                    } else if self.itemID == nil {
                        self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
                    }
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel gender".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }

    
    // MARK:- Show Payment Methods Options Picker
    func setupTimingsSheet() {
        
        let sheet = UIAlertController(title: "gender alert title".localized, message: "gender alert message".localized, preferredStyle: .actionSheet)
        for timing in timings {
            sheet.addAction(UIAlertAction(title: timing.title, style: .default, handler: {_ in
                self.viewModel.getTiming(timingViewModel: timing)
            }))
        }
        sheet.addAction(UIAlertAction(title: "cancel gender".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }

    func setFirstSceinarioOrder() {
        
        guard let orderDetails: String = self.viewModel.description, orderDetails.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الوصف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let itemId: Int = self.itemID, itemId > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال رقم المتجر"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let subservice: Int = self.subserviceID, subservice > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الخدمة الفرعية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let promocode: String = self.viewModel.promocode, promocode.count == 8  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال البرومو كود"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let paymentMethod: Int = self.viewModel.paymentMethod?.id, paymentMethod > 0  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال نوع الدفع"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let picklongitude: Double = destinationLongitude, picklongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let pickLatitude: Double = destinationLatitude, pickLatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlongitude: Double = arrivalDestinationLongitude, destlongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlatitude: Double = arrivalDestinationLatitude, destlatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        if let hours = self.viewModel.duringHours {
            self.viewModel.sendFirstScenarioOrder(orderDetails: orderDetails, itemID: itemID!, subserviceID: subserviceID!, promocode: promocode, paymentMethod: paymentMethod, pickLat: String(pickLatitude), pickLong: String(picklongitude), desLat: String(destlatitude), desLong: String(destlongitude), orderTime: String(hours))
        }
    }
    
    func setSecondSceinarioOrder() {
        
        guard let orderDetails: String = self.viewModel.description, orderDetails.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الوصف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let subservice: Int = self.subserviceID, subservice > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الخدمة الفرعية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let promocode: String = self.viewModel.promocode, promocode.count == 8  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال البرومو كود"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let paymentMethod: Int = self.viewModel.paymentMethod?.id, paymentMethod > 0  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال نوع الدفع"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlongitude: Double = arrivalDestinationLongitude, destlongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlatitude: Double = arrivalDestinationLatitude, destlatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        if let hours = self.viewModel.duringHours {
            self.viewModel.sendSecondScenarioOrder(orderDetails: orderDetails, subserviceID: subserviceID!, promocode: promocode, paymentMethod: paymentMethod, desLat: String(destlatitude), desLong: String(destlongitude), orderTime: String(hours))
        }
    }

    
    // MARK:- Set date and time
    func setFirstScenarioStartDate() {
        
        guard let orderDetails: String = self.viewModel.description, orderDetails.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الوصف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let itemId: Int = self.itemID, itemId > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال رقم المتجر"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let subservice: Int = self.subserviceID, subservice > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الخدمة الفرعية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let promocode: String = self.viewModel.promocode, promocode.count == 8  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال البرومو كود"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let paymentMethod: Int = self.viewModel.paymentMethod?.id, paymentMethod > 0  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال نوع الدفع"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let picklongitude: Double = destinationLongitude, picklongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let pickLatitude: Double = destinationLatitude, pickLatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlongitude: Double = arrivalDestinationLongitude, destlongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlatitude: Double = arrivalDestinationLatitude, destlatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        guard let startTime: String = self.viewModel.time, startTime.count > 0, startTime.count == 8 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال التوقيت"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }

        guard let startDate: String = self.viewModel.date, startDate.count > 0, startDate.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        let year: String = startDate.substring(with: 6..<10)
        let day: String = startDate.substring(with: 3..<5)
        let month: String = startDate.substring(with: 0..<2)
        
        let date = startDate.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
        let newDate = "\(year)-\(month)-\(day)"
        print(newDate)
        
        let start: String = newDate + " " + startTime

        print("StartDate: \(start)")
        
        self.viewModel.sendFirstScenarioOrder(orderDetails: orderDetails, itemID: itemID!, subserviceID: subserviceID!, promocode: promocode, paymentMethod: paymentMethod, pickLat: String(pickLatitude), pickLong: String(picklongitude), desLat: String(destlatitude), desLong: String(destlongitude), orderTime: start)

    }
    
    func setSecondScenarioStartDate() {
        
        guard let orderDetails: String = self.viewModel.description, orderDetails.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الوصف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        guard let subservice: Int = self.subserviceID, subservice > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال الخدمة الفرعية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let promocode: String = self.viewModel.promocode, promocode.count == 8  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال البرومو كود"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let paymentMethod: Int = self.viewModel.paymentMethod?.id, paymentMethod > 0  else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال نوع الدفع"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlongitude: Double = arrivalDestinationLongitude, destlongitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let destlatitude: Double = arrivalDestinationLatitude, destlatitude > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        guard let startTime: String = self.viewModel.time, startTime.count > 0, startTime.count == 8 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال التوقيت"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }

        guard let startDate: String = self.viewModel.date, startDate.count > 0, startDate.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        let year: String = startDate.substring(with: 6..<10)
        let day: String = startDate.substring(with: 3..<5)
        let month: String = startDate.substring(with: 0..<2)
        
        let date = startDate.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
        let newDate = "\(year)-\(month)-\(day)"
        print(newDate)
        
        let start: String = newDate + " " + startTime

        print("StartDate: \(start)")
        
        self.viewModel.sendSecondScenarioOrder(orderDetails: orderDetails, subserviceID: subserviceID!, promocode: promocode, paymentMethod: paymentMethod, desLat: String(destlatitude), desLong: String(destlongitude), orderTime: start)

    }




    // MARK: - Navigation
    func goToMap(cellChoosed: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        viewController.cellChoosed = cellChoosed
        navigationController?.pushViewController(viewController, animated: true)
    }

}
