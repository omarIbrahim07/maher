//
//  OrderButtonTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol sendOrderButtonTableViewCellDelegate {
    func sendOrderButtonPressed()
}

class OrderButtonTableViewCell: UITableViewCell {

    var delegate: sendOrderButtonTableViewCellDelegate?

    @IBOutlet weak var sendOrderButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sendOrderButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        sendOrderButton.setTitle("send button".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func sendOrderButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.sendOrderButtonPressed()
        }
    }
    
    @IBAction func sendOrderButtonIsPressed(_ sender: Any) {
        self.sendOrderButtonPressed()
    }
    
}
