//
//  TopViewTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class TopViewTableViewCell: UITableViewCell {

    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        innerView.roundCorners(.bottomRight, radius: 60)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
