//
//  TimeTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol TimeTableViewCellDelegate {
    func timeButtonViewPressed()
    func duringButtonViewPressed()
}
    

class TimeTableViewCell: UITableViewCell {
    
    var delegate: TimeTableViewCellDelegate?

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var duringHourValueLabel: UILabel!
    @IBOutlet weak var duringLabel: UILabel!
    @IBOutlet weak var anotherTimeLabel: UILabel!
    @IBOutlet weak var timeTitleLabel: UILabel!
    @IBOutlet weak var nowLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLocalization()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView() {
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
    }
    
    func setLocalization() {
        timeTitleLabel.text = "time".localized
        duringLabel.text = "during".localized
        anotherTimeLabel.text = "another time".localized
        nowLabel.text = "now".localized
    }
    
    func timeButtonViewPressed() {
        if let delegateValue = delegate {
            delegateValue.timeButtonViewPressed()
        }
    }
    
    func duringButtonViewPressed() {
        if let delegateValue = delegate {
            delegateValue.duringButtonViewPressed()
        }
    }
    
    @IBAction func duringButtonIsPressed(_ sender: Any) {
        self.duringButtonViewPressed()
    }
    
    @IBAction func anotherTimeButtonIsPressed(_ sender: Any) {
        self.timeButtonViewPressed()
    }
    
}
