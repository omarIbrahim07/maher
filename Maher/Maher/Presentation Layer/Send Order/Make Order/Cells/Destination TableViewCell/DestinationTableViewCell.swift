//
//  DestinationTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol mapdestinationButtonTableViewCellDelegate {
//    func orderButtonPressed(itemID: Int, subserviceID: Int)
    func mapDestinationButtonPressed()
    func currentLocationButtonPressed()
}

class DestinationTableViewCell: UITableViewCell {
    
    var delegate: mapdestinationButtonTableViewCellDelegate?

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var locationAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        configureView()
        setLocalization()
    }
    
    func configureView() {
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
        currentLocationButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        mapView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func setLocalization() {
        currentLocationButton.setTitle("my location".localized, for: .normal)
        mapLabel.text = "map".localized
        destinationLabel.text = "destination".localized

    }
    func mapDestinationButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.mapDestinationButtonPressed()
        }
    }
    
    func currentLocationButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.currentLocationButtonPressed()
        }
    }
    
    @IBAction func myLocationButtonIsPressed(_ sender: Any) {
        print("My location Button")
        self.currentLocationButtonPressed()
    }
    
    @IBAction func mapButtonIsPressed(_ sender: Any) {
        print("Map Button")
        self.mapDestinationButtonPressed()
    }
    
}
