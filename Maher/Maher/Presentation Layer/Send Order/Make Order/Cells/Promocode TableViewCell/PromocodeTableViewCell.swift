//
//  PromocodeTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import KWVerificationCodeView

protocol PromocodeTableViewCellDelegate {
    func PromocodeViewPressed(promocde: String)
}

class PromocodeTableViewCell: UITableViewCell {
    
    var delegate: PromocodeTableViewCellDelegate?
    var verificationCode: String?
    
    @IBOutlet weak var promocodeLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var verificationCodeView: KWVerificationCodeView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }
    
    func configure() {
        promocodeLabel.text = "promocode title".localized
        self.verificationCodeView.delegate = self
        closeKeypad()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        verificationCodeView.endEditing(true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
    }
    
    func PromocodeViewPressed(promocde: String) {
        if let delegateValue = delegate {
            delegateValue.PromocodeViewPressed(promocde: promocde)
        }
    }
    
}
