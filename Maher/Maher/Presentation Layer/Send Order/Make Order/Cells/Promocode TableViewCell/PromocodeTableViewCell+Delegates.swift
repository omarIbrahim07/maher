//
//  PromocodeTableViewCell+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/1/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import KWVerificationCodeView

extension PromocodeTableViewCell: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
//      confirmButton.isEnabled = verifyCodeView.hasValidCode()
//        self.verificationCode = verificationCodeView.getVerificationCode()
        self.PromocodeViewPressed(promocde: verificationCodeView.getVerificationCode())
    }
}


