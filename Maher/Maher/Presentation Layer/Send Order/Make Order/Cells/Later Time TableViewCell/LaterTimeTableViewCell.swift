//
//  LaterTimeTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol LaterTimeTableViewCellDelegate {
    func laterTimeButtonViewPressed()
    func timeButtonPressed()
    func dateButtonPressed()
}
        
class LaterTimeTableViewCell: UITableViewCell {

    var delegate: LaterTimeTableViewCellDelegate?

    @IBOutlet weak var timeTitleLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var anotherTimeLabel: UILabel!
    @IBOutlet weak var laterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLocalization()
    }
    
    func configureView() {
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
    }

    func setLocalization() {
        timeTitleLabel.text = "time".localized
        anotherTimeLabel.text = "another time".localized
        laterLabel.text = "later".localized
        dateLabel.text = "date".localized
        timeLabel.text = "time".localized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func laterTimeButtonViewPressed() {
        if let delegateValue = delegate {
            delegateValue.laterTimeButtonViewPressed()
        }
    }
    
    func timeButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.timeButtonPressed()
        }
    }
    
    func dateButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.dateButtonPressed()
        }
    }

    @IBAction func timeButtonIsPressed(_ sender: Any) {
        timeButtonPressed()
    }
    
    @IBAction func dateButtonIsPressed(_ sender: Any) {
        dateButtonPressed()
    }
    
    @IBAction func anotherTimeButtonIsPressed(_ sender: Any) {
        self.laterTimeButtonViewPressed()
    }
    
}
