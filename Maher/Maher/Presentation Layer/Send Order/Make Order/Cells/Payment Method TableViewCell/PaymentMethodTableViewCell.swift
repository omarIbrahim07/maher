//
//  PaymentMethodTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol PaymentMethodTableViewCellDelegate {
    func paymentMethodButtonViewPressed()
}

class PaymentMethodTableViewCell: UITableViewCell {
    
    var delegate: PaymentMethodTableViewCellDelegate?

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
        paymentLabel.text = "payment".localized
        paymentMethodLabel.text = "cash".localized
    }
    
    func paymentMethodButtonViewPressed() {
        if let delegateValue = delegate {
            delegateValue.paymentMethodButtonViewPressed()
        }
    }
    
    @IBAction func paymentMethodButtonIsPressed(_ sender: Any) {
        paymentMethodButtonViewPressed()
    }
    
}
