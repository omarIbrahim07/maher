//
//  OrderDescriptionTableViewCell.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
protocol OrderDescriptionTableViewCellDelegate {
    func orderDescriptionTextViewPressed(description: String)
}

class OrderDescriptionTableViewCell: UITableViewCell {

    var delegate: OrderDescriptionTableViewCellDelegate?

    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }
        
    func configure() {
        innerView.roundCorners([.topLeft, .topRight], radius: 8)
        innerView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1), borderWidth: 3)
        descriptionTextView.delegate = self
        descriptionTextView.text = "order description".localized
        descriptionTextView.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        orderDescriptionLabel.text = "order description".localized

        
        if "Lang".localized == "en" {
            descriptionTextView.textAlignment = .left
        } else if "Lang".localized == "ar" {
            descriptionTextView.textAlignment = .right
        }

//        sendButton.setTitle("rate now button".localized, for: .normal)
        closeKeypad()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        descriptionTextView.endEditing(true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func orderDescriptionTextViewPressed(description: String) {
        if let delegateValue = delegate {
            delegateValue.orderDescriptionTextViewPressed(description: description)
        }
    }
    
}
