//
//  OrderDescriptionTableViewCell+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/1/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension OrderDescriptionTableViewCell: UITextViewDelegate {
            
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "order description".localized
            textView.textColor = #colorLiteral(red: 0.38512218, green: 0.3379976153, blue: 0.6507643461, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        orderDescriptionTextViewPressed(description: textView.text)
    }
    
}

