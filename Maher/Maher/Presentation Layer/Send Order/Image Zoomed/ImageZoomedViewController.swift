//
//  ImageZoomedViewController.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

import UIKit

class ImageZoomedViewController: BaseViewController {

    var imageURL: String?
    @IBOutlet weak var image: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let imageURL = self.imageURL {
            image.loadImageFromUrl(imageUrl: imageURL)
        }
    }

}
