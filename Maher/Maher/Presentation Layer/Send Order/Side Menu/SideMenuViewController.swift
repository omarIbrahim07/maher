//
//  SideMenuViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH
import SideMenu

class SideMenuViewController: UIViewController {
    
    let menuArray: [String] = [
//        "Main",
//        "Profile",
//        "Orders",
        "Settings",
        "Terms & Conditions",
        "Privacy policy",
        "About app",
        "Who's us",
        "Complaints",
//        "Service provider",
        "Contact us",
        "Favourites",
        "العربية"
    ]
    
    let arabicMenuArray: [String] = [
//      "الرئيسية",
//      "الصفحة الشخصية",
//      "الطلبات",
      "الإعدادات",
      "الشروط والأحكام",
      "سياسة الخصوصية",
      "حول التطبيق",
      "من نحن",
      "شكاوى",
//      "مقدم خدمة",
      "تواصل معنا",
    "المفضلات",
    "English"
    ]
        
    let menuImages: [String] = [
//        "ic_home_24px",
//        "Profile",
//        "ic_shopping_cart_24px",
        "ic_brightness_high_24px",
        "ic_import_contacts_24px",
        "ic_security_24px",
        "ic_info_outline_24px",
        "ic_sms_failed_24px",
        "ic_content_paste_24px",
//        "delivery-man",
        "ic_insert_link_24px",
        "ic_insert_link_24px",
        "ic_insert_link_24px",
    ]


    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SideMenuManager.defaultManager.leftMenuNavigationController?.menuWidth = (view.frame.width - 50)
        configureView()
        configureMenuTableViewCell()
    }
    
    func configureView() {
        
        if "Lang".localized == "en" {
//            tableView.roundCorners([.topRight], radius: 40)
        } else if "Lang".localized == "ar" {
//            tableView.roundCorners([.topLeft], radius: 40)
        }
    }
    
    func configureMenuTableViewCell() {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
// MARK:- Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToFavourites() {
        if let favouritesController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavouritesViewController") as? FavouritesViewController {
            //                rootViewContoller.test = "test String"
            self.present(favouritesController, animated: true, completion: nil)
        }
    }
    
    func changeLanguage() {
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//        MOLH.reset()
        reset()
        //        languageLabel.text = "Language".localized
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
    }

    
//    func goToMentainanceOrders() {
//        if let mentainanceOrdersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MentainanceOrdersViewController") as? MentainanceOrdersViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(mentainanceOrdersController, animated: true, completion: nil)
//            print("Mentainance Orders")
//        }
//    }
//    
//    func goToOffers() {
//        if let offersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
//            //                rootViewContoller.test = "test String"
////            offersController.choosedServiceEnum = .menu
//            offersController.viewModel.choosedServiceEnum = .menu
//            self.present(offersController, animated: true, completion: nil)
//            print("Offers")
//        }
//    }
//    
//    func goToBids() {
////        if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewControllernav") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? BidsViewController {
////            //                rootViewContoller.test = "test String"
////            self.present(myAccountNavigationController, animated: true, completion: nil)
////            print("Fes")
////        }
//        
//        if let bidsController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewController") as? BidsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(bidsController, animated: true, completion: nil)
//            print("Bids")
//        }
//    }
//    
//    func goToListOfChats() {
//        if let listOfChats = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ListOfChatsViewController") as? ListOfChatsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(listOfChats, animated: true, completion: nil)
//            print("List Of Chats")
//        }
//    }
//    
//    func goToMyAccount() {
//        if let myAccount = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(myAccount, animated: true, completion: nil)
//            print("My Account")
//        }
//    }
//    
//    func goToSafteyProcedures() {
//        if let safteyProceduresVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SafteyProceduresViewController") as? SafteyProceduresViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(safteyProceduresVC, animated: true, completion: nil)
//            print("Saftey Procedures")
//        }
//    }
//    
//    func goToTermsAndConditions() {
//        if let termsAndConditionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(termsAndConditionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToPromoCode() {
//        if let promoCodeVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "PromoCodeViewController") as? PromoCodeViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(promoCodeVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToComplainsAndSuggestions() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ComplainsAndSuggestionsViewController") as? ComplainsAndSuggestionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToConnectWithUs() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ContactWithUsViewController") as? ContactWithUsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("Connect With Us")
//        }
//    }
//    
//    func goToAboutApp() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutAppViewController") as? AboutAppViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("About App")
//        }
//    }
//    
    func logout() {
                
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            self.goToLogIn()
            
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }

//    func goToImageDetails(imageURL: String) {
//        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
//            //                rootViewContoller.test = "test String"
//            imageDetailsVC.imageURL = imageURL
//            self.present(imageDetailsVC, animated: true, completion: nil)
//            print("bidDetailsVC")
//        }
//    }
    
    func goToAboutUs() {
        if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutUsNavVC") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? AboutUsViewController {
            //                rootViewContoller.test = "test String"
            self.present(myAccountNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }
        
    func goToNotifications() {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "FollowingOrdersViewController") as! FollowingOrdersViewController
//        //        viewController.typeIdSelected = self.typeIdSelected
//        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToProviderRegistraion() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProviderRegistrationViewController") as! ProviderRegistrationViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }



}
