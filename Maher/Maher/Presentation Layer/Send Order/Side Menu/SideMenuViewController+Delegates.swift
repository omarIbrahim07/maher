//
//  SideMenuViewController+Delegates.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
                cell.delegate = self
                cell.logoutButtonDelegate = self
                if let user = UserDefaultManager.shared.currentUser {
                    if let firstName: String = user.firstName {
                        cell.userNameLabel.text = firstName
                    }
                    
                    if let image: String = user.image {
                        cell.profileUserImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
                    }
                    
                    if let email: String = user.email {
                        cell.userEmailLabel.text = email
                    }
                }
                return cell
            }
        }
        
        else if indexPath.row > 0 && indexPath.row < 12 {
            if let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell {
                
                
                if "Lang".localized == "en" {
                    cell.menuLabel.text = menuArray[indexPath.row - 1]
                } else if "Lang".localized == "ar" {
                    cell.menuLabel.text = arabicMenuArray[indexPath.row - 1]
                }
                if indexPath.row < 9 {
                    cell.menuImage.image = UIImage(named: menuImages[indexPath.row - 1])
                }
                return cell
            }
        }
        
//        else if indexPath.row == 6 {
//            if let cell: LogoutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell {
//                if "Lang".localized == "en" {
//                    cell.logoutLabel.text = "logout".localized
//                } else if "Lang".localized == "ar" {
//                    cell.logoutLabel.text = "logout".localized
//                }
//                return cell
//            }
//        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if indexPath.row == 1 {
//            self.goToHomePage()
//        }
//        else if indexPath.row == 2 {
//        } else if indexPath.row == 3 {
//            goToAboutUs()
//        } else if indexPath.row == 4 {
//            goToNotifications()
//        } else if indexPath.row == 5 {
//            changeLanguage()
//        } else if indexPath.row == 6 {
//            self.logout()
////            goToMyAccount()
//        } else if indexPath.row == 7 {
////            goToSafteyProcedures()
//        } else if indexPath.row == 8 {
//
//        } else if indexPath.row == 9 {
////            goToTermsAndConditions()
//        } else if indexPath.row == 10 {
//            print("تواصل معنا")
//        }
        if indexPath.row == 1 {
            goToNotifications()
        } else if indexPath.row == 2 {
//            changeLanguage()
        } else if indexPath.row == 3 {
//            self.logout()
            //            goToMyAccount()
        } else if indexPath.row == 4 {
            //            goToSafteyProcedures()
        } else if indexPath.row == 5 {
            
        } else if indexPath.row == 6 {
            //            goToTermsAndConditions()
        } else if indexPath.row == 7 {
            print("تواصل معنا")
        } else if indexPath.row == 8 {
            print("Favourites")
            self.goToFavourites()
        } else if indexPath.row == 9 {
            self.changeLanguage()
        }


        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SideMenuViewController: profileImageTableViewCellDelegate {
    func profileImageButtonPressed() {
        if let image = UserDefaultManager.shared.currentUser?.image {
//            self.goToImageDetails(imageURL: ImageURL_USERS + image)
        }
    }
}

extension SideMenuViewController: logOutButtonTableViewCellDelegate {
    func logOutButtonPressed() {
        self.logout()
    }
}

