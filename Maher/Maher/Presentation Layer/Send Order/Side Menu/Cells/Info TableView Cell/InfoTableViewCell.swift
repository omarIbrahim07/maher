//
//  InfoTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol profileImageTableViewCellDelegate {
    func profileImageButtonPressed()
}

protocol logOutButtonTableViewCellDelegate {
    func logOutButtonPressed()
}

class InfoTableViewCell: UITableViewCell {

    var delegate: profileImageTableViewCellDelegate?
    var logoutButtonDelegate: logOutButtonTableViewCellDelegate?
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var profileUserImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var joinUsButton: UIButton!
    
    //MARK:- Delegate Helpers
    func profileImageButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.profileImageButtonPressed()
        }
    }
    
    func logOutButtonPressed() {
        if let delegateValue = logoutButtonDelegate {
            delegateValue.logOutButtonPressed()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        viewContainer.roundCorners(.bottomRight, radius: 60)
        joinUsButton.roundCorners(.bottomRight, radius: 13)
        configureCell()
    }

    func configureCell() {
        profileUserImage.addCornerRadius(raduis: profileUserImage.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    @IBAction func logoutButtonIsPressed(_ sender: Any) {
        print("Logout")
        self.logOutButtonPressed()
    }
    
    @IBAction func joinUsButtonIsPressed(_ sender: Any) {
        
    }
    
}
