
//MARK:- GET Location on map pointt

//
//  MapViewController.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import GoogleMaps


class MapViewController: BaseViewController {

    private let locationManager = CLLocationManager()
    var marker = GMSMarker()
    var cellChoosed: Int?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {

      // 1
      let geocoder = GMSGeocoder()
        let labelHeight = self.addressLabel.intrinsicContentSize.height
        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                            bottom: labelHeight, right: 0)

      // 2
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard let address = response?.firstResult(), let lines = address.lines else {
          return
        }

        print(coordinate.latitude)
        print(coordinate.longitude)

        // 3
        self.addressLabel.text = lines.joined(separator: "\n")
        if self.cellChoosed == 1 {
            myDestinationChoiceAddress = lines.joined(separator: "\n")
        } else if self.cellChoosed == 2 {
            myArrivalDestinationChoiceAddress = lines.joined(separator: "\n")
        }

        // 4
        UIView.animate(withDuration: 0.25) {
          //2
//          self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
          self.view.layoutIfNeeded()
        }
      }
    }
    
    // MARK: - Navigation
//    override func didMove(toParent parent: UIViewController?) {
//        super.didMove(toParent: parent)
//
//        if parent == nil {
//            debugPrint("Back Button pressed.")
//            didBackButtonPressed(longitude: longitude!, latitude: latitude!)
//        }
//    }

}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedWhenInUse else {
        return
    }
    // 4
    locationManager.startUpdatingLocation()

    //5
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
  }

  // 6
      func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            guard let location = locations.first else {
              return
            }

            // 7
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)

            // 8
            locationManager.stopUpdatingLocation()
        }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
      print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        // Creates a marker in the center of the map.
        marker.map = nil
        self.mapView.clear()
        marker.map = mapView
        marker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        if cellChoosed == 1 {
            destinationLongitude = coordinate.longitude
            destinationLatitude = coordinate.latitude
        } else if cellChoosed == 2 {
            arrivalDestinationLongitude = coordinate.longitude
            arrivalDestinationLatitude = coordinate.latitude
        }
        marker.map = mapView
        reverseGeocodeCoordinate(coordinate)
    }

}
