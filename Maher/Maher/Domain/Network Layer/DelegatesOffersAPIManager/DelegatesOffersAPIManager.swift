//
//  DelegatesOffersAPIManager.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DelegatesOffersAPIManager: BaseAPIManager {

    func getDelegatesOffers(basicDictionary params:APIParams , onSuccess: @escaping ([DelegateOffer])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GET_DELEGATES_OFFERS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<DelegateOffer>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func assignProviderToOrder(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: USER_ASSIGNNING_PROVIDER_TO_ORDER_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
           
        if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
        onSuccess(saved)
        }
           
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
       
        }) { (apiError) in
            onFailure(apiError)
        }
     }

    func getDelegatesNewOrders(basicDictionary params:APIParams , onSuccess: @escaping ([DelegateNewOrder])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_DELEGATES_NEW_ORDERS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<DelegateNewOrder>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getDelegateNewOrderDetails(basicDictionary params:APIParams , onSuccess: @escaping (DelegateNewOrder)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .get, path: GET_DELEGATE_NEW_ORDER_DETAILS_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<DelegateNewOrder>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func delegateSendOffer(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: PROVIDER_SEND_OFFER_PRICE_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
           
        if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
            onSuccess(saved)
        }
                       
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
       
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "لقد تم تقديم عرض من قبل"
            }
            onFailure(apiError)
        }
     }

    func favouriteProvider(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

       let engagementRouter = BaseRouter(method: .post, path: FAVOURITE_PROVIDER_URL, parameters: params)

       super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
          
       if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
       onSuccess(saved)
       }
          
       else {
           let apiError = APIError()
           onFailure(apiError)
       }
      
       }) { (apiError) in
           onFailure(apiError)
       }
    }
    
    func favouriteItem(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

       let engagementRouter = BaseRouter(method: .post, path: FAVOURITE_ITEM_URL, parameters: params)

       super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
          
       if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
       onSuccess(saved)
       }
          
       else {
           let apiError = APIError()
           onFailure(apiError)
       }
      
       }) { (apiError) in
           onFailure(apiError)
       }
    }


    func getFavouriteProviders(basicDictionary params:APIParams , onSuccess: @escaping ([FavouriteProvider])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_FAVOURITE_PROVIDERS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<FavouriteProvider>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getFavouriteItems(basicDictionary params:APIParams , onSuccess: @escaping ([FavouriteItem])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_FAVOURITE_ITEMS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<FavouriteItem>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

}
