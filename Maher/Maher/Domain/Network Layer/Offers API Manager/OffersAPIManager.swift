//
//  OffersAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class OffersAPIManager: BaseAPIManager {

    func sendPromoCode(basicDictionary params:APIParams , onSuccess: @escaping (PromocodeResponse)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_PROMOCODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<PromocodeResponse>().map(JSON: response) {
                   
            onSuccess(wrapper)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getPromoCode(basicDictionary params:APIParams , onSuccess: @escaping (PromocodeObj)->Void, onFailure: @escaping  (APIError)->Void) {
           
       let engagementRouter = BaseRouter(method: .get, path: GET_PROMOCODE_URL, parameters: params)
       
       super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
           
        if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<PromocodeObj>().map(JSON: response) {

            onSuccess(wrapper)
           }
           else {
               let apiError = APIError()
               onFailure(apiError)
           }
           
       }) { (apiError) in
           onFailure(apiError)
       }
   }
    
    func getOffers(basicDictionary params:APIParams , onSuccess: @escaping ([Offer])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_OFFERS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Offer>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getOfferDetails(basicDictionary params:APIParams , onSuccess: @escaping (OfferDetails)->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: GET_OFFER_DETAILS_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<OfferDetails>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }


    
}
