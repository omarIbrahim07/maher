//
//  ServicesAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ServicesAPIManager: BaseAPIManager {

    func getServices(basicDictionary params:APIParams , onSuccess: @escaping ([Service])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SERVICES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Service>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getSubervices(forService serviceID: Int, basicDictionary params:APIParams , onSuccess: @escaping ([Subservice])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let getSubserviceURL = GET_SUBSERVICES_URL + String(serviceID)
        
        let engagementRouter = BaseRouter(method: .get, path: getSubserviceURL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let jsonArray: [[String : Any]] = json["data"] as? [[String : Any]] {
                let wrapper = Mapper<Subservice>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getSecondSubservices(forSubservice subserviceID: Int, basicDictionary params:APIParams , onSuccess: @escaping ([SecondSubservice])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let getSubserviceURL = GET_SECONDSUBSERVICES_URL + String(subserviceID)
        
        let engagementRouter = BaseRouter(method: .get, path: getSubserviceURL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let jsonArray: [[String : Any]] = json["data"] as? [[String : Any]] {
                let wrapper = Mapper<SecondSubservice>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getStoreDetails(forItemId itemID: Int, basicDictionary params:APIParams , onSuccess: @escaping (StoreDetails)->Void, onFailure: @escaping  (APIError)->Void) {

        let getStoreDetailsURL = GET_STORE_DETAILS_URL + String(itemID)

        let engagementRouter = BaseRouter(method: .get, path: getStoreDetailsURL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<StoreDetails>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }



}
