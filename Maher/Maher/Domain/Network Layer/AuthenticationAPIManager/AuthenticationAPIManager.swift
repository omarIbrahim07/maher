////
////  AuthenticationAPIManager.swift
////  GameOn
////
////  Created by Hassan on 12/18/18.
////  Copyright © 2018 GameOn. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//class AuthenticationAPIManager: BaseAPIManager {
//
//    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                UserDefaultManager.shared.currentUser = wrapper
//                onSuccess(wrapper)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func registerUser(basicDictionary params: APIParams, onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: SIGNUP_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//            else if let response: [String : Any] = responseObject as? [String : Any], let message = response["message"] as? String, message == "api_messages.The email has already been taken." {
//                onSuccess(User())
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func logout(basicDictionary params: APIParams, onSuccess: @escaping ()->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGOUT_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            onSuccess()
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//    func getProfile(basicDictionary params:APIParams , onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: GET_PROFILE_DATA, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<User>().map(JSON: data) {
//                onSuccess(wrapper)
//            }
//
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
//
//
//}

//
//  AuthenticationAPIManager.swift
//  GameOn
//
//  Created by Hassan on 12/18/18.
//  Copyright © 2018 GameOn. All rights reserved.
//

import UIKit
import ObjectMapper

class AuthenticationAPIManager: BaseAPIManager {
    
    static var shared = AuthenticationAPIManager()
    var userLoggedIn: Bool {
        get {
            if let _ = UserDefaultManager.shared.currentUser {
                return true
            }
            else {
                return false
            }
        }
    }
    
    var fbData: [String : Any]?
    
    func registerUser(basicDictionary params:APIParams, onSuccess: @escaping (UserSignedObj) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: REGISTER_CLIENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let userWrapper = Mapper<UserSignedObj>().map(JSON: response) {
                onSuccess(userWrapper)
                print(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "البريد الالكتروني مٌستخدم من قبل"
            }
            onFailure(apiError)
        }
    }
    
//    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
//
//        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String, let user: [String : Any] = response["user"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: user) {
//                let authorization = "Bearer \(token)"
//                UserDefaultManager.shared.authorization = authorization
//                UserDefaultManager.shared.currentUser = userWrapper
//                onSuccess(userWrapper)
////                UserDefaultManager.shared.currentUser = user
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func loginUser(basicDictionary params:APIParams , onSuccess: @escaping (String, Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: LOGIN_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let token: String = response["token"] as? String, let success: Bool = response["success"] as? Bool, let data = response["user"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                let authorization = "Bearer \(token)"
                UserDefaultManager.shared.authorization = authorization
                UserDefaultManager.shared.currentUser = userWrapper
                onSuccess(authorization, success)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getUserProfile(onSuccess: @escaping (User) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_USER_URL, parameters: [:])
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getProviderProfile(basicDictionary params:APIParams, onSuccess: @escaping (ProviderData) -> Void, onFailure: @escaping  (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GET_PROVIDER_PROFILE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<ProviderData>().map(JSON: data) {
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
        
    }

    
    func editProfile(basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper
                
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func sendVerificationCode(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_VERIFICATION_CODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["status"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func changeProviderState(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHANGE_PROVIDER_STATE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Int = response["orders_notify"] as? Int {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func checkVerificationCode(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHECK_VERIFICATION_CODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let status: Bool = response["status"] as? Bool {
                
                onSuccess(status)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func forgetPassword(basicDictionary params:APIParams , onSuccess: @escaping (ForgetPassword)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: FORGET_PASSWORD_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<ForgetPassword>().map(JSON: response) {
                
                onSuccess(wrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func changePasswordFromLogIn(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD_FROM_LOGIN_URL, parameters: params)
    
    super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let status: Bool = response["status"] as? Bool {
            
            onSuccess(status)
        }
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func changePassword(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHANGE_PASSWORD_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func changeUserProfileWithImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)
//
//        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
////                UserDefaultManager.shared.currentUser = userWrapper
//
//                onSuccess(message)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func changeUserProfileWithImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)

        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper

                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }


    
    func logout(basicDictionary params:APIParams , onSuccess: @escaping (String)->String, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: LOGOUT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["message"] as? String {
                UserDefaultManager.shared.authorization = nil
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
        
    func changeUserProfile(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (User)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: EDIT_PROFILE_URL, parameters: params)
        
        super.performUploadNetworkRequest(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let userWrapper = Mapper<User>().map(JSON: data) {
                UserDefaultManager.shared.currentUser = userWrapper

                
                onSuccess(userWrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getProviderQuestion(basicDictionary params:APIParams , onSuccess: @escaping (ProviderQuestion)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .get, path: GET_PROVIDER_QUESTION_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<ProviderQuestion>().map(JSON: response) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func registerAsProviderWithoutImages(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: REGISTER_PROVIDER_URL, parameters: params)

        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in

        if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
        onSuccess(saved)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
     }

    
    func registerAsProvider(imageDataArray: [Data], basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: REGISTER_PROVIDER_URL, parameters: params)

        super.performUploadNetworkRequestImageArray(nameOfArrayImages: "images[]", imageDataArray: imageDataArray, forRouter: engagementRouter, onSuccess: { (responseObject) in

        if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
        onSuccess(saved)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
     }
    

//    func getProviderQuestion(basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
//
//        let engagementRouter = BaseRouter(method: .get, path: GET_PROVIDER_QUESTION_URL, parameters: params)
//
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//        if let response: [String : Any] = responseObject as? [String : Any], let saved: String = response["question"] as? String {
//        onSuccess(saved)
//        }
//
//        else {
//            let apiError = APIError()
//            onFailure(apiError)
//        }
//
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//     }
}

