//
//  StoreDetails.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/25/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class StoreDetails : Mappable {
            
    var id : Int?
    var name : String?
    var nameEN : String?
    var icon : String?
    var cover : String?
    var rating : String?
    var longitude : String?
    var latitude : String?
    var description : String?
    var descriptionEn : String?
    var openFrom : String?
    var openTo : String?
    var location : String?
    var subserviceID : Int?
    var menuImages: [menuImage]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEN <- map["name_en"]
        icon <- map["icon"]
        cover <- map["cover"]
        rating <- map["rating"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        description <- map["description"]
        descriptionEn <- map["description_en"]
        openFrom <- map["open_from"]
        openTo <- map["open_to"]
        location <- map["location"]
        menuImages <- map["menu"]
        subserviceID <- map["subservice_id"]
    }
    
}

class menuImage : Mappable {

    var image : String?

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        image <- map["image"]
    }
}
