//
//  ProviderData.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ProviderData: Mappable {
    
    var id: Int?
    var firstName: String?
    var email: String?
    var address: String?
    var phone: String?
    var image: String?
    var country: String?
    var isVerified: Int?
    var balance: String?
    var isProvider: Int?
    var rating: String?
    var ordersNotify: Int?
    var providerRatings: [ProviderRating]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        firstName <- map["first_name"]
        email <- map["email"]
        address <- map["address"]
        phone <- map["phone"]
        image <- map["image"]
        country <- map["country"]
        isVerified <- map["is_verified"]
        balance <- map["balance"]
        isProvider <- map["is_provider"]
        rating <- map["rating"]
        ordersNotify <- map["orders_notify"]
        providerRatings <- map["ratings"]
    }
}

class ProviderRating: Mappable {
    
    var id: Int?
    var orderID: Int?
    var userID: Int?
    var providerID: String?
    var providerRating: Int?
    var userRating: Int?
    var userComment: String?
    var providerComment: String?
    var categoryRating: String?
    var categoryComment: String?
    var userName: String?
    var userImage: String?
    var createdAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        orderID <- map["order_id"]
        userID <- map["user_id"]
        providerID <- map["provider_id"]
        providerRating <- map["provider_rating"]
        userRating <- map["user_rating"]
        userComment <- map["user_comment"]
        providerComment <- map["provider_comment"]
        categoryRating <- map["category_rating"]
        categoryComment <- map["category_comment"]
        userName <- map["user_name"]
        userImage <- map["user_image"]
        createdAt <- map["created_at"]
    }
}
