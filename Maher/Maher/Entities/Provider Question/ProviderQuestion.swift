//
//  ProviderQuestion.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ProviderQuestion : Mappable {

    var serviceName: String?
    var serviceNameEn : String?
    var question : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        serviceName <- map["service_name"]
        serviceNameEn <- map["service_nameEN"]
        question <- map["question"]
    }

}
