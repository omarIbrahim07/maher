//
//  Service.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Service : Mappable {
    
    var id : Int?
    var name : String?
    var image : String?
    var nameEN : String?
    var country: String?
    var type: Int?
    var active: Int?
    var sorting: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEN <- map["name_en"]
        image <- map["image"]
        country <- map["country"]
        type <- map["type"]
        active <- map["active"]
        sorting <- map["sorting"]
    }
    
}
