//
//  DelegateOffer.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/2/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DelegateOffer : Mappable {
    
    var id : Int?
    var orderID : Int?
    var cost : String?
    var providerID : Int?
    var orderOfferStatus : Int?
    var orderOfferLong : String?
    var orderOfferLat : String?
    var providerName : String?
    var order: Order?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        orderID <- map["order_id"]
        cost <- map["cost"]
        providerID <- map["provider_id"]
        orderOfferStatus <- map["orderoffer_status"]
        orderOfferLong <- map["orderoffer_long"]
        orderOfferLat <- map["orderoffer_lat"]
        providerName <- map["provider_name"]
        order <- map["order"]
    }

}
