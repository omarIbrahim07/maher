//
//  Subservice.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Subservice : Mappable {
        
    var id : Int?
    var name : String?
    var image : String?
    var nameEN : String?
    var serviceId : Int?
    var description: String?
    var descriptionEn: String?
    var itemsCount: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEN <- map["name_en"]
        image <- map["image"]
        serviceId <- map["service_id"]
        description <- map["description"]
        descriptionEn <- map["description_en"]
        itemsCount <- map["itemscount"]
    }
    
}
