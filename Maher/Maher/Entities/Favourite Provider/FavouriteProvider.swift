//
//  FavouriteProvider.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class FavouriteProvider: Mappable {
    
    var id: Int?
    var userID: Int?
    var providerID: Int?
    var createdAt: String?
    var updatedAt: String?
    var providerName: String?
    var providerImage: String?
    var providerRating: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userID <- map["user_id"]
        providerID <- map["provider_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        providerName <- map["provider_name"]
        providerImage <- map["provider_image"]
        providerRating <- map["provider_rating"]
    }
}
