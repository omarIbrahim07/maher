//
//  DelegateNewOrder.swift
//  Maher
//
//  Created by Omar Ibrahim on 7/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class DelegateNewOrder : Mappable {

    var orderID: Int?
    var orderStatus: Int?
    var clientName: String?
    var pickLatitude: String?
    var pickLongitude: String?
    var desLatitude: String?
    var desLongitude: String?
    var description: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderID <- map["id"]
        orderStatus <- map["order_status"]
        clientName <- map["client_name"]
        pickLatitude <- map["pick_lat"]
        pickLongitude <- map["pick_long"]
        desLatitude <- map["des_lat"]
        desLongitude <- map["des_long"]
        description <- map["order_details"]
    }
}
