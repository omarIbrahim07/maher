//
//  SecondSubservice.swift
//  Maher
//
//  Created by Omar Ibrahim on 6/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class SecondSubservice : Mappable {
            
    var id : Int?
    var name : String?
    var nameEN : String?
    var icon : String?
    var cover : String?
    var rating : String?
    var longitude : String?
    var latitude : String?
    var description : String?
    var descriptionEn : String?
    var openFrom : String?
    var openTo : String?
    var location : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEN <- map["name_en"]
        icon <- map["icon"]
        cover <- map["cover"]
        rating <- map["rating"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        description <- map["description"]
        descriptionEn <- map["description_en"]
        openFrom <- map["open_from"]
        openTo <- map["open_to"]
        location <- map["location"]
    }
    
}
