//
//  FavouriteItem.swift
//  Maher
//
//  Created by Omar Ibrahim on 8/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class FavouriteItem: Mappable {
    
    var id: Int?
    var userID: Int?
    var itemID: Int?
    var createdAt: String?
    var updatedAt: String?
    var favouriteItemDetails: FavouriteItemDetails?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userID <- map["user_id"]
        itemID <- map["item_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        favouriteItemDetails <- map["details"]
    }
}

class FavouriteItemDetails: Mappable {
    
    var id: Int?
    var name: String?
    var nameEn: String?
    var icon: String?
    var cover: String?
    var rating: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        nameEn <- map["name_en"]
        icon <- map["icon"]
        cover <- map["cover"]
        rating <- map["rating"]
    }
}
