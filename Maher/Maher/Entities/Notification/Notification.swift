//
//  Notification.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 7/29/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Notificationn: Mappable {
    
    var id: Int?
    var userID: Int?
    var orderId : Int?
    var providerName: String?
    var orderStatus: Int?
    var message : String?
    var messageEN : String?
    var updatedAt : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        userID <- map["user_id"]
        orderId <- map["order_id"]
        providerName <- map["provider_name"]
        orderStatus <- map["order_status"]
        message <- map["message"]
        messageEN <- map["messageEN"]
        updatedAt <- map["updated_at"]
    }
    
}
