//
//  Order.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Order: Mappable {
    
    var id: Int?
    var orderStatus: Int?
    var orderDetails: String?
    var userID: Int?
    var itemID: Int?
    var subserviceID: Int?
    var promocode: String?
    var paymentMethod: Int?
    var pickLatitude: String?
    var pickLongitude: String?
    var desLatitude: String?
    var orderTime: String?
    var attachmentFile: String?
    var providerID: Int?
    var country: String?
    var createdAt: String?
    var updatedAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        orderStatus <- map["order_status"]
        orderDetails <- map["order_details"]
        userID <- map["user_id"]
        itemID <- map["item_id"]
        subserviceID <- map["subservice_id"]
        promocode <- map["promocode"]
        paymentMethod <- map["payment_method"]
        pickLatitude <- map["pick_lat"]
        pickLongitude <- map["pick_long"]
        desLatitude <- map["des_lat"]
        orderTime <- map["order_time"]
        attachmentFile <- map["attachment_file"]
        providerID <- map["provider_id"]
        country <- map["country"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        
    }
}
